## CVC
****
This project is to propose an efficient method for clustering complex structure dataset (still struggle with overlapped cases) and Big Data as well.
It is composed of 3 keys phases: sampling with Modification ProTraS (MP) to create Coreset, cluster structure detection on VAT image with CoreK, and mapping step to assign
 cluster ID
from CoreK result to the original data.<br>
The result is promising and this project is still in developing better detection as well as further application in streaming context.
****
### How to Use
Firstly, sbt must be installed in your computer so as to compile and run the library.<br>
Secondly, this library has been deployed as a pipeline of processing. which means each step is defined as a pipeline operator (including MP, CoreK, index, etc.)<br>
Thus, in order to correct usage of the library, keep in mind with these steps:
1. Set up pipeline operator with specific configurations such as parameter, mode, input, output, etc.
2. Declare the pipeline process
3. Fit the input (dataset)
4. Add the operator to the pipeline
5. Transform the entire process

> *These processes must be in correct order to return the correct results.*
****
### Environment mode
#### Offline processing
##### Description
Simply declare the dataset file and create the pipeline as describes above to get the result.<br>
The result is then written into log file with proper dataset name.<br>
However, with dataset multi-dimensional (larger than 2), to get the visualisation of the result in 3-d,
remember to run script file 3d_visualisation.

##### Run
###### CVC
```run --program_mode cvc --filecore cluto-t4-8k --epsilon 0.017 --percentage 0.4 --ratio 0.00004 --avatmode avatlite --mode cluster --clean_cache true --avatlite_port 6379```
###### OPTICS or DBSCAN
```run --filecore cluto-t4-8k --epsilon 0.017 --clean_cache true```

then select main file of MainOPTICSWIthProTraS (or MainDBSCANWIthProTraS)

> Actually, you can just type ```run``` in sbt console, then a list of main file shows up and you can easily choose the desire execution.

#### Online processing (or Streaming processing)
##### Simulation
###### Save Log from Text file to Local MongoDB
* If you're using IDE IntellIJ, please configure to run as an application, with main class file is test.SaveLogToMongo,
working directory as the cvc directory, use Java 1.8 (**not 1.11** as usual), due to the mismatch between spark 2.4 and MongoDB.

* If you're using traditionally SBT, please aware to set up your java version to 1.8, then type

    ``` sbt run SaveJsonToMongo ```

If everything goes right, now your MongoDB must have blocks of data within.

###### Run Simulation
* Configure IntellIJ setting for running application as the job above, but for Simulation class.
* With SBT

    ``` sbt run Simulation ```
    
##### Streaming (still in developing, unstable release)
###### Monitoring
Combination of InfluxDB and Grafana (need installed pie chart plugin as well)<br>
Before running the simulation (or the twitter streaming), must setup grafana connection with local InfluxDB <br>
Import dashboard.json in common_python/visual directory on grafana to get the final dashboard

<br>

## How to test streaming CoreKM
****
### Benchmark
- First project cvc_streaming, run, choose number 4: StreamingOffline.
- If you want to change the data file read, look at some configurations at the beginning of the file. An example as below.
``` Scala
datafile :+= new DataFile("spambase.data", -1, 2000, 200, 0.001)
//    datafile :+= new DataFile("kddcup.data_new", 1000, 8000, 2000, 0.005)
//    datafile :+= new DataFile("covtype_data", -1, 8000, 3000, 0.3)
//    datafile :+= new DataFile("USCensus1990.data", -1, 8000, 3000, 0.5)
//    datafile :+= new DataFile("BigCross.data", -1, 8000, 2000, 0.2)
```
You can look in the DataFile.scala for more details about what these parameters means.
****
### Twitter Spam detection
- First project cvc_streaming, run, choose number 5: StreamingOfflineTwitter.
- Also check the configuration. The file used in this part taken from Kaggle.
- And notice that the furtherETL process, do we need to skip all the unnecessary token ?
- The evaluation of this algorithm, however, deployed in common_python/evaluation project, with available implementation for necessary index.