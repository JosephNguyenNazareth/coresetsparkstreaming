package vat_family

import java.awt.image.BufferedImage
import java.awt.image.WritableRaster
import java.awt.image.Raster
import java.awt.Graphics2D

import javax.imageio.ImageIO
import java.awt.Color

import org.json4s.native.JsonParser.DoubleVal
import utils.{DataPoint, Dataset, Measure, PipelineOperator}

class VAT extends PipelineOperator {

  protected var distanceFunction =
    (firstPoint: DataPoint, secondPoint: DataPoint) => {
      Measure.euclide(firstPoint, secondPoint)
    }

  protected var vatLogMode: Boolean = true

  protected var vatPlotMode: Boolean = true

  protected var objectOrder = Array[Int]()

  protected var vatImage = Array[Array[Double]]()

  protected var epsilon: Double = 0.5
  protected var percentage: Double = 0.8

  override def toString: String = "vat"

  def enableLogMode(e: Boolean): VAT = {
    vatLogMode = e
    return this
  }

  def enablePlotMode(e: Boolean): VAT = {
    vatPlotMode = e
    return this
  }

  def getVATImage(): Array[Array[Double]] = {
    return vatImage
  }

  def getVATOrder(): Array[Int] = {
    return objectOrder
  }

  def setEpsilon(ep: Double): VAT = {
    epsilon = ep
    return this
  }

  def setPercentage(per: Double): VAT = {
    percentage = per
    return this
  }

  def setDistanceFunction(df: (DataPoint, DataPoint) => Double): VAT = {
    distanceFunction = df
    return this
  }

  protected def saveImage(i: Array[Array[Double]], fileCore: String) {
    var image: BufferedImage =
      new BufferedImage(i.length, i.length, BufferedImage.TYPE_INT_RGB)
    var max: Double = i.map(_.max).max
    var min: Double = i.map(_.min).min

    //normalize
    for (x <- i.indices) {
      for (y <- i(x).indices) {
        var pixel = ((i(x)(y) / (max - min)) * 255).toInt
        image.setRGB(x, y, new Color(pixel, pixel, pixel).getRGB())
      }
    }

    //save
    ImageIO.write(
      image,
      "png",
      new java.io.File(
        s"./result/process/${this.toString}" + "/" + fileCore + "_" + epsilon.toString + "_" + percentage.toString + ".png"))
  }

  def saveObjectOrder(order: Array[Int], fileCore: String) {
    //saving object list into .log
    if (order.length > 0) {
      val dir = scala.reflect.io.File("./.log")
      if (!dir.isDirectory) dir.createDirectory()
      val core = new java.io.PrintWriter(
        new java.io.File("./.log/" + fileCore + s"_${this.toString}_order.txt"))
      core.write(order.mkString("\n"))
      core.close
    }
  }

  override def process(dataset: Dataset): Dataset = {
    duration = 0
    return fit(dataset)
  }

  def fit(dataset: Dataset): Dataset = {
    objectOrder = Array[Int]()
    val t0 = System.nanoTime()
    //dataset is a sampling one
    vatImage = new Array[Array[Double]](dataset.length)
    // create dissimilarity matrix (row major)
    var distanceMap: Array[Array[Double]] = Array[Array[Double]]()
    for (point <- 0 until dataset.length) {
      val distancePoint: Array[Double] =
        dataset.getDataset().map(x => distanceFunction(dataset(point), x))
      distanceMap :+= distancePoint
    }
    println("VAT: 1")

    // somes initial variables for VAT
    var KSet: Set[Int] = 0 until dataset.length toSet // record the index of all points in data
    var initialMaxDistance
      : Double = 0.0 // record the initial max distance in dissimilarity matrix
    var initialMaxCouple
      : (Int, Int) = (-1, -1) // the pair of points which have the initial max distance

    //search through dissimilarity matrix to find max couple
    for (idxObj <- distanceMap.indices) { //for each object
      for (idxPartner <- distanceMap(idxObj).indices) { //for each partner in object
        if (initialMaxDistance < distanceMap(idxObj)(idxPartner)) {
          initialMaxCouple = (idxObj, idxPartner)
          initialMaxDistance = distanceMap(idxObj)(idxPartner)
        }
      }
    }
    println("VAT: 2", dataset.length())

    // old version
//    var PList: Array[Int] = Array[Int](initialMaxCouple._1)
//
//    var ISet: Set[Int] = Set(initialMaxCouple._1)
//    var JSet: Set[Int] = KSet - initialMaxCouple._1
//
//    // find the closest point to current point
//    //for each coresetPoint
//    for (t <- 1 to dataset.length - 1) {
//      var minCouple: (Int, Int) = (-1, -1)
//      var minDistance: Double = Double.MaxValue
//      for (k <- ISet) {
//        for (q <- JSet) {
//          if (distanceMap(k)(q) < minDistance) {
//            minDistance = distanceMap(k)(q)
//            minCouple = (k, q)
//          }
//        }
//      }
//
//      //update
//      PList :+= minCouple._2
//      ISet = ISet + minCouple._2
//      JSet = JSet - minCouple._2
//    }

    // new version
    var PList: Array[Int] = Array[Int](initialMaxCouple._1)
    var JList: Array[Int] = new Array[Int](dataset.length) //0 unmoved, 1 moved
    var JMinDist: Array[Double] = new Array[Double](dataset.length)
    var JMinIndex: Array[Int] = new Array[Int](dataset.length)

    println(JList.length)

    //initialize first
    for (t <- JList.indices) {
      if (t == initialMaxCouple._1) {
        JList(t) = 1
        JMinDist(t) = 0.0
        JMinIndex(t) = -1
      } else {
        JList(t) = 0
        JMinDist(t) = Double.MaxValue
        JMinIndex(t) = initialMaxCouple._1
      }
    }

    var previousIndex = initialMaxCouple._1
    while (PList.length < dataset.length) {
      var minCouple: (Int, Int) = (-1, -1)
      var minDistance: Double = Double.MaxValue
      for (t <- JList.indices) {
        if (JList(t) == 0) { // unmoved
          //test for update
          var newDistance = distanceMap(t)(previousIndex)
          if (newDistance < JMinDist(t)) {
            JMinDist(t) = newDistance
            JMinIndex(t) = previousIndex
          }
          //check for smallest
          if (minDistance > JMinDist(t)) {
            minDistance = JMinDist(t)
            minCouple = (JMinIndex(t), t)
          }
        }
      }
      //update j
      JList(minCouple._2) = 1
      JMinDist(minCouple._2) = 0.0
      JMinIndex(minCouple._2) = -1
      //update p
      PList :+= minCouple._2
      previousIndex = minCouple._2
    }
    println("VAT: 3")

    // time to create new VAT heatmap
    for (p <- 0 until dataset.length) {
      vatImage(p) = new Array[Double](dataset.length)
      for (q <- 0 until dataset.length) {
        vatImage(p)(q) = distanceMap(PList(p))(PList(q))
      }
    }
    for (t <- PList) {
      objectOrder :+= dataset(t).index
    }
    println("VAT: 4")
    val t1 = System.nanoTime()
    duration = t1 - t0

    //MUST DO: SAVE IMAGE AND OBJECT ORDER, except when iVAT override these methods
    if (vatLogMode) {
      saveObjectOrder(objectOrder, dataset.getFileCore())
    }

    if (vatPlotMode) {
      saveImage(vatImage, dataset.getFileCore())
    }

    //clustering
    return dataset
  }
}
