package clustering

import scala.io.Source
import Double._
import math._
import java.io._
import util.control.Breaks._
import scala.collection.mutable.ArrayBuffer
import scala.math.Ordering
import sys.process._

import utils.{DataPoint, Dataset, Measure}

class OPTICSModel extends ClusterAlgorithm[OPTICSModel] {
  private var epsilonDistance: Double = 0.0
  private var minPts: Int = 0
  private var orgData: Array[DataPoint] = Array[DataPoint]()
  private var reachDist: Array[(Int, Double)] = Array[(Int, Double)]()
  private var cFinDist: Array[(Int, Double)] = Array[(Int, Double)]()
  private var processList: Array[(Int, Boolean)] =
    orgData.map(x => (x.index, false))
  private var orderList: Array[(Int, Double, Double)] =
    Array[(Int, Double, Double)]()

  override def toString: String = {
    return "optics"
  }

  def setEpsilon(eps: Double): OPTICSModel = {
    epsilonDistance = eps
    return this
  }

  def setMinPts(mPts: Int): OPTICSModel = {
    minPts = mPts
    return this
  }

  def fit(data: Dataset): Dataset = {
    orgData = data.getDataset().distinct
    reachDist = orgData.map(x => (orgData.indexOf(x), -1.0))
    cFinDist = orgData.map(x => (orgData.indexOf(x), 0.0))
    processList = orgData.map(x => (orgData.indexOf(x), false))

    for (realIndex <- orgData.indices) {
      breakable {
        if (processList(realIndex)._2)
          break
        val neighbour
          : Array[Int] = _rangeQuery(data, realIndex) // neighbour list index
        processList(realIndex) = processList(realIndex).copy(_2 = true)
        reachDist(realIndex) = reachDist(realIndex).copy(_2 = -1.0)

        cFinDist(realIndex) = cFinDist(realIndex).copy(
          _2 = coreDistance(data, realIndex, neighbour))
        orderList :+= (realIndex, reachDist(realIndex)._2, cFinDist(realIndex)._2)

        if (cFinDist(realIndex)._2 != -1.0) {
          var seeds: Array[(Int, Double)] = Array[(Int, Double)]()
          seeds = update(data, realIndex, neighbour, seeds)

          for (seedPoint <- seeds) {
            val seedIndex: Int = seedPoint._1
            val otherNeighbour = _rangeQuery(data, seedIndex)

            processList(seedIndex) = processList(seedIndex).copy(_2 = true)
            cFinDist(seedIndex) = cFinDist(seedIndex).copy(
              _2 = coreDistance(data, seedIndex, otherNeighbour))
            orderList :+= (seedIndex, reachDist(seedIndex)._2, cFinDist(
              seedIndex)._2)

            if (cFinDist(seedIndex)._2 != -1.0) {
              seeds = update(data, seedIndex, otherNeighbour, seeds)
            }
          }
        }
      }
    }
    extractClusterSimple(orderList)
    orderList = orderList.sortBy(x => (x._2, x._3, x._1))

    return new Dataset(orgData)
  }

  private def _rangeQuery(dataset: Dataset, pointIndex: Int): Array[Int] = {
    var neighbours: Array[Int] = Array[Int]()
    for (i <- 0 until dataset.length) {
      if (getDistance(dataset, pointIndex, i) <= epsilonDistance) {
        neighbours :+= i
      }
    }
    return neighbours
  }

  private def coreDistance(dataset: Dataset,
                           point: Int,
                           neighbour: Array[Int]): Double = {
    if (neighbour.length < minPts)
      return -1.0

    var cpDistSet: Array[Double] = Array[Double]()
    for (i <- neighbour) {
      cpDistSet :+= getDistance(dataset, point, i)

    }
    cpDistSet = cpDistSet.sorted
    val cpDist: Double = cpDistSet((minPts - 1))
    return cpDist
  }

  private def reachDistance(dataset: Dataset,
                            point: Int,
                            neighbour: Array[Int],
                            coreDist: Double): Array[Double] = {
    var normDistance: Array[Double] = Array[Double]()

    if (neighbour.length < minPts) {
      for (i <- neighbour) {
        normDistance :+= -1.0
      }
    } else {
      for (i <- neighbour) {
        normDistance :+= max(getDistance(dataset, point, i), coreDist)
      }
    }

    return normDistance
  }

  private def update(dataset: Dataset,
                     point: Int,
                     neighbour: Array[Int],
                     seeds: Array[(Int, Double)]): Array[(Int, Double)] = {
    val cpDist = coreDistance(dataset, point, neighbour)
    var functionSeeds = seeds
    val rpDist = reachDistance(dataset, point, neighbour, cpDist)
    for (realIndex <- neighbour) {
      breakable {
        if (processList(realIndex)._2)
          break
        val nrDist: Double = rpDist(neighbour.indexOf(realIndex))
        if (reachDist(realIndex)._2 == -1.0) {
          reachDist(realIndex) = reachDist(realIndex).copy(_2 = nrDist)
          functionSeeds :+= reachDist(realIndex)
        } else if (nrDist < reachDist(realIndex)._2) {
          reachDist(realIndex) = reachDist(realIndex).copy(_2 = nrDist)
          // check if there is that point in the functionSeeds set
          val searchPoint =
            functionSeeds.filter(x => x._1 == reachDist(realIndex)._1)
          if (searchPoint.length > 0) {
            val seedIndex: Int = functionSeeds.indexOf(searchPoint(0))
            functionSeeds(seedIndex) =
              functionSeeds(seedIndex).copy(_2 = nrDist)
          } else {
            functionSeeds :+= reachDist(realIndex)
          }
        }
      }
    }
    functionSeeds = functionSeeds.sortBy(x => (x._2, x._1))
    return functionSeeds
  }

  private def extractClusterSimple(
      orderList: Array[(Int, Double, Double)]): Unit = {
    val epsilonX: Double = epsilonDistance * 0.4
    var clusterIndex: Int = 0
    for (i <- orderList.indices) {
      if (orderList(i)._2 > epsilonX) {
        if (orderList(i)._3 <= epsilonX) {
          clusterIndex += 1
          val temp = orderList(i)._1
          orgData(temp).cluster = clusterIndex
        }
      } else {
        val temp = orderList(i)._1
        orgData(temp).cluster = clusterIndex
      }
    }
  }
}
