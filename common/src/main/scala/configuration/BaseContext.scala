package configuration

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

class BaseContext {
  def buildSparkContext(appName: String): SparkContext = {
    val conf = buildSparkConf(appName)
    new SparkContext(conf)
  }

  def buildSparkConf(appName: String): SparkConf = {
    val conf = new SparkConf(true)
      .setAppName(appName)
      .setMaster("local[*]")
      .set("spark.streaming.kafka.consumer.cache.initialCapacity", "64")
      .set("spark.streaming.kafka.consumer.cache.maxCapacity", "64")
      .set("spark.streaming.kafka.consumer.poll.ms", "75000")
      .set("spark.streaming.unpersist", "true")
      .set("spark.cleaner.ttl", "600")
      .set("spark.driver.extraJavaOptions", "-XX:+UseG1GC")
      .set("spark.executor.extraJavaOptions", "-XX:+UseG1GC")
      .set("spark.streaming.backpressure.enabled", "true")
      .set("spark.streaming.receiver.maxRate", "100")
      .set("spark.streaming.kafka.maxRatePerPartition", "100")
      .set("spark.streaming.stopGracefullyOnShutdown", "true")
      .set("spark.shuffle.reduceLocality.enabled", "false")
//            .set("redis.host", "10.60.37.9")
//            .set("redis.port", "7000")

    return conf
  }
}
