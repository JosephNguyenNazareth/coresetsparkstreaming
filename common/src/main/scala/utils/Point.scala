package utils

class DataPoint(
    var index: Int = -1,
    var userId: String = "",
    var tweet: Twitter = new Twitter(),
    var coord: Array[Double] = Array[Double](),
    var rep: Int = -1,
    var disToRep: Double = 0.0,
    var cluster: Int = -1
) {

  def length(): Int = {
    return coord.length
  }

  override def toString: String = {
    if (coord.nonEmpty) {
      var coordString: String = "[" + coord(0).toString
      for (i <- 1 to coord.length - 1) {
        coordString += "," + coord(i).toString
      }

      coordString += "]"

      return s"$index $coordString $rep $disToRep $cluster"
    } else {
      return s"$index $userId $rep $disToRep $cluster"
    }

  }

  override def clone: DataPoint = {
    return new DataPoint(index,
                         userId,
                         tweet,
                         coord.clone,
                         rep,
                         disToRep,
                         cluster)
  }

  def this(input: String) {
    this()
    var params: Array[String] = input.trim.split(" ").toArray
    index = params(0).toInt
    tweet = new Twitter()
    coord = params(1)
      .replaceAll("\\[", "")
      .replaceAll("\\]", "")
      .replaceAll("\n", "")
      .split(",")
      .map(_.toDouble)
    rep = params(2).toInt
    disToRep = params(3).toDouble
    cluster = params(4).toInt
  }

  def this(input: DataPoint) {
    this()
    index = input.index
    userId = input.userId
    tweet = input.tweet
    coord = input.coord.clone
    rep = input.rep
    disToRep = input.disToRep
    cluster = input.cluster
  }
}

case class TwitterData(tweetID: String = "",
                       content: String = "",
                       tweetTime: String = "",
                       timeStamp: String = "",
                       userID: String = "",
                       userName: String = "",
                       friends: Long = 0,
                       followers: Long = 0,
                       favourites: Long = 0,
                       ymd: String = "") {}

class Twitter(var tweetID: String = "",
              var content: String = "",
              var tweetTime: String = "",
              var timeStamp: String = "",
              var userID: String = "",
              var userName: String = "",
              var friends: Long = 0,
              var followers: Long = 0,
              var favourites: Long = 0,
              var ymd: String = "") {
  def this(newTweet: TwitterData) {
    this()
    tweetID = newTweet.tweetID
    content = newTweet.content
    tweetTime = newTweet.tweetTime
    timeStamp = newTweet.timeStamp
    userID = newTweet.userID
    userName = newTweet.userName
    friends = newTweet.friends
    followers = newTweet.followers
    favourites = newTweet.favourites
    ymd = newTweet.ymd
  }
}
