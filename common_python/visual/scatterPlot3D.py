import redis
import argparse
import ast
import plotly.graph_objects as go
def main():
    rc = redis.Redis(host='localhost', port=6379, db=2)
    while True:
        while True:
            request = rc.get("scatterplot_3d_request")
            if request is not None:
                rc.delete("scatterplot_3d_request")
                break
        # decode to string
        request = request.decode("utf-8")
        # convert to dictionary
        query = ast.literal_eval(request)
        #begin plotting
        fig = go.Figure(data=[go.Scatter3d(x=query['x'], y=query['y'], z=query['z'],
                                    mode='markers')])
        fig.update_layout({"title" : query['message']})
        fig.show()

if __name__ == "__main__":
    main()