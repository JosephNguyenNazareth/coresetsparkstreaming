import redis
import time
import ast


class DataReceiver(object):
    def __init__(self, host, port, database):
        # Configuration for redis
        self._host = host
        self._port = port
        self._database = database
        self._rc = redis.Redis(host=self._host, port=self._port, db=self._database)

        self._rc.set("visual_status", "success")
        # send signal to
        print("Finish initializing class for receiving data")

    def get(self, key):
        while True:
            is_join = self._rc.get("is_join")
            if is_join is not None:
                self._rc.delete("is_join")
                if is_join.decode("utf-8") == "true":
                    self._rc.set("is_close", "true")
                    exit()
            request = self._rc.get("visual_request")
            if request is not None:
                self._rc.delete("visual_request")
                # Send message if receive
                self._rc.set("visual_receive", "true")
                break
            # sleeping for 5 seconds to wait to new request
            time.sleep(1)
        # decode to string
        request = request.decode("utf-8")
        # convert to dictionary
        query = ast.literal_eval(request)
        return query
