package utils

//pipelines for preprocessing and analyze data
class Pipeline(l: Array[PipelineOperator] = Array[PipelineOperator]()) {
  private var operatorList = l
  private var obj: Dataset = null
  protected var duration: Long = 0
  //hold a list of PipelineOperator for dataset

  //fit data before perform transform
  def fit(inputDataset: Dataset): Pipeline = {
    obj = inputDataset
    return this
  }

  def transform(): Pipeline = {
    if (operatorList.length <= 0) {
      //raise error
      return this
    }
    duration = 0

    //for each operator, perfom process
    for (i <- operatorList.indices) {
      obj = operatorList(i).process(obj)
      duration = duration + operatorList(i).timeElapsed()
    }
    return this
  }

  def timeElapsed(): Long = {
    return duration
  }

  def getOutput(): Dataset = {
    return obj
  }

  def addOperator(op: PipelineOperator): Pipeline = {
    operatorList :+= op
    return this
  }

  def apply(i: Int) = operatorList(i)
}
