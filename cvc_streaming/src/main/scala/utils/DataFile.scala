package utils

class DataFile(name: String,
               label: Int = 1000,
               batch: Int = 1000,
               streamBatch: Int = 500,
               ep: Double,
               exten: String = ".txt",
               ratioAnomaly: Double = 0.000800000001) {
  var fileCore: String = name
  var labelColumn: Int = label
  var batchSize: Int = batch
  var streamingBatchSize: Int = streamBatch
  var epsilon: Double = ep
  var extension: String = exten
  var ratio: Double = ratioAnomaly
}
