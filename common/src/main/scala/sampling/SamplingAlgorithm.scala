package sampling
import sys.process._
import javax.swing.JFrame
import javax.swing.JPanel
import java.awt.image.BufferedImage

import javax.imageio.ImageIO
import java.lang.Thread
import java.awt.Graphics2D
import java.awt.image.BufferedImage

import scala.reflect.io.File
import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartPanel
import org.jfree.chart.ChartUtils
import org.jfree.chart.JFreeChart
import org.jfree.chart.plot.XYPlot
import org.jfree.data.xy.XYDataset
import org.jfree.data.xy.XYSeries
import org.jfree.data.xy.XYSeriesCollection
import javax.swing.JFrame
import javax.swing.JPanel
import java.awt.image.BufferedImage

import javax.imageio.ImageIO
import java.lang.Thread
import java.awt.Graphics2D
import java.awt.image.BufferedImage

import utils._

import scala.util.control.Breaks._
import com.redis._
import utils.{DataPoint, Dataset, Measure, PipelineOperator}
import configuration.YAMLConfig

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

abstract class SamplingAlgorithm[T <: SamplingAlgorithm[T]]
    extends PipelineOperator {
  protected var plotEnable: Boolean = false
  protected var logEnable: Boolean = false
  protected var distanceFunction =
    (firstPoint: DataPoint, secondPoint: DataPoint) => {
      Measure.euclide(firstPoint, secondPoint)
    }
  protected var calMode: String = "ram-based" //"ram-based, memory-based"
  protected var tmd: Option[Array[Array[Double]]] = None
  protected var n: Int = 0
  protected var percentage: Double = 0.5 //default
  protected var epsilon: Double = -1.0 //default
  protected var representative: Map[Int, ArrayBuffer[Int]] = Map()
  protected var samplingMode: String = "default"
  protected var samplingPhase: String = "default"

  def setDistanceFunction(df: (DataPoint, DataPoint) => Double): T = {
    distanceFunction = df
    return this.asInstanceOf[T]
  }

  def setSamplingMode(mode: String): T = {
    samplingMode = mode
    return this.asInstanceOf[T]
  }

  def setSamplingPhase(phase: String): T = {
    samplingPhase = phase
    return this.asInstanceOf[T]
  }

  def setCalMode(mode: String): T = {
    if (calMode != "memory-based" && calMode != "ram-based") {
      println("Unknown calculation mode: Choosing default")
      calMode = "ram-based"
    }
    calMode = mode
    return this.asInstanceOf[T]
  }

  protected def getDistance(dataset: Dataset, d1: Int, d2: Int): Double = {
    if (calMode == "memory-based") {
      var first = -1
      var second = -1
      if (d1 <= d2) {
        first = d1
        second = d2 - d1
      } else {
        first = d2
        second = d1 - d2
      }
      if (tmd.get(first)(second) < 0.0) {
        var distance = distanceFunction(dataset(d1), dataset(d2))
        tmd.get(first)(second) = distance
      }
      return tmd.get(first)(second)
    } else {
      return distanceFunction(dataset(d1), dataset(d2))
    }
  }

  //PRIVATE FUNCTIONS
  protected def getInitialPointIndex(dataset: Dataset): Int = {
    //  var virtualPoint : Array[Double] = new Array[Double](dataset(0).coord.length)
    //  for(i <- 0 to dataset.length - 1){
    //      for (t <- 0 to dataset(0).coord.length - 1){
    //          virtualPoint(t) += dataset(i).coord(t)
    //      }
    //  }
    //  for (t <- 0 to dataset(0).coord.length - 1){
    //          virtualPoint(t)  = virtualPoint(t) / dataset.length
    //  }
    //  var minDist : Double = Double.MaxValue
    //  var initialPoint : Int = -1
    //  for(i <- 0 to dataset.length - 1){
    //      var distance : Double = Measure.euclide(new DataPoint(coord = virtualPoint),dataset(i))
    //      if(distance < minDist){
    //          minDist = distance
    //          initialPoint = i
    //      }
    //  }
    //  return initialPoint
//    val a = scala.util.Random.nextInt(dataset.length)
//    println(a)
//    return 1299
//    if (dataset.getFileCore().contains("chainlink_3d")) {
//      return 81
//    } else if (dataset.getFileCore().contains("hawk_5_0.47")) {
//      return 1299
//    } else if (dataset.getFileCore().contains("cluto-t4-8k")) {
//      return 3670
//    }
    return scala.util.Random.nextInt(dataset.length)
  }

  def enablePlot(e: Boolean): T = {
    plotEnable = e
    return this.asInstanceOf[T]
  }

  def enableLog(e: Boolean): T = {
    logEnable = e
    return this.asInstanceOf[T]
  }

  private def saveScatterChart(dataset: Dataset, message: String) {
    if (dataset(0).coord.size == 2) {
      val dir = File(s"./result/process/${message}/image")
      if (!dir.isDirectory) dir.createDirectory()
      var series = new XYSeries(message)
      var plotData = new XYSeriesCollection
      if (dataset.length > 0) {
        for (dp <- dataset.getDataset()) {
          series.add(dp.coord(0), dp.coord(1))
        }
        plotData.addSeries(series)
        var scatterChart = ChartFactory.createScatterPlot(s"$message",
                                                          "X-Axis",
                                                          "Y-Axis",
                                                          plotData)
        ChartUtils.saveChartAsPNG(
          new java.io.File(
            s"./result/process/${message}/image/" + dataset
              .getFileCore() + ".png"),
          scatterChart,
          1024,
          729)
      }
    } else {
      println("Warning: Unoptimized 3d mode")
      println("Please open scatterPlot3D.py")
      val redisConfig =
        YAMLConfig
          .get("redis")
          .asInstanceOf[java.util.LinkedHashMap[String, Any]]
      val redis3D = redisConfig
        .get("redis_3d_visual")
        .asInstanceOf[java.util.LinkedHashMap[String, Any]]
      val host = redis3D.get("host").asInstanceOf[String]
      val port = redis3D.get("port").asInstanceOf[Int]
      val db = redis3D.get("database").asInstanceOf[Int]
      val re = new RedisClient(host, port, db)
      //calculating list of x, y and z
      var x = Array[Double]()
      var y = Array[Double]()
      var z = Array[Double]()
      for (point <- dataset.getDataset()) {
        x :+= point.coord(0)
        y :+= point.coord(1)
        z :+= point.coord(2)
      }
      var request = s"""{
        "x" : [${x.mkString(",")}],
        "y" : [${y.mkString(",")}],
        "z" : [${z.mkString(",")}],
        "message" : "${message}"
      }"""
      re.set("scatterplot_3d_request", request)
    }
  }

  private def writeToFile(dataset: Dataset, tp: String) {
    val dir = File(s"./result/process/${tp}/data/")
    if (!dir.isDirectory) dir.createDirectory()
    var outputFile: String = ""
    if (samplingMode == "default")
      outputFile = s"./result/process/${tp}/data/" + dataset
        .getFileCore() + "_" + epsilon.toString + "_" + percentage.toString + ".csv"
    else if (samplingMode == "improved")
      outputFile = s"./result/process/${tp}/data/" + dataset
        .getFileCore() + "_" + epsilon.toString + "_" + percentage.toString + "improved.csv"
    val core = new java.io.PrintWriter(new java.io.File(outputFile))
    core.write(dataset.getDataset().map(_.toString).mkString("\n"))
    core.close()
  }

  private def writeRepresentative(dataset: Dataset, tp: String): Unit = {
    var representationSet: Array[Array[Int]] = Array[Array[Int]]()
    for (k <- representative.keys) {
      for (v <- representative(k)) {
        val temp: Array[Int] = Array[Int](k, v)
        representationSet :+= temp
      }
    }
    val dir = File(s"./result/process/${tp}/data/")
    if (!dir.isDirectory) dir.createDirectory()
    val core = new java.io.PrintWriter(
      new java.io.File(s"./result/process/${tp}/data/" + dataset
        .getFileCore() + "_" + epsilon.toString + "_" + percentage.toString + "_representative.csv"))
    core.write(representationSet.map(_.mkString(",")).mkString("\n"))
    core.close()
  }

  def _fit(dataset: Dataset): Dataset = {
    if (calMode == "memory-based") {
      //initialize upper triangular matrix
      n = dataset.length()
      tmd = Some(new Array[Array[Double]](n))
      for (i <- 0 until n) {
        tmd.get(i) = Array.fill[Double](n - i)(-1.0)
        tmd.get(i)(0) = 0.0
      }
    }
    return fit(dataset)
  }

  def fit(dataset: Dataset): Dataset

  protected def readRepresentative(
      dataset: Dataset): Map[Int, ArrayBuffer[Int]] = {
    val data: Array[DataPoint] = dataset.getDataset()
    var mappingRepresent: Map[Int, ArrayBuffer[Int]] =
      Map[Int, ArrayBuffer[Int]]()

    for (line <- data) {
      val index = line.index
      val rep = line.rep
      if (mappingRepresent.contains(rep))
        mappingRepresent(rep) += index
      else
        mappingRepresent += (rep -> ArrayBuffer[Int](index))
    }
    return mappingRepresent
  }

  override def process(dataset: Dataset): Dataset = {
    duration = 0
    var sampling = _fit(dataset)

    //log mode
    if (plotEnable) {
      saveScatterChart(dataset, "origin")
      saveScatterChart(sampling, "coreset")
    }

    if (logEnable) {
      writeToFile(dataset, "origin")
      writeToFile(sampling, "coreset")
//      writeRepresentative(sampling, "coreset")
    }

    return sampling
  }
}
