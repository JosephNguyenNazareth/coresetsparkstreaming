// package utils

// import com.mongodb.MongoClient
// import com.mongodb.spark.config.ReadConfig
// import com.mongodb.spark._
// import org.apache.spark.SparkContext
// import org.apache.spark.sql.{DataFrame, SparkSession}
// import org.apache.spark.sql.SQLContext
// import org.bson.Document
// import com.mongodb.{
//   BasicDBObject,
//   MongoClient,
//   MongoClientOptions,
//   MongoCredential,
//   ServerAddress
// }
// import com.mongodb.client.MongoDatabase
// import com.mongodb.spark.rdd.MongoRDD
// import org.bson.codecs.configuration.CodecRegistries.{
//   // fromProviders,
//   fromRegistries
// }
// import org.slf4j.LoggerFactory

// object MongoDBConnector {
//   private val logger = LoggerFactory.getLogger("MongoDBConnector")

//   def readCollectionByClient(spark: SparkSession,
//                              config: java.util.LinkedHashMap[String, Any],
//                              collection: String,
//                              searchConfig: ConfigurationSearch): DataFrame = {
//     val host = config.get("host").asInstanceOf[String]
//     val database = config.get("database").asInstanceOf[String]
//     val port = config.get("port").asInstanceOf[Integer]

//     val uri = s"""mongodb://${host}:${port}/${database}.${collection}"""

//     val readConfig = ReadConfig(Map("uri" -> uri))

//     val pipeline = createPipeline(searchConfig)
//     val rdd = spark.sparkContext.loadFromMongoDB(readConfig)
//     val aggregatedRdd = rdd.withPipeline(pipeline)
//     return aggregatedRdd.toDF()

//   }

//   def readCollectionByClientTwitter(
//       sparkContext: SparkContext,
//       config: java.util.LinkedHashMap[String, Any],
//       collection: String,
//       searchConfig: ConfigurationSearch): DataFrame = {
//     val host = config.get("host").asInstanceOf[String]
//     val database = config.get("database").asInstanceOf[String]
//     val port = config.get("port").asInstanceOf[Integer]

//     val uri = s"""mongodb://${host}:${port}/${database}.${collection}"""

//     val readConfig = ReadConfig(Map("uri" -> uri))
//     val pipeline = createPipeline(searchConfig)

//     val rdd = MongoSpark.load(sparkContext, readConfig)
//     val aggregatedRdd = rdd.withPipeline(pipeline)
//     return aggregatedRdd.toDF()

//   }

//   def createPipeline(searchConfig: ConfigurationSearch): Seq[Document] = {
//     var pipeline = Seq[Document]()
//     if (searchConfig.condition != null)
//       pipeline = pipeline :+ new Document("$match", searchConfig.getCondition())
//     if (searchConfig.project != null)
//       pipeline = pipeline :+ new Document("$project", searchConfig.getProject())
// //    if (addField != null) pipeline = pipeline :+ new Document("$addFields", addField)
// //    if (sort != null) pipeline = pipeline :+ new Document("$sort", sort)
// //    if (limit != null) pipeline = pipeline :+ new Document("$limit", limit)
// //    if (skip != null) pipeline = pipeline :+ new Document("$skip", skip)
//     return pipeline
//   }

//   def deleteDocument(statement: Document,
//                      config: java.util.LinkedHashMap[String, Any],
//                      collection: String): Unit = {
//     val host = config.get("host").asInstanceOf[String]
//     val database = config.get("database").asInstanceOf[String]
//     logger.info(
//       s"Read MongoDB: mongodb://$host:27017/, database: $database, collection: $collection")
//     val mongoClient = new MongoClient(host, 27017)
//     val db: MongoDatabase = mongoClient.getDatabase(database)
//     val collectionDB = db.getCollection(collection)

//     val deleteCount = collectionDB.deleteMany(statement).getDeletedCount
//     println("==================== Delete Successfully====================")
//     println("number of deleted rows: " + deleteCount)
//     mongoClient.close()

//   }
// }
