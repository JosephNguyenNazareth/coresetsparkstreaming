package metrics

import utils.{DataPoint, Measure, PipelineOperator}

abstract class Index[T <: Index[T]] extends PipelineOperator {
  protected var value = 0.0
  protected var distanceFunction =
    (firstPoint: DataPoint, secondPoint: DataPoint) => {
      Measure.euclide(firstPoint, secondPoint)
    }

  def setDistanceFunction(df: (DataPoint, DataPoint) => Double): T = {
    distanceFunction = df
    return this.asInstanceOf[T]
  }

  def getValue(): Double = {
    return value
  }
}
