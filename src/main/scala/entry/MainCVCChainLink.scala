package entry

import java.io._

import clustering.CVCModel
import metrics.{CDbwIndex, DaviesBouldinIndex, DunnIndex, SilhouetteIndex}
import utils.{Dataset, DateUtils, Pipeline, SystemUtils}

import scala.math.pow

/*
These are the following arguments that need to be passed:
    --filecore string (name of file)

    --prefix_dir string (prefix of directory)

    --epsilon float (0 to 1.0)

    --percentage float (0 to 0.1)

    --ratio string (0 to 0.1)

    --threshold_level int (1 and above)

    --population int (1 and above)

    --num_gen int (1 and above)

    --avatmode string ("avatlite", "avat", "avatlite_idsa")

    --mode string ("cluster", "estimate")

    --clean_cache boolean (true, false)

    --enable_plot (true,false)

    --enable_log (true,false)
 */

object MainCVCChainLink extends App {
  /*
    1. Parsing arguments
   */
  //Default arguments
//  covid_19_literature_dim
  var fileCore = Array[String](
    "chainlink_3d_0.01_0.5"
  )
  var prefixDir = "./result/process/origin/data/"
  var epsilon = 0.01
  var percentage = 0.004
  var ratio = 0.03
  var thresholdLevel: Int = 1
  var population: Int = 50
  var numGen: Int = 150
  var avatMode = "avatlite"
  var mode = "cluster"
  var cleanCache = false
  var enableLog = false
  var enablePlot = true

  // for iteration
  val currentTime = DateUtils.getCurrentDateTime("yyyy-MM-dd HH:mm:ss")

  args.sliding(2, 2).toList.collect {
    case Array("--filecore", x: String)        => fileCore :+= x
    case Array("--prefix_dir", x: String)      => prefixDir = x
    case Array("--epsilon", x: String)         => epsilon = x.toDouble
    case Array("--percentage", x: String)      => percentage = x.toDouble
    case Array("--ratio", x: String)           => ratio = x.toDouble
    case Array("--threshold_level", x: String) => thresholdLevel = x.toInt
    case Array("--population", x: String)      => population = x.toInt
    case Array("--num_gen", x: String)         => numGen = x.toInt
    case Array("--avatmode", x: String)        => avatMode = x
    case Array("--mode", x: String)            => mode = x
    case Array("--clean_cache", x: String)     => cleanCache = x.toBoolean
    case Array("--enable_plot", x: String)     => enablePlot = x.toBoolean
    case Array("--enable_log", x: String)      => enableLog = x.toBoolean
  }

  /*
    2. Check clean cache
   */
  if (cleanCache) {
    SystemUtils.cleanDirectory()
  }

  if (fileCore.length != 0) {
    for (file <- fileCore) {
      println(s"percentage: ${percentage}")
      //declare pipeline
      var dataset: Dataset = new Dataset()
        .setPrefix(prefixDir)
        .setTargetFile(file)
        .readFile(",")

      var pipeline: Pipeline = new Pipeline()

      var clusterAlgo = new CVCModel()
        .setEpsilon(epsilon)
        .setPercentage(percentage)
        .setAVATMode(avatMode)
        .setRatio(ratio)
        .setThresholdValueForIDSA(thresholdLevel)
        .setPopulationForIDSA(population)
        .setNumGenerationForIDSA(numGen)
        .setMode(mode)
        .setReturnMode("origin")
        .setCalMode("memory-based")
        .enableLog(enableLog)
        .enablePlot(enablePlot)
        .setSamplingMode("improved")
        .setSamplingPhase("modification")

      pipeline.addOperator(clusterAlgo)

      //declaring metrics for evaluation
      var daviesBouldinIndex = new DaviesBouldinIndex()
      var dunnIndex = new DunnIndex()
      var cdbwIndex = new CDbwIndex()
      var silhouetteIndex = new SilhouetteIndex()
      if (avatMode != "avatlite_idsa") {
        pipeline.addOperator(daviesBouldinIndex)
        pipeline.addOperator(dunnIndex)
        pipeline.addOperator(cdbwIndex)
        pipeline.addOperator(silhouetteIndex)
      }

      //Initialize dataset

      //fit dataset into pipeline
      pipeline.fit(dataset)

      //transform
      pipeline.transform()

      val bw = new BufferedWriter(
        new FileWriter(new File("./result/log/cvc/result_" + file + ".txt"),
                       true))
      bw.write(
        "\n########################################## RESULT FINISHED ##########################################\n")
      bw.write(s"Testing time: ${currentTime}\n")
      bw.write(s"Epsilon: ${epsilon}\n")
      bw.write(s"Percentage: ${percentage}\n")
      bw.write(s"Number of cluster: ${clusterAlgo.getEstimated().deep}\n")
      bw.write(s"Dataset name: ${dataset.getFileCore()}\n")
      bw.write(s"Origin: ${dataset.length}\n")
      if (avatMode != "avatlite_idsa") {
        bw.write(s"DaviesBouldinIndex : ${daviesBouldinIndex.getValue()}\n")
        bw.write(s"DunnIndex : ${dunnIndex.getValue()}\n")
        bw.write(s"CDbwIndex : ${cdbwIndex.getValue()}\n")
        bw.write(s"SilhouetteIndex : ${silhouetteIndex.getValue()}\n")
      }
      val mb = 1024 * 1024
      val runtime = Runtime.getRuntime
      bw.write(
        "** Used Memory:  " + (runtime.totalMemory - runtime.freeMemory) / mb + " MB\n")
      bw.write("** Free Memory:  " + runtime.freeMemory / mb + " MB\n")
      bw.write("** Total Memory: " + runtime.totalMemory / mb + " MB\n")
      bw.write("** Max Memory:   " + runtime.maxMemory / mb + " MB\n")
      bw.write(
        s"Time elspased (without file logging) in seconds: ${pipeline.timeElapsed().toDouble / pow(10, 9)} seconds\n")
      bw.write(
        "#####################################################################################################\n")
      bw.close()

      clusterAlgo.closePythonConnection()
    }
  } else {
    println("Warning: No file cores specified")
  }
}
