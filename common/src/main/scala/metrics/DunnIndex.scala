package metrics

import scala.collection.mutable.Map
import utils._

import utils.{Dataset, DataPoint}

class DunnIndex extends Index[DunnIndex] {
  private var interClusterDistanceType
    : String = "min" //"min","max", "cen", "avg1", "avg2"
  private var intraClusterDistanceType: String = "max" //"max", "avg1", "avg2"

  def setInterClusterDistanceType(tp: String): DunnIndex = {
    if (tp != "max" && tp != "min" && tp != "cen" && tp != "avg1" && tp != "avg2") {
      println(s"Unrecognized distance type ${tp}. Using default: min")
      interClusterDistanceType = "min"
    } else {
      interClusterDistanceType = tp
    }
    return this
  }

  def setIntraClusterDistanceType(tp: String): DunnIndex = {
    if (tp != "max" && tp != "avg1" && tp != "avg2") {
      println(s"Unrecognized distance type ${tp}. Using default: max")
      intraClusterDistanceType = "max"
    } else {
      intraClusterDistanceType = tp
    }
    return this
  }

  def getInterClusterDistanceType(tp: String): String = {
    return interClusterDistanceType
  }

  def getIntraClusterDistanceType(tp: String): String = {
    return intraClusterDistanceType
  }

  private def findCentroid(i: Int,
                           grouper: Map[Int, Array[Int]],
                           dataset: Dataset): Array[Double] = {
    var sum = new Array[Double](dataset(grouper(i)(0)).coord.size)
    for (idx <- grouper(i)) {
      for (i <- 0 to dataset(grouper(i)(0)).coord.size - 1) {
        sum(i) = sum(i) + dataset(idx).coord(i)
      }
    }
    for (i <- 0 to sum.size - 1) {
      sum(i) = sum(i) / dataset(grouper(i)(0)).coord.size
    }
    return sum
  }

  private def singleLinkageDistance(i: Int,
                                    j: Int,
                                    grouper: Map[Int, Array[Int]],
                                    dataset: Dataset): Double = {
    var minDist = Double.MaxValue
    for (x <- grouper(i)) {
//      println("Get " + x)
      for (y <- grouper(j)) {
        var dist = distanceFunction(dataset(x), dataset(y))
        if (dist < minDist) {
          minDist = dist
        }
      }
    }
    return minDist
  }

  private def completeLinkageDistance(i: Int,
                                      j: Int,
                                      grouper: Map[Int, Array[Int]],
                                      dataset: Dataset): Double = {
    var maxDist = 0.0
    for (x <- grouper(i)) {
      for (y <- grouper(j)) {
        var dist = distanceFunction(dataset(x), dataset(y))
        if (dist > maxDist) {
          maxDist = dist
        }
      }
    }
    return maxDist
  }

  private def averageLinkageDistance(i: Int,
                                     j: Int,
                                     grouper: Map[Int, Array[Int]],
                                     dataset: Dataset): Double = {
    var averageDist = 0.0
    for (x <- grouper(i)) {
      for (y <- grouper(j)) {
        averageDist += distanceFunction(dataset(x), dataset(y))
      }
    }
    return averageDist / (grouper(i).size * grouper(j).size)
  }

  private def centroidLinkageDistance(i: Int,
                                      j: Int,
                                      grouper: Map[Int, Array[Int]],
                                      dataset: Dataset): Double = {
    var c1 = findCentroid(i, grouper, dataset)
    var c2 = findCentroid(j, grouper, dataset)
    return distanceFunction(new DataPoint(coord = c1),
                            new DataPoint(coord = c2))
  }

  private def averageCentroidLinkageDistance(i: Int,
                                             j: Int,
                                             grouper: Map[Int, Array[Int]],
                                             dataset: Dataset): Double = {
    var c1 = findCentroid(i, grouper, dataset)
    var c2 = findCentroid(j, grouper, dataset)
    var sumDistC1 = 0.0
    var sumDistC2 = 0.0
    for (index <- grouper(i)) {
      sumDistC1 += distanceFunction(new DataPoint(coord = c1), dataset(index))
    }
    for (index <- grouper(j)) {
      sumDistC2 += distanceFunction(new DataPoint(coord = c2), dataset(index))
    }
    return (sumDistC1 + sumDistC2) / (grouper(i).size + grouper(j).size)
  }

  private def completeDiameterDistance(i: Int,
                                       grouper: Map[Int, Array[Int]],
                                       dataset: Dataset): Double = {
    var maxValue = 0.0
    for (x <- grouper(i)) {
      for (y <- grouper(i)) {
        if (x != y) {
          var dist = distanceFunction(dataset(x), dataset(y))
          if (dist > maxValue) {
            maxValue = dist
          }
        }
      }
    }
    return maxValue
  }

  private def averageDiameterDistance(i: Int,
                                      grouper: Map[Int, Array[Int]],
                                      dataset: Dataset): Double = {
    var dist = 0.0
    for (x <- grouper(i)) {
      for (y <- grouper(i)) {
        if (x != y) {
          dist += distanceFunction(dataset(x), dataset(y))
        }
      }
    }
    return dist / (grouper(i).size * (grouper(i).size - 1))
  }

  private def centroidDiameterDistance(i: Int,
                                       grouper: Map[Int, Array[Int]],
                                       dataset: Dataset): Double = {
    var cen = findCentroid(i, grouper, dataset)
    var sumDist = 0.0
    for (x <- grouper(i)) {
      sumDist += distanceFunction(new DataPoint(coord = cen), dataset(x))
    }
    return (2 * sumDist) / grouper(i).size
  }

  def process(dataset: Dataset): Dataset = {
    value = 0.0
    //calculate metric
    var grouper = Map[Int, Array[Int]]()

    //group points that are in the same cluster
    for (dp <- dataset.getDataset()) {
      if (!grouper.keySet.contains(dp.cluster)) {
        grouper(dp.cluster) = Array[Int]()
      }
      grouper(dp.cluster) +:= dp.index
    }

    //get minimum inter cluster distance
    var interDist = Double.MaxValue
    var intraDist = 0.0
    for (i <- 0 until grouper.size) {
      println("process line" + i)
      for (j <- i + 1 until grouper.size) {
        println("process subline" + j)
        //inter
        var tmpInterDist = interClusterDistanceType match {
          case "max"  => completeLinkageDistance(i, j, grouper, dataset)
          case "min"  => singleLinkageDistance(i, j, grouper, dataset)
          case "cen"  => centroidLinkageDistance(i, j, grouper, dataset)
          case "avg1" => averageLinkageDistance(i, j, grouper, dataset)
          case "avg2" => averageCentroidLinkageDistance(i, j, grouper, dataset)
        }
        if (tmpInterDist < interDist) {
          interDist = tmpInterDist
        }
      }

      //intra
      var tmpIntraDist = intraClusterDistanceType match {
        case "max"  => completeDiameterDistance(i, grouper, dataset)
        case "avg1" => averageDiameterDistance(i, grouper, dataset)
        case "avg2" => centroidDiameterDistance(i, grouper, dataset)
      }
      if (tmpIntraDist > intraDist) {
        intraDist = tmpIntraDist
      }
    }

    value = interDist / intraDist
    println("Done Dunn")
    return dataset
  }
}
