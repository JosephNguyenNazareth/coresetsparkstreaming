package clustering

import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartPanel
import org.jfree.chart.ChartUtils
import org.jfree.chart.JFreeChart
import org.jfree.chart.plot.XYPlot
import org.jfree.data.xy.XYDataset
import org.jfree.data.xy.XYSeries
import org.jfree.data.xy.XYSeriesCollection

import scala.io.Source
import Double._
import math._
import java.io._
import util.control.Breaks._
import scala.collection.mutable.ArrayBuffer
import scala.math.Ordering
import sys.process._
import clustering._
import utils._

import utils.{DataPoint, Dataset}

class DBSCANModel extends ClusterAlgorithm[DBSCANModel] {
  private var epsilonDistance: Double = 0.0
  private var minPts: Int = 0

  override def toString: String = {
    return "dbscan"
  }

  def setEpsilon(eps: Double): DBSCANModel = {
    epsilonDistance = eps
    return this
  }

  def setMinPts(mPts: Int): DBSCANModel = {
    minPts = mPts
    return this
  }

  private def _rangeQuery(dataset: Dataset, point: Int): Array[Int] = {
    var neighbors: Array[Int] = Array[Int]()
    for (i <- 0 until dataset.length) {
      if (getDistance(dataset, point, i) <= epsilonDistance) {
        neighbors :+= i
      }
    }
    return neighbors
  }

  def fit(dataset: Dataset): Dataset = {
    //cluster == -2 -> it is noise
    val t0 = System.nanoTime()
    var c: Int = 0
    for (i <- 0 until dataset.length) {
      if (dataset(i).cluster == -1) {
        var neighbors: Array[Int] = _rangeQuery(dataset, i)
        if (neighbors.length < minPts) {
          dataset(i).cluster = -2
        } else {
          c = c + 1
          dataset(i).cluster = c
          var s: Array[Int] = Array[Int]()
          for (p <- neighbors) {
            if (dataset(p).index != dataset(i).index) {
              s :+= p
            }
          }
          for (p <- s) {
            if (dataset(p).cluster == -2) {
              dataset(p).cluster = c
            }
            if (dataset(p).cluster != -1) {
              dataset(p).cluster = c
              neighbors = _rangeQuery(dataset, p)
              if (neighbors.length >= minPts) {
                for (newP <- neighbors) {
                  s :+= newP
                }
              }
            }
          }
        }
      }
    }
    val t1 = System.nanoTime()
    //last step: convert all noise as newest cluster
    c = c + 1
    var clusterSet = Set[Int]()
    for (i <- 0 until dataset.length) {
      if (dataset(i).cluster == -2) {
        dataset(i).cluster = c
      }
      clusterSet += dataset(i).cluster
    }
    var clusterMap: Map[Int, Int] = Map()
    var f = 0
    for (e <- clusterSet) {
      clusterMap += (e -> f)
      f = f + 1
    }
    //map cluster to order values
    for (i <- 0 until dataset.length) {
      dataset(i).cluster = clusterMap(dataset(i).cluster)
    }
    //finish: begin write to file
    if (logEnable) {
      writeToFile(dataset)
    }
    if (plotEnable) {
      saveScatterChart(dataset)
    }
    //determine number of clusters
    duration = t1 - t0
    return dataset
  }
}
