import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
from hawks.generator import create_generator
import sys
import os
TARGET_SILHOUTTE = float(sys.argv[1])
EXAMPLE_NUM = int(sys.argv[2])
CLUSTER_NUM = int(sys.argv[3])
EQUAL_SIZE = True if sys.argv[4] == "True" else (
    False if sys.argv[4] == "False" else None)
OVERLAP = float(sys.argv[5])
OVERLAP_LIMIT = str(sys.argv[6])
DATASET_NAME = str(sys.argv[7])

fileDir = "../Datasets/" + DATASET_NAME + ".txt"
paramsFileDir = "../Datasets/" + DATASET_NAME + "_params.txt"
# Fix the seed number
config = {
    "hawks": {
        "folder_name": None,  # None
        "mode": "single",  # only
        "n_objectives": 1,  # can only be one
        "num_runs": 1,  # self-limit: from 1 to 50 (number of rerun)
        "seed_num": 100,  # can be changed
        "save_best_data": False,
        "save_stats": False,
        "plot_best": False,
        "save_plot": False
    },
    "objectives": {
        "silhouette": {
            "target": TARGET_SILHOUTTE  # 0 to 2
        }
    },
    "dataset": {
        "num_examples": EXAMPLE_NUM,  # 1 to 50
        "num_clusters": CLUSTER_NUM,
        "num_dims": 2,  # default for visualization
        "equal_clusters": EQUAL_SIZE,
        "min_clust_size": 3  # 3 < num_examples
    },
    "ga": {
        "num_gens": 50,  # 0 to 100
        "num_indivs": 10,
        "mut_method_mean": "random",
        "mut_args_mean": {  # mean
            "scale": 1.0,
            "dims": "each"
        },
        "mut_method_cov": "haar",  # only choice
        "mut_args_cov": {  # covariance
            "power": 0.3
        },
        "mut_prob_mean": "length",  # reccommend
        "mut_prob_cov": "length",  # reccomend
        "mate_scheme": "dv",  # variation operators
        "mate_prob": 0.7,
        "prob_fitness": 0.5,
        "elites": 0.2
    },
    "constraints": {
        "overlap": {
            "threshold": OVERLAP,
            "limit": OVERLAP_LIMIT
        },
        "eigenval_ratio": {
            "threshold": 20,
            "limit": "upper"
        }
    }
}
# Any missing parameters will take the default seen in configs/defaults.json
print("hello")
generator = create_generator(config)
# Run the generator
generator.run()
# Get the best dataset found and it's labels
datasets, label_sets = generator.get_best_dataset()
# Stored as a list for multiple runs

with open(fileDir, "w+") as buffer:
    data, labels = datasets[0], label_sets[0]
    i = 0
    for mem in data:
        the_string = str(mem[0]) + " " + str(mem[1])
        if i != len(data) - 1:
            the_string += "\n"
        buffer.write(the_string)
        i = i + 1
with open(paramsFileDir, "w+") as param:
    param.write("Target silhoutte: " + str(TARGET_SILHOUTTE) + "\n")
    param.write("Number of example: " + str(EXAMPLE_NUM) + "\n")
    param.write("number of cluster: " + str(CLUSTER_NUM) + "\n")
    param.write("Is clusters equal in size?: " + ("True" if EQUAL_SIZE ==
                                                  True else ("False" if EQUAL_SIZE == False else "Unknown")) + "\n")
    param.write("Overlap: " + str(OVERLAP) + "\n")
    param.write("OVerlap bound: " + OVERLAP_LIMIT + "\n")
