import numpy as np
from skimage import io, color
from scipy import ndimage
from skimage.filters import gaussian, threshold_otsu
from skimage.morphology import watershed
from skimage.feature import peak_local_max
from skimage.measure import regionprops, label
import sys, math, os, statistics, argparse
import json
import ast
import os
import redis
import cv2 as cv
import copy

parser = argparse.ArgumentParser(description='Argument parser for aVATLite process')
parser.add_argument('--file_core', default=None, type=str,
                    help='Name of file core for aVATLite detection')
parser.add_argument('--mode', default=None, type=str,
                    help='Mode for aVATlite')
parser.add_argument('--epsilon', default=0.5, type=float,
                    help='Epsilon used in sampling')
parser.add_argument('--percentage', default=0.8, type=float,
                    help='Percentage used in modification')
parser.add_argument('--ratio', default=0.0008, type=float,
                    help='Ratio used in removing small clusters')
parser.add_argument('--num_gen', default=150, type=int,
                    help='Name of file core for aVATLite detection')
parser.add_argument('--population', default=50, type=int,
                    help='population for IDSA')
parser.add_argument('--threshold_level', default=5, type=int,
                    help='number of threshold value resulting from IDSA')
parser.add_argument('--host', default='localhost', type=str,
                    help='host for query connection')
parser.add_argument('--port', default=6379, type=int,
                    help='port for query connection')
parser.add_argument('--db', default=0, type=int,
                    help='database index')


class Node(object):
    def __init__(self, value, level, repr_level, parent):
        self.value = value
        self.child = []
        self.level = level
        self.repr_level = repr_level
        self.parent = parent
        self.string_repr_level = " ".join([str(x) for x in repr_level])

    def __repr__(self):
        return "Node: {}, level: {}, child {}".format(self.value, self.level,
                                                      "({})".format(["\n" + repr(x) for x in self.child]))


class AVATLite(object):
    def __init__(self):
        self._file_core = None
        self._ratio = None
        self._epsilon = None
        self._percentage = None
        self._num_gen = None
        self._population = None
        self._threshold_level = None
        self._mode = None
        self._object_order_list = None
        self._image_file = None

    @staticmethod
    def objective_function(original_image, segment_image):
        # calculate rmse first
        if original_image.shape != segment_image.shape or len(original_image.shape) != 2:
            raise ValueError("Unequal shape")

        rmse = math.sqrt(abs(np.sum(np.subtract(original_image, segment_image)))
                         / float(original_image.shape[0] * original_image.shape[1]))

        return rmse

    def _idsa(self, input_image, min_bound=0.0, max_bound=255.0, d: int = 2, num_p: int = 50, max_gen: int = 200):
        # Set seed
        np.random.seed(1000)
        """
        Reference: A Global Multilevel something
        """
        # 1. Convert input image
        input_image = input_image.astype(np.float64)

        # 2. Initialize levels matrix
        levels = np.random.rand(num_p, d) * (max_bound - min_bound) + min_bound

        # best solution, along with objective best value
        best = None
        fitness = float('inf')
        pop_fitness = np.zeros((levels.shape[0],), dtype=np.float32)

        ### 3. Initial loop: calculate fitness for every population
        for x in range(levels.shape[0]):  # for each population
            current_fitness = 0.0
            for y in range(levels.shape[1]):  # for each dimension
                _, thresh = cv.threshold(input_image, levels[x][y], 255, cv.THRESH_BINARY)
                current_fitness += AVATLite.objective_function(input_image, thresh)
            pop_fitness[x] = current_fitness
            if fitness > current_fitness:
                fitness = current_fitness
                best = levels[x]

        ### 4. Generation loop
        # for each generation step
        for gen in range(max_gen):
            # rank vector
            ranker = np.argsort(pop_fitness)[::1]
            levels = levels[ranker]
            chosen_prob = ranker.astype(np.float32) / ranker.shape[0]
            r1 = 0
            for idx in range(levels.shape[0]):
                # A. Mutation step
                # calculate scale
                scale = np.random.gamma(2) * (2 * np.random.rand(1)[0]) * (np.random.rand(1)[0] - np.random.rand(1)[0])
                r1, r2 = np.random.randint(low=0, high=num_p, size=2)
                s = levels[r1] + scale * (levels[r2] - levels[idx])

                # B. Crossover step
                # calculate trial vector using binominal method
                trial_vector = np.zeros(levels.shape[1])
                for y in range(s.shape[0]):
                    first_rand = np.random.uniform()
                    second_rand = np.random.randint(0, levels.shape[1])
                    is_chosen = np.random.choice([0, 1])
                    if is_chosen:
                        trial_vector[y] = s[y]
                    else:
                        trial_vector[y] = levels[idx][y]

                # C. Selection step
                current_fitness = float('inf')
                for y in range(levels.shape[1]):  # for each dimension
                    _, thresh = cv.threshold(input_image, trial_vector[y], 255, cv.THRESH_BINARY)
                    current_fitness += AVATLite.objective_function(input_image, thresh)
                if pop_fitness[idx] > current_fitness:
                    pop_fitness[idx] = current_fitness
                    levels[idx] = trial_vector
                    if fitness > current_fitness:
                        best = trial_vector
                        fitness = current_fitness

        # Return result
        return best

    def update_rc(self, rc):
        self._rc = rc

    def _detect(self):
        # 0. Read files
        normal_img = io.imread(self._image_file)
        gray_img = color.rgb2gray(normal_img)

        # 1. transform image using monotonic function
        max_val = np.max(gray_img)
        min_val = np.min(gray_img)
        gray_img = (gray_img - min_val) / (max_val - min_val)
        h = gray_img.shape[0]
        w = gray_img.shape[1]
        mean_val = 0.0
        # pass 1: cal mean value
        for y in range(0, h):
            for x in range(0, w):
                mean_val = mean_val + gray_img[y, x]
        mean_val = mean_val / (h * w)

        # pass 2: transformation
        for y in range(0, h):
            for x in range(0, w):
                t = gray_img[y, x]
                gray_img[y, x] = 1 - math.exp(-t * t / (mean_val * mean_val))
        # pass 3: normalize
        max_val = np.max(gray_img)
        min_val = np.min(gray_img)
        gray_img = ((gray_img - min_val) / (max_val - min_val)) * 255
        gray_img = gray_img.astype("uint8")

        if self._mode == "avatlite_idsa" or self._mode == "avatlite_idsa_streaming":
            # Getting optimal threshold values using idsa
            multivalue_threshold = self._idsa(gray_img, min_bound=0, max_bound=255, d=self._threshold_level, num_p=self._population, max_gen=self._num_gen)
            multivalue_threshold = np.sort(multivalue_threshold)

            if self._mode == "avatlite_idsa":
                k_candidates = []
                cluster_list_candidates = []
                for i, threshold in enumerate(multivalue_threshold):
                    k, cluster_list, _ = self._avat_lite(gray_img, (threshold, i))
                    k_candidates.append(k)
                    cluster_list_candidates.append(cluster_list)
                self._k = k_candidates
                self._cluster_list = cluster_list_candidates
            elif self._mode == "avatlite_idsa_streaming":  # avatlite_idsa_merge
                # format for range cluster (left_range,right_range,[hirarchical_levels]
                cluster_list_candidates = []
                range_cluster_candidates = []
                existed_cluster = set([])
                ancestor_index = 0

                for i, threshold in enumerate(multivalue_threshold[::-1]):
                    # begin avatlite
                    _, cluster_list, cluster_image_range = self._avat_lite(gray_img, (threshold, i))
                    if i == 0:  # top nodes of tree
                        for t, (image_cluster, range_cluster) in enumerate(zip(cluster_list, cluster_image_range)):
                            # added clusters
                            existed_cluster.add(range_cluster)
                            # range and cluster list as a tree
                            range_cluster_candidates.append(Node(range_cluster, t, [t], -1))
                            cluster_list_candidates.append(Node(image_cluster, t, [t], -1))
                            ancestor_index = t
                    else:
                        for image_cluster, range_cluster in zip(cluster_list, cluster_image_range):
                            if range_cluster not in existed_cluster:
                                # get ancestors
                                ancestors = list(filter(
                                    lambda parent: range_cluster[0] >= parent[1].value[0] and range_cluster[1] <= parent[1].value[1],
                                    enumerate(range_cluster_candidates)
                                ))
                                # get parent
                                # print("comparison", range_cluster, range_cluster_candidates, ancestors)

                                if len(ancestors) > 0:
                                    last_parent = min(
                                        ancestors,
                                        key=lambda parent: (range_cluster[0] - parent[1].value[0]) + (
                                                parent[1].value[1] - range_cluster[1])
                                    )
                                    # get max index of siblings
                                    if len(last_parent[1].child) == 0:
                                        max_idx = 0
                                    else:
                                        max_idx = max([x.level for x in last_parent[1].child]) + 1
                                        ancestor_index += 1

                                    new_range_node = Node(range_cluster, max_idx, last_parent[1].repr_level + [max_idx],
                                                          last_parent[0])
                                    new_image_node = Node(image_cluster, max_idx, last_parent[1].repr_level + [max_idx],
                                                          last_parent[0])
                                    # update child for parents
                                    range_cluster_candidates[last_parent[0]].child.append(new_range_node)
                                    cluster_list_candidates[last_parent[0]].child.append(new_image_node)
                                    # add node
                                    existed_cluster.add(range_cluster)
                                    range_cluster_candidates.append(new_range_node)
                                    cluster_list_candidates.append(new_image_node)
                                else:
                                    ancestor_index += 1
                                    existed_cluster.add(range_cluster)
                                    # range and cluster list as a tree
                                    range_cluster_candidates.append(Node(range_cluster, ancestor_index, [ancestor_index], -1))
                                    cluster_list_candidates.append(Node(image_cluster, ancestor_index, [ancestor_index], -1))

                self._cluster_list = cluster_list_candidates
                self._k = len(self._cluster_list)
                self._range_cluster = range_cluster_candidates
        elif self._mode == "avatlite":
            self._k, self._cluster_list, _ = self._avat_lite(gray_img)

    def _update_result(self):
        result = {}
        # write down number cluster list
        # single k, a matrix of cluster list
        if self._mode == "avatlite":
            result['cluster'] = self._k
            for i, cluster in enumerate(self._cluster_list):
                result['cluster_{}'.format(i)] = " ".join([str(x) for x in cluster])
        elif self._mode == "avatlite_idsa":
            for i, (k, c_list) in enumerate(zip(self._k, self._cluster_list)):
                result['level_{}'.format(i)] = k
                for t, c in enumerate(c_list):
                    result['cluster_{}_{}'.format(i, t)] = " ".join([str(x) for x in c])
        elif self._mode == "avatlite_idsa_streaming":
            result['cluster'] = self._k
            for i, cluster in enumerate(self._cluster_list):
                pass
                # cluster = Node
                result['cluster_{}'.format(i)] = " ".join([str(x) for x in cluster.value])
                result['cluster_{}_parent'.format(i)] = cluster.parent
        result['status'] = 'done'
        self._rc.hmset(self._uuid, result)

    def _avat_lite(self, gray_img, threshold=None):
        # 1. BINARIZING STEP
        # SAVE IMAGE
        if self._mode == "avatlite":  # using otsu
            binary_img = gray_img < threshold_otsu(gray_img)
            binary_img_for_save = binary_img.astype('uint8') * 255
            io.imsave("./result/process/ivat_binarized/" + self._file_core + "_" + str(self._epsilon) + "_" + str(self._percentage) + "_binarized.png", binary_img_for_save)
        elif self._mode == "avatlite_idsa":
            threshold_value = threshold[0]
            iteration = threshold[1]
            binary_img = copy.deepcopy(gray_img)
            binary_img = binary_img < threshold_value
            binary_img_for_save = binary_img.astype('uint8') * 255
            io.imsave("./result/process/ivat_binarized/" + self._file_core + "_idsa_{}.png".format(iteration),
                      binary_img_for_save)
        elif self._mode == "avatlite_idsa_streaming":
            threshold_value = threshold[0]
            iteration = threshold[1]
            binary_img = copy.deepcopy(gray_img)
            binary_img = binary_img < threshold_value
            binary_img_for_save = binary_img.astype('uint8') * 255
            io.imsave("./result/process/ivat_binarized/" + self._file_core + "_idsa_streaming_{}.png".format(iteration),
                      binary_img_for_save)

        # 2. padding for watershed
        binary_img_pad = np.pad(binary_img, 1, mode='constant')

        # 3. Begin segmentation
        distance = ndimage.distance_transform_edt(binary_img_pad)
        local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3)),
                                    labels=binary_img_pad)
        markers, _ = ndimage.label(local_maxi)
        labels = watershed(-distance, markers, mask=binary_img_pad)
        regions = regionprops(labels)
        img_range = binary_img_pad.shape[0] - 2  # due to padding
        # remove some blocks to prevent adding noise
        filtered_regions = [x for x in regions if x.area /
                            (img_range * img_range) >= self._ratio]
        k = len(filtered_regions)
        cluster_list = []
        cluster_image_range = []
        # filters regions that are not square or almost square
        # NEED TO CHECK MERGE AND RESOLVE THEM!
        if k != 0:
            cluster_list = []
            block_range = []
            for props in filtered_regions:
                minr, minc, maxr, maxc = props.bbox
                left_range = int(math.ceil(float(minr + minc) / 2)) - 1
                right_range = int(math.floor(float(maxr + maxc) / 2)) - 1
                group_list = []
                block_range.append((left_range, right_range))
                for i in range(left_range, right_range):
                    group_list.append(self._object_order_list[i])
                cluster_list.append(group_list)
                cluster_image_range.append((left_range, right_range))
            k = len(cluster_list)

            # that's for highlight
            highlighter = np.full((gray_img.shape[0], gray_img.shape[1], 3), (107, 85, 51), 'uint8')
            color_holder = []
            for c in block_range:
                co = tuple(np.random.choice(range(256), size=3))
                while co in color_holder or co == (207, 185, 151):
                    co = tuple(np.random.choice(range(256), size=3))
                color_holder.append(co)  # new color
                highlighter[c[0]:c[1], c[0]:c[1], :] = co
        else:
            k = 1
            c_list = []
            for i in range(0, len(self._object_order_list)):
                c_list.append(self._object_order_list[i])
            cluster_list.append(c_list)
            cluster_image_range.append((0, len(self._object_order_list)))

            highlighter = np.full(gray_img.shape, 255, 'uint8')

        # highlight save
        if self._mode == "avatlite":
            io.imsave("./result/process/ivat_highlight/" + self._file_core + "_" + str(self._epsilon) + "_" + str(self._percentage) + "_avatlite.png", highlighter)
        elif self._mode == "avatlite_idsa" or self._mode == "avatlite_idsa_streaming":
            io.imsave("./result/process/ivat_highlight/" + self._file_core + "_{}_{}.png".format(self._mode, iteration),
                      highlighter)

        return k, cluster_list, cluster_image_range

    def doTask(self, query):
        """
        Input:
        query = {
            "id" : string,
            "file_core" : string,
            "mode" : string,
            "epsilon" : float,
            "percentage" : float,
            "ratio" : float,
            "num_gen" : int,
            "population" : int,
            "threshold_level" : int,
            "status" : "wait"
            "order" : string
        }
        """
        if query is None:
            result = {'status': 'fail'}
            self._rc.hmset(query["id"], result)
        self._uuid = query['id']
        self._file_core = query['file_core']
        self._mode = query['mode']
        self._ratio = float(query['ratio'])
        self._epsilon = float(query['epsilon'])
        self._percentage = float(query['percentage'])
        self._num_gen = int(query['num_gen'])
        self._population = int(query['population'])
        self._threshold_level = int(query['threshold_level'])
        self._image_file = os.path.join(os.getcwd(), "result/process/ivat/{0}_{1}_{2}.png".format(self._file_core, str(self._epsilon), str(self._percentage)))
        self._object_order_list = [int(x.strip()) for x in query['order'].split()]

        try:
            self._detect()
            self._update_result()
        except Exception as e:
            print(e)
            result = {'status': 'fail'}
            self._rc.hmset(query["id"], result)


def main():
    # 1. Getting parameters
    args = parser.parse_args()

    # Creating a python server
    try:
        avatlite_task = AVATLite()
        rc = redis.Redis(host=args.host, port=args.port, db=args.db)
        avatlite_task.update_rc(rc)
        # Fire to scala to signal that python has successfully connect to redis
        rc.set("avatlite_status", "success")
    except:
        exit()
    while True:  # Execution loop
        while True:
            # Check whether thread should finish
            is_join = rc.get("is_join")
            if is_join is not None:
                rc.delete("is_join")
                if is_join.decode("utf-8") == "true":
                    rc.set("is_close", "true")
                    exit()
            request = rc.get("avatlite_request")
            if request is not None:
                rc.delete("avatlite_request")
                break
        # decode to string
        request = request.decode("utf-8")
        # convert to dictionary
        query = ast.literal_eval(request)
        avatlite_task.doTask(query)


if __name__ == "__main__":
    main()
