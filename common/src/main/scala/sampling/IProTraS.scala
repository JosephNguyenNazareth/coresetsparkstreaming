package sampling

import scala.collection.mutable.ArrayBuffer
import utils.{DataPoint, Dataset, Measure, RepMaxHeap}

class IProTraS extends ProTraS {
  //PRIVATE ATTRIBUTES
  private var cosineFunction = (b: DataPoint, a: DataPoint, c: DataPoint) => {
    Measure.cosineAngleFromPoint(b, a, c)
  }

  override def setDistanceFunction(
      df: (DataPoint, DataPoint) => Double): IProTraS = {
    distanceFunction = df
    return this
  }

  def setCosineFunction(
      cf: (DataPoint, DataPoint, DataPoint) => Double): IProTraS = {
    cosineFunction = cf
    return this
  }

  def setPercentage(per: Double): IProTraS = {
    if (per < 0.0 || per > 1.0) {
      println(
        "Error setting percentage for the modified ProTraS: Please choose percentage that is between 0 and 1!")
    }
    percentage = per
    return this
  }

  override def enablePlot(e: Boolean): IProTraS = {
    super.enablePlot(e)
    return this
  }

  override def enableLog(e: Boolean): IProTraS = {
    super.enableLog(e)
    return this
  }

  override def setEpsilon(newEp: Double): IProTraS = {
    if (newEp <= 0.0) {
      println(
        "Error setting epsilon for the ProTraS: Please choose epsilon that is larger than 0!")
    }
    epsilon = newEp
    return this
  }

  override def fit(dataset: Dataset): Dataset = {
    val t0 = System.nanoTime()
    var coresetIndex: Array[Int] = Array[Int]()
    if (samplingPhase == "default")
      coresetIndex = fitForProTraS(dataset)
    else if (samplingPhase == "modification") {
      coresetIndex = dataset.getDataset().map(x => x.rep).toArray.distinct
      representative = readRepresentative(dataset)
    }

    if (coresetIndex.length > 1) {
      // otsu-based noise filter
      var filteredCoresetIndex = Array[Int]()
      var mean: Double = 0.0
      for ((k, v) <- representative) {
        mean = mean + v.length
      }
      mean = percentage * (mean / representative.size)
      for (i <- coresetIndex) {
        if (representative(i).length >= mean)
          filteredCoresetIndex :+= i
      }

      //overlap filtering
      //check if the point is shifted
      var wholeList: Array[Int] = filteredCoresetIndex
      var unshiftedList: Array[Int] = filteredCoresetIndex //original set
      var shiftedList: Array[Int] = Array[Int]()

      if (wholeList.length == 1)
        shiftedList = wholeList
      else {
        while (unshiftedList.length > 0) {
          var selfPoint: Boolean = false
          //1. get last element
          var theIndex: Int = unshiftedList(0)
          unshiftedList = unshiftedList.slice(1, unshiftedList.length)
          var currentShiftedList: Array[Int] = Array[Int]()
          var oldCurrentShiftedList: Array[Int] = Array[Int]()
          currentShiftedList :+= theIndex
          oldCurrentShiftedList :+= theIndex

          do {
            var minDist: Double = Double.MaxValue
            var minCoresetIndex: Int = -1
            //1. find index nearest to theIndex
            //find closet x in wholeList compared to currentShiftedList
            for (i <- currentShiftedList.indices) {
              theIndex = currentShiftedList(i)
              // prim based
              for (x <- wholeList) {
                if (x != theIndex) {
                  var distance: Double = getDistance(dataset, x, theIndex)
                  if (distance < minDist) {
                    minDist = distance
                    minCoresetIndex = x
                  }
                }
              }
            }

            //check if selfPoint should be true
            //if minCoresetIndex in the currentShiftedList itself, or it has been shifted before
            if (currentShiftedList.contains(minCoresetIndex) || shiftedList
                  .contains(minCoresetIndex)) {
              selfPoint = true
              //shift every point x in currentShiftedList
              for (x <- currentShiftedList) {
                //xem lai
                //groupDataPoint: List of point index belonging to rep point whose index is x
                var groupDataPoint: Array[Int] = Array[Int]()
                for (idx <- representative(x)) {
                  groupDataPoint :+= dataset(idx).index
                }

                //sameSideGroupVector: A list of Tuple3[DataPoint,DataPoint,DataPoint]
                //Tuple3[Int,Int,Int] : three points index that form an angle
                var threePointGroup: Array[(Int, Int, Int)] = groupDataPoint
                  .filter(t => t != x)
                  .map(t => Tuple3[Int, Int, Int](minCoresetIndex, x, t))

                //exclude group that only have rep as itself
                if (threePointGroup.length > 0) {
                  // An Array tof Tuple2[Int,Double]
                  //Int: index of point of rep set, excluding rep itself/
                  //Double: closeness, cosine value between vector(x,rep) and vector(x,minCoresetIndex)
                  //this array is sort from smallest double to largest double
                  var newGroupArrayTemp: Array[(Int, Double)] =
                    threePointGroup
                      .map(
                        g =>
                          (g._3,
                           cosineFunction(dataset(g._1),
                                          dataset(g._2),
                                          dataset(g._3))))
                      .sortBy(g => (g._2, g._1))

                  //get the index
                  var percentageCosine: Int =
                    (percentage * (newGroupArrayTemp.length - 1)).toInt

                  //get threshold value from index
                  val percentageCosineValue = newGroupArrayTemp(
                    percentageCosine)._2

                  //get datapoint that form a cosine value larger than threshold
                  //the larger the percentage, the larger the threshold value, the more compact cluster is
                  var newGroupArray: Array[DataPoint] = newGroupArrayTemp
                    .filter(x => x._2 >= percentageCosineValue)
                    .map(x => dataset(x._1))

                  //there is other points
                  // Using percentage to get the point that is nth close to rep
                  var newCoresetIndex: Int = -1
                  if (newGroupArray.length > 0) {
                    var groupHeap: RepMaxHeap = new RepMaxHeap(newGroupArray)
                    var order: Int =
                      ((1 - percentage) * (groupHeap.length + 1)).toInt
                    if (order >= groupHeap.length) {
                      order = groupHeap.length - 1
                    }
                    newCoresetIndex = groupHeap.kTHLargestDataPoint(order).index
                  }

                  //update representative
                  if (newCoresetIndex != -1) {
                    var theList: ArrayBuffer[Int] = representative(x).clone
                    representative += (newCoresetIndex -> theList)
                    representative = representative.filterKeys(_ != x)
                    //update wholeList
                    var wholeListIndex: Int = wholeList.indexOf(x)
                    wholeList(wholeListIndex) = newCoresetIndex
                    //update currentShiftedList
                    var currentShiftedListIndex: Int =
                      currentShiftedList.indexOf(x)
                    currentShiftedList(currentShiftedListIndex) =
                      newCoresetIndex
                  }
                }
              }
            } else {
              currentShiftedList :+= minCoresetIndex
              oldCurrentShiftedList :+= minCoresetIndex
            }
          } while (!selfPoint);

          //add currentShiftedList to shiftedList, remove them from unshiftedList
          for (i <- currentShiftedList) {
            shiftedList :+= i
          }
          unshiftedList =
            unshiftedList.filter(x => !oldCurrentShiftedList.contains(x))
        }
      }

      for (targetIdx <- 0 until dataset.length) {
        //For original dataset
        var minDist: Double = Double.MaxValue
        var repIdx: Int = -1
        //for each coresetIndex
        for (k <- shiftedList) {
          var distance: Double = getDistance(dataset, k, targetIdx)
          if (distance < minDist) {
            minDist = distance
            repIdx = k
          }
        }
        //update coreset
        dataset(targetIdx).rep = repIdx
        dataset(targetIdx).disToRep = minDist

        /** ****************************************************** */
      }

      //update sampling dataset with shiftedList
      var sampling = new Dataset()
      for (i <- shiftedList) {
        sampling :+= dataset(i)
      }
      val t1 = System.nanoTime()
      duration = t1 - t0
      representative = Map()

      sampling.setTargetFile(dataset.getFileCore() + "_improved")
      return sampling
    } else {
      representative = Map()
      val sampling: Dataset = dataset.clone
      sampling.setTargetFile(dataset.getFileCore() + "_improved")
      return sampling
    }
  }
}
