package metrics

import scala.collection.mutable.Map
import utils._

import utils.{DataPoint, Dataset}

class DaviesBouldinIndex extends Index[DaviesBouldinIndex] {
  def process(dataset: Dataset): Dataset = {
    value = 0.0
    //calculate metric
    var grouper = Map[Int, Array[Int]]()

    //group points that are in the same cluster
    for (dp <- dataset.getDataset()) {
      if (!grouper.keySet.contains(dp.cluster)) {
        grouper = grouper + (dp.cluster -> Array[Int]())
      }
      grouper(dp.cluster) :+= dp.index
    }

    //calculate centroids of each cluster
    var centroid = Map[Int, Array[Double]]()
    var averageDist = Map[Int, Double]()

    for ((k, v) <- grouper) {
      var sum = new Array[Double](dataset(v(0)).coord.size)
      for (idx <- v) {
        for (i <- dataset(v(0)).coord.indices) {
          sum(i) = sum(i) + dataset(idx).coord(i)
        }
      }
      for (i <- sum.indices) {
        sum(i) = sum(i) / v.size
      }
      centroid = centroid + (k -> sum)
      averageDist = averageDist + (k -> 0.0)
      for (idx <- v) {
        averageDist(k) += distanceFunction(new DataPoint(coord = sum),
                                           dataset(idx))
      }
      averageDist(k) = averageDist(k) / v.size

    }

    //for each cluster
    for ((i, vi) <- grouper) {
      var maxValue = 0.0
      for ((j, vj) <- grouper) {
        if (i != j) {
          var tmpValue = (averageDist(i) + averageDist(j)) / distanceFunction(
            new DataPoint(coord = centroid(i)),
            new DataPoint(coord = centroid(j)))
          if (tmpValue > maxValue) {
            maxValue = tmpValue
          }
        }
      }
      value += maxValue
    }

    //divide by n
    value /= grouper.size
    println("Done Davies-Bouldin")

    return dataset
  }

}
