package configuration

import java.io.{FileInputStream, InputStream}
import org.yaml.snakeyaml.Yaml

object YAMLConfig {
  private val configDir = "./resources"
  private val filename = configDir + "/coreset-spark-streaming.yaml"
  var file: InputStream = null
  if (configDir.isEmpty) {
    file = getClass.getResourceAsStream(filename)
  } else {
    file = new FileInputStream(filename)
  }

  def get(name: String): Any = { return mainConfig.get(name) }

  private val yaml = new Yaml()
  private val mainConfig =
    yaml.load(file).asInstanceOf[java.util.LinkedHashMap[String, Any]]

  // val kafka = mainConfig.get("kafka").asInstanceOf[java.util.LinkedHashMap[String, Any]]
}
