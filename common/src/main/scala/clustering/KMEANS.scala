package clustering

import utils.{Dataset, DataPoint}
import utils.Measure
import scala.collection.mutable.ArrayBuffer

class KMEANS extends ClusterAlgorithm[KMEANS] {
  private var k: Int = 5
  private var iteration: Int = 100
  private var coreset: Array[Array[Int]] = Array[Array[Int]]()
  private var cheatMode: String = "naive"
  private var finalCluster: Int = 0

  def getClustes(): Int = {
    return finalCluster
  }

  override def toString: String = {
    return "kmeans"
  }

  def setK(newK: Int): KMEANS = {
    k = newK
    return this
  }

  def setCoreset(newCoreset: Array[Array[Int]]): KMEANS = {
    coreset = newCoreset
    return this
  }

  def setIteration(newIteration: Int): KMEANS = {
    iteration = newIteration
    return this
  }

  def setCheatMode(newCheatMode: String): KMEANS = {
    cheatMode = newCheatMode
    return this
  }

  private def getInitialCentroid(dataset: Dataset): Array[DataPoint] = {
    var initialCentroid: Array[DataPoint] = Array[DataPoint]()
    if (cheatMode == "naive") {
      // just pick random centroids
      var coresetList: Array[Int] = Array[Int]()
      for (x <- 0 until k) {
        coresetList ++= coreset(x)
      }
      for (x <- 0 until k) {
        val nextIndex: Int = coresetList(
          scala.util.Random.nextInt(coresetList.length))
        initialCentroid :+= dataset(nextIndex)
      }
    } else if (cheatMode == "cheat") {
      val listDataPoint: Array[DataPoint] = dataset.getDataset()
      // pick each initialised centroid from each cluster
      // first iterate through the whole dataset to create a map of centroids->[list datapoints]
      // then iterate through each centroids list and pick new random centroids

//      var clusterCollection: Map[Int, ArrayBuffer[Int]] =
//        Map[Int, ArrayBuffer[Int]]()
//      val dataPoints: Array[DataPoint] = coreset.getDataset()
//
//      println(dataPoints.length)
//      println(dataPoints(0).cluster)
//
//      for (x <- dataPoints.indices) {
//        if (clusterCollection.contains(dataPoints(x).cluster)) {
//          clusterCollection(dataPoints(x).cluster) += dataPoints(x).index
//        } else {
//          clusterCollection += (dataPoints(x).cluster -> ArrayBuffer[Int](
//            dataPoints(x).index))
//        }
//      }
//
//      for ((k, _) <- clusterCollection) {
//        println("key: ", k)
//      }

      println("Number of clusters: " + k)
      println(coreset.length)
      for (x <- 0 until k) {
        val a = scala.util.Random.nextInt(coreset(x).length)
        println(a)
        val nextIndex: Int = coreset(x)(a)
        println(nextIndex)
//        println(listDataPoint.mkString("\n"))
        initialCentroid :+= listDataPoint.filter(x => x.index == nextIndex)(0)
      }
    }

    return initialCentroid
  }

  private def assignCentroid(
      centroidList: Array[DataPoint],
      dataset: Dataset): Map[DataPoint, ArrayBuffer[DataPoint]] = {
    val dataPointList: Array[DataPoint] = dataset.getDataset()
    var newDataPointList: Map[DataPoint, ArrayBuffer[DataPoint]] =
      Map[DataPoint, ArrayBuffer[DataPoint]]()

    for (currentCentroid <- centroidList) {
      newDataPointList += (currentCentroid -> ArrayBuffer[DataPoint](
        currentCentroid))
    }

    for (point <- dataPointList) {
      if (!centroidList.contains(point)) {
        val distanceFromCurrentCentroids: Array[Double] =
          centroidList.map(x => Measure.euclide(x, point))
        val minCentroidIndex: Int =
          distanceFromCurrentCentroids.indexOf(distanceFromCurrentCentroids.min)
        val minCentroid: DataPoint = centroidList(minCentroidIndex)
        newDataPointList(minCentroid) += point
      }
    }

    return newDataPointList
  }

  private def updateCentroid(
      datapointWithCentroid: Map[DataPoint, ArrayBuffer[DataPoint]])
    : Array[DataPoint] = {
    var newCentroidList: Array[DataPoint] = Array[DataPoint]()
    for ((currentCentroid, value) <- datapointWithCentroid) {
      var newCentroid: Array[Double] = Array[Double]()
      val numPointInCluster: Int = value.length
      for (index <- currentCentroid.coord.indices) {
        val newAxeValue: Double = value
          .map(x => x.coord(index))
          .sum / numPointInCluster
        newCentroid :+= newAxeValue
      }
      newCentroidList :+= new DataPoint(coord = newCentroid)
    }

    return newCentroidList
  }

  def fit(dataset: Dataset): Dataset = {
    //cluster == -2 -> it is
    val t0 = System.nanoTime()
    var centroids: Array[DataPoint] = getInitialCentroid(dataset)
    var datapointWithCentroid: Map[DataPoint, ArrayBuffer[DataPoint]] =
      Map[DataPoint, ArrayBuffer[DataPoint]]()

    var countIteration: Int = iteration
    while (countIteration > 0) {
      // calculate the distance between points and its centroid
      datapointWithCentroid = assignCentroid(centroids, dataset)
      centroids = updateCentroid(datapointWithCentroid)

      countIteration -= 1
    }

    var clusterID: Int = 0
    val dataPointList: Array[DataPoint] = dataset.getDataset()
    for ((_, pointList) <- datapointWithCentroid) {
      for (point <- pointList) {
        val pointIndex: Int = dataPointList.indexOf(point)
        if (pointIndex != -1)
          dataset(pointIndex).cluster = clusterID
      }
      clusterID += 1
    }

    val t1 = System.nanoTime()
    //last step: convert all noise as newest cluster
    clusterID = clusterID + 1
    var clusterSet = Set[Int]()
    for (i <- 0 until dataset.length) {
      if (dataset(i).cluster == -2) {
        dataset(i).cluster = clusterID
      }
//      clusterSet += dataset(i).cluster
    }
    finalCluster = clusterID
//    var clusterMap: Map[Int, Int] = Map()
//    var f = 0
//    for (e <- clusterSet) {
//      clusterMap += (e -> f)
//      f = f + 1
//    }
//    //map cluster to order values
//    for (i <- 0 until dataset.length) {
//      dataset(i).cluster = clusterMap(dataset(i).cluster)
//    }
    //finish: begin write to file
    if (logEnable) {
      writeToFile(dataset)
    }
    if (plotEnable) {
      saveScatterChart(dataset)
    }
    //determine number of clusters
    duration = t1 - t0
    return dataset
  }
}
