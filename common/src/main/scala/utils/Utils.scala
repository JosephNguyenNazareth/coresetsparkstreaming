package utils

import math.{sqrt, abs, pow, log10, exp, min, max}
import java.io.File
import org.apache.commons.io.FileUtils
import org.apache.commons.text.similarity.CosineDistance
import java.util.{Calendar, Date, Locale, TimeZone}

object SystemUtils {
  def cleanDirectory() = {
    //-1. Clear .log folder
    if (scala.reflect.io.File("./.log").isDirectory)
      FileUtils.cleanDirectory(new java.io.File("./.log"))
    else
      new java.io.File("./.log").mkdirs()

    if (scala.reflect.io.File("./result/cvc").isDirectory) {
      FileUtils.cleanDirectory(new java.io.File("./result/cvc/data"))
      FileUtils.cleanDirectory(new java.io.File("./result/cvc/image"))
    } else {
      new java.io.File("./result/cvc/data").mkdirs()
      new java.io.File("./result/cvc/image").mkdirs()
    }

    if (scala.reflect.io.File("./result/dbscan").isDirectory) {
      FileUtils.cleanDirectory(new java.io.File("./result/dbscan/data"))
      FileUtils.cleanDirectory(new java.io.File("./result/dbscan/image"))
    } else {
      new java.io.File("./result/dbscan/data").mkdirs()
      new java.io.File("./result/dbscan/image").mkdirs()
    }

    if (scala.reflect.io.File("./result/denclue").isDirectory) {
      FileUtils.cleanDirectory(new java.io.File("./result/denclue/data"))
      FileUtils.cleanDirectory(new java.io.File("./result/denclue/image"))
    } else {
      new java.io.File("./result/denclue/data").mkdirs()
      new java.io.File("./result/denclue/image").mkdirs()
    }

    if (scala.reflect.io.File("./result/optics").isDirectory) {
      FileUtils.cleanDirectory(new java.io.File("./result/optics/data"))
      FileUtils.cleanDirectory(new java.io.File("./result/optics/image"))
    } else {
      new java.io.File("./result/optics/data").mkdirs()
      new java.io.File("./result/optics/image").mkdirs()
    }

    if (scala.reflect.io.File("./result/streaming").isDirectory) {
      FileUtils.cleanDirectory(new java.io.File("./result/streaming/data"))
      FileUtils.cleanDirectory(new java.io.File("./result/streaming/image"))
    } else {
      print(new java.io.File("./result/streaming/data").mkdirs())
      new java.io.File("./result/streaming/image").mkdirs()
    }

    if (scala.reflect.io.File("./result/process/avat_segment").isDirectory)
      FileUtils.cleanDirectory(
        new java.io.File("./result/process/avat_segment"))
    else
      new java.io.File("./result/process/avat_segment").mkdirs()
    if (scala.reflect.io.File("./result/process/coreset/data").isDirectory)
      FileUtils.cleanDirectory(
        new java.io.File("./result/process/coreset/data"))
    else
      new java.io.File("./result/process/coreset/data").mkdirs()
    if (scala.reflect.io.File("./result/process/coreset/image").isDirectory)
      FileUtils.cleanDirectory(
        new java.io.File("./result/process/coreset/image"))
    else
      new java.io.File("./result/process/coreset/image").mkdirs()
    if (scala.reflect.io.File("./result/process/ivat").isDirectory)
      FileUtils.cleanDirectory(new java.io.File("./result/process/ivat"))
    else
      new java.io.File("./result/process/ivat").mkdirs()
    if (scala.reflect.io.File("./result/process/vat").isDirectory)
      FileUtils.cleanDirectory(new java.io.File("./result/process/vat"))
    else
      new java.io.File("./result/process/vat").mkdirs()
    if (scala.reflect.io.File("./result/process/ivat_binarized").isDirectory)
      FileUtils.cleanDirectory(
        new java.io.File("./result/process/ivat_binarized"))
    else
      new java.io.File("./result/process/ivat_binarized").mkdirs()
    if (scala.reflect.io.File("./result/process/ivat_preprocessed").isDirectory)
      FileUtils.cleanDirectory(
        new java.io.File("./result/process/ivat_preprocessed"))
    else
      new java.io.File("./result/process/ivat_preprocessed").mkdirs()
    if (scala.reflect.io.File("./result/process/origin/data").isDirectory)
      FileUtils.cleanDirectory(new java.io.File("./result/process/origin/data"))
    else
      new java.io.File("./result/process/origin/data").mkdirs()
    if (scala.reflect.io.File("./result/process/origin/image").isDirectory)
      FileUtils.cleanDirectory(
        new java.io.File("./result/process/origin/image"))
    else
      new java.io.File("./result/process/origin/image").mkdirs()
    if (scala.reflect.io.File("./result/process/ivat_highlight").isDirectory)
      FileUtils.cleanDirectory(
        new java.io.File("./result/process/ivat_highlight"))
    else
      new java.io.File("./result/process/ivat_highlight").mkdirs()
  }
}

class RepMaxHeap(private var h: Array[DataPoint]) {
  /*
    a DSA class for a max heap that helps finding
    the max value in the list
   */

  private var heapSize: Int = h.length
  var heap: Array[DataPoint] = heapify(h)

  private def _removeMax(hp: Array[DataPoint]): Array[DataPoint] = {
    var tmpHp = hp
    var point: DataPoint = hp(0)
    tmpHp(0) = hp(heapSize - 1)
    heapSize = heapSize - 1
    tmpHp = _heapify(tmpHp, 0)
    return tmpHp
  }
  private def _heapify(hp: Array[DataPoint], i: Int): Array[DataPoint] = {
    var l = 2 * i + 1
    var r = 2 * i + 2
    var m = i
    if (l < heapSize) {
      if (hp(i).disToRep < hp(l).disToRep) {
        m = l
      }
    }
    if (r < heapSize) {
      if (hp(m).disToRep < hp(r).disToRep) {
        m = r
      }
    }
    if (m != i) {
      //swap
      var tmp: DataPoint = hp(i)
      hp(i) = hp(m)
      hp(m) = tmp
      return _heapify(hp, m)
    } else {
      return hp
    }
  }

  private def heapify(hp: Array[DataPoint]): Array[DataPoint] = {
    var tmpHp = hp
    var startIdx: Int = (heapSize / 2) - 1
    for (i <- startIdx to 0 by -1) {
      tmpHp = _heapify(tmpHp, i)
    }
    return tmpHp
  }

  def kTHLargestDataPoint(k: Int): DataPoint = {
    var tmpHeap: Array[DataPoint] = heap.clone
    var order: Int = k
    var oldHeapSize: Int = heapSize
    if (order > heapSize) {
      order = heapSize
    }
    if (order < 1) {
      order = 1
    }
    for (i <- 1 to order - 1) {
      tmpHeap = _removeMax(tmpHeap)
    }
    var result = tmpHeap(0)
    heapSize = oldHeapSize
    return result
  }

  def length(): Int = {
    return heapSize
  }

}
//object represent measurements
object Measure {
  private var cosineDistanceFunction =
    (b: DataPoint, a: DataPoint, c: DataPoint) => {
      Measure.cosineAngleFromPoint(b, a, c)
    }
  //private function
  private def makeVector(firstPoint: DataPoint,
                         secondPoint: DataPoint): Array[Double] = {
    val newVector: Array[Double] = new Array[Double](firstPoint.coord.length)
    for (row <- firstPoint.coord.indices) {
      newVector(row) = secondPoint.coord(row) - firstPoint.coord(row)
    }
    return newVector
  }

  private def cosineAngle(firstVector: Array[Double],
                          secondVector: Array[Double]): Double = {
    var numerator: Double = 0.0
    var firstDenominator: Double = 0.0
    var secondDenominator: Double = 0.0

    for (row <- firstVector.indices) {
      numerator += firstVector(row) * secondVector(row)
      firstDenominator += firstVector(row) * firstVector(row)
      secondDenominator += secondVector(row) * secondVector(row)
    }
    return numerator / (sqrt(firstDenominator) * sqrt(secondDenominator))
  }
  /*
    An object defining measure for use
   */

  def euclide(first: DataPoint, second: DataPoint): Double = {
    val firstPoint = first.coord
    val secondPoint = second.coord
    var distance: Double = 0.0
    for (col <- firstPoint.indices) {
      distance += pow(firstPoint(col) - secondPoint(col), 2)
    }
    return sqrt(distance)
  }

  def manhattan(first: DataPoint, second: DataPoint): Double = {
    val firstPoint = first.coord
    val secondPoint = second.coord
    var distance: Double = 0.0
    for (col <- firstPoint.indices) {
      distance += abs(firstPoint(col) - secondPoint(col))
    }
    return distance
  }

  def cosineAngleFromPoint(b: DataPoint, a: DataPoint, c: DataPoint): Double = {
    //calculate cosine angle of bac
    return cosineAngle(makeVector(a, b), makeVector(a, c))
  }

  def cosineAngleFromText(b: DataPoint, a: DataPoint, c: DataPoint): Double = {
    //assumed that the 3 points here form a triangle
    var dAB = normalizedLevenshteinDistance(a, b)
    var dAC = normalizedLevenshteinDistance(a, c)
    var dBC = normalizedLevenshteinDistance(b, c)
    var cosineValue = (pow(dAB, 2) + pow(dAC, 2) - pow(dBC, 2)) / (2 * dAB * dAC)
    if (cosineValue < -1) {
      cosineValue = -1
    } else if (cosineValue > 1) {
      cosineValue = 1
    }
    return cosineValue
  }

  def mean(group: Array[DataPoint]): DataPoint = {
    var meanPoint: Array[Double] = new Array[Double](group(0).length)
    var groupSize: Int = group.length
    for (mem <- group) {
      for (t <- 0 to mem.length - 1) {
        meanPoint(t) = meanPoint(t) + (mem.coord(t) / groupSize)
      }
    }
    return new DataPoint(coord = meanPoint)
  }

  var log2 = (x: Double) => log10(x) / log10(2.0)

  def textDistance(t1: DataPoint, t2: DataPoint): Double = {
    var docDistance = new CosineDistance()
    return docDistance.apply(t1.tweet.content, t2.tweet.content)
  }
  //this should be done on GPU
  def normalizedLevenshteinDistance(d1: DataPoint, d2: DataPoint): Double = {
    //create a prefix matrix for edit distance
    val lenD1 = d1.tweet.content.length
    val lenD2 = d2.tweet.content.length
    val prefixMatrix = Array.ofDim[Int](lenD1 + 1, lenD2 + 1)
    for (i <- 0 to lenD1) {
      for (j <- 0 to lenD2) {
        if (i == 0) {
          prefixMatrix(i)(j) = j
        } else if (j == 0) {
          prefixMatrix(i)(j) = i
        } else {
          //calculate cost for insertion, deletion and match
          val insertion = prefixMatrix(i)(j - 1) + 1
          val deletion = prefixMatrix(i - 1)(j) + 1
          var matchCost = prefixMatrix(i - 1)(j - 1)
          if (d1.tweet.content(i - 1) != d2.tweet.content(j - 1)) {
            matchCost += 1
          }
          prefixMatrix(i)(j) = min(insertion, min(deletion, matchCost))
        }
      }
    }
    return prefixMatrix(lenD1)(lenD2).toDouble / List(lenD1, lenD2).max
  }
}

object Normalise {
  /*
    An object containing functions to normalize
    an array of double
   */
  def zScoreNormalise(pattern: Array[Double],
                      mean: Double,
                      sigma: Double): Array[Double] = {
    val normPattern: Array[Double] = pattern.map(x => (x - mean) / sigma)

    return normPattern
  }

  def minMaxNormalise(pattern: Array[Double],
                      axis: Int,
                      minRange: Double = 0.0,
                      maxRange: Double = 1.0): Array[Double] = {
    var normPattern: Array[Double] = Array[Double]()
    for (i <- 0 to axis - 1) {
      normPattern :+= pattern(i)
    }
    normPattern :+= (pattern(axis) - minRange) / (maxRange - minRange)
    for (i <- axis + 1 to pattern.length - 1) {
      normPattern :+= pattern(i)
    }
    return normPattern
  }
}

object DateUtils {
  def getCurrentDateTime(format: String): String = {
    var dateFormat = new java.text.SimpleDateFormat(format);
    var date = new Date();
    return dateFormat.format(date);
  }

  def convertStrDateToFormat(strDate: String,
                             fromFormat: String,
                             toFormat: String): String = {
    var date = convertToCalendar(strDate, fromFormat);
    return getDateString(date, toFormat);
  }

  def convertToCalendar(strDate: String, format: String): Calendar = {
    val formatDate = new java.text.SimpleDateFormat(format)
    var date = formatDate.parse(strDate)
    var c = Calendar.getInstance();
    c.setTime(date)
    return c
  }

  def getDateString(time: Calendar, format: String): String = {
    val formatDate = new java.text.SimpleDateFormat(format)
    return formatDate.format(time.getTime)
  }
}
