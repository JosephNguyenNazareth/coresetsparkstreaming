package test

import metrics.{CDbwIndex, DaviesBouldinIndex, DunnIndex, SilhouetteIndex}
import utils.{DataPoint, Dataset, Pipeline, SystemUtils}
import java.io.{BufferedWriter, File, FileWriter}

import org.apache.spark.SparkContext
import org.apache.spark.sql.{Column, DataFrame, SQLContext}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{
  monotonicallyIncreasingId,
  col,
  lit,
  row_number
}

import scala.util.control.Breaks._
import streaming.StreamingModel
import configuration.BaseContext

import scala.collection.JavaConversions.collectionAsScalaIterable
import utils.DataFile

object SimulationOfflineBig extends BaseContext with Serializable {
  val TRACE: Boolean = true
  // demarrer la Spark Session de processus
  val conf = buildSparkConf("Simulation Streaming Benchmark Big Cross")
  conf.set("spark.ui.port", "9050")
  conf.set("spark.executor.memory", "8g")
  conf.set("spark.executor.cores", "4")
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")
  val sqlContext = new SQLContext(sc)
  val spark = sqlContext.sparkSession

  def main(args: Array[String]): Unit = {
    // Default arguments
    var datafile: Array[DataFile] = Array[DataFile]();
    datafile :+= new DataFile("BigCross.data", -1, 10000, 1000, 0.6)

    var prefixDir = "./datasets/streamkm/"
    var cleanCache = false
    var datasetSize: Long = 0

    if (cleanCache) {
      SystemUtils.cleanDirectory()
    }
    var counter: Int = 0
    if (datafile.length != 0) {
      for (i <- datafile.indices) {
        val file: String = datafile(i).fileCore
        println(
          "======= Streaming Coreset Clustering on dataset " + file + " =======")
        //Initialize dataset
        var row: Int = 0;
        val batchSize: Int = datafile(i).batchSize
        StreamingModel.setAlgorithm("corekmeans")
        StreamingModel.setBatchSize(datafile(i).streamingBatchSize)
        StreamingModel.setEpsilon(datafile(i).epsilon)
        StreamingModel.setConstant(2)

        var data: DataFrame = spark.read
          .format("csv")
          .option("header", value = false)
          .option("delimiter", value = ",")
          .load(prefixDir + file + datafile(i).extension)

        data = data
          .withColumn("id", monotonicallyIncreasingId)
          .withColumn("rank",
                      row_number().over(Window.partitionBy().orderBy("id")))
          .drop("id")
          .cache()
        datasetSize = data.count()
        val t1 = System.nanoTime()

        // var loopIndex: Int = 1
        breakable {
          while (true) {
            val preprareData: DataFrame =
              data.filter(
                col("rank") >= lit(row) && col("rank") < lit(row + batchSize))
            val dataLeft: Dataset =
              furtherETL(preprareData)
            if (dataLeft.length() <= 0) {
              // println(preprareData.count())
              break
            }

            dataLeft.setTargetFile(file)
            StreamingModel.parseDataStreaming(dataLeft)
            row = row + batchSize
            data = data.filter(col("rank") >= lit(row))
            println("Processed line " + row)
          }
        }
        var clusterData: Dataset = new Dataset()
        if (TRACE) {
//          val sampleData: Dataset = StreamingModel.getOutput()
          StreamingModel.clustering()
          clusterData = StreamingModel.getOutput()
          StreamingModel.writeToFileStreaming(clusterData)
//          StreamingModel.saveScatterChart(clusterData)

          val t2 = System.nanoTime()
          val duration: Double = (t2 - t1) / 1000000000.0
          StreamingModel.closeConnection()

          // time for some metrics
          // first reassign point index
          val dataPointProcess: Array[DataPoint] = clusterData.getDataset()
          for (x <- 0 until clusterData.length()) {
            dataPointProcess(x).index = x
          }
          val clusterDataReassign: Dataset = new Dataset(dataPointProcess)

          val pipeline: Pipeline = new Pipeline()
          val daviesBouldinIndex = new DaviesBouldinIndex()
          val dunnIndex = new DunnIndex()
          val silhouetteIndex = new SilhouetteIndex()

          pipeline.addOperator(daviesBouldinIndex)
          pipeline.addOperator(dunnIndex)
          pipeline.addOperator(silhouetteIndex)
          pipeline.fit(clusterDataReassign)
          pipeline.transform()

          // final result
          val bw = new BufferedWriter(
            new FileWriter(
              new File("./result/log/streaming/result_" + file + ".txt"),
              true))
          bw.write(
            "\n########################################## RESULT FINISHED ##########################################\n")
          bw.write(s"Epsilon: ${datafile(i).epsilon}\n")
          //        bw.write(s"Percentage: ${percentage}\n")
          bw.write(s"Number of cluster: ${StreamingModel.getCluster()}\n")
          bw.write(s"Dataset name: ${file}\n")
          bw.write(s"Coreset: ${clusterData.length()}\n")
          bw.write(s"Origin: ${datasetSize}\n")
          bw.write(s"DaviesBouldinIndex : ${daviesBouldinIndex.getValue()}\n")
          bw.write(s"DunnIndex : ${dunnIndex.getValue()}\n")
          bw.write(s"SilhouetteIndex : ${silhouetteIndex.getValue()}\n")
          val mb = 1024 * 1024
          val runtime = Runtime.getRuntime
          bw.write(
            "** Used Memory:  " + (runtime.totalMemory - runtime.freeMemory) / mb + " MB\n")
          bw.write("** Free Memory:  " + runtime.freeMemory / mb + " MB\n")
          bw.write("** Total Memory: " + runtime.totalMemory / mb + " MB\n")
          bw.write("** Max Memory:   " + runtime.maxMemory / mb + " MB\n")
          bw.write(s"Duration: ${duration}s\n")
          bw.write(
            "#####################################################################################################\n")
          bw.close()
        }
      }
    } else {
      println("Warning: No file cores specified")
    }
    sc.stop()
  }

  def furtherETL(data: DataFrame): Dataset = {
    import spark.implicits._
    val dataFinale: DataFrame = data.drop("rank")
    // val dataList: Array[Array[Double]] = collectionAsScalaIterable(
    //   dataFinale.as[Array[Double]].collectAsList()).toArray
    val tmp = dataFinale.collect.map(_.toSeq)
    val dataList: Array[Array[Double]] = tmp
      .map(
        _.toString
          .replaceAll("WrappedArray\\(", "")
          .replaceAll("\\)", "")
          .replaceAll(",+", ",")
          .split(",")
          .map(_.toDouble)
          .toArray)
      .toArray

    var dataPointList: Array[DataPoint] = Array[DataPoint]()
    for (x <- dataList.indices) {
      dataPointList :+= new DataPoint(x, coord = dataList(x))
    }

    val prepareDataset: Dataset = new Dataset()
    prepareDataset.setDataset(dataPointList)

    return prepareDataset
  }
}
