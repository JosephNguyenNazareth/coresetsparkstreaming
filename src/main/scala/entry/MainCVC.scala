package entry

import java.io.{BufferedWriter, File, FileWriter}

import utils.{Dataset, Pipeline, SystemUtils, minMaxNormalise}
import clustering.{CVCModel, DBSCANModel}
import metrics.{CDbwIndex, DaviesBouldinIndex, DunnIndex, SilhouetteIndex}

import scala.math.pow

/*
These are the following arguments that need to be passed:
    --filecore string (name of file)

    --prefix_dir string (prefix of directory)

    --epsilon float (0 to 1.0)

    --percentay[Int] = Array[Int](initialMaxCouple._1)
//    var JList: Array[Int] = new Array[Int](dataset.length) //0 unmoved, 1 moved
//    var JMinDist: Array[Double] = new Array[Double](dataset.length)
//    var JMinIndex: Array[Int] = new Array[Int](dataset.length)
//
//    println(JList.length)
//
//    //initialize first
//    for (t <- JList.indices) {
//      if (t == initialMaxCouple._1) {
//        JList(t) = 1
//        JMinDist(t) = 0.0
//        JMinIndex(t) = -1
//      } else {
//        JList(t) = 0
//        JMinDist(t) = Double.MaxValue
//        JMinIndex(t) = initialMaxCouple._1
//      }
//    }
//
//    var previousIndex = initialMaxCouple._1
//    while (PList.length < dataset.length) {
//      var minCouple: (Int, Int) = (-1, -1)
//      var minDistance: Double = Double.MaxValue
//      for (t <- JList.indices) {
//        if (JList(t) == 0) { // unmoved
//          //test for update
//          var newDistance = distanceMap(t)(previousIndex)
//          if (newDistance < JMinDist(t)) {
//            JMinDist(t) = newDistance
//            JMinIndex(t) = previousIndex
//          }
//          //check for smallest
//          if (minDistance > JMinDist(t)) {
//            minDistance = JMinDist(t)
//            minCouple = (JMinIndex(t), t)
//          }
//        }
//      }
//      //update j
//      JList(minCouple._2) = 1
//      JMinDist(minCouple._2) = 0.0
//      JMinIndex(minCouple._2) = -1
//      //update p
//      PList :+= minCouple._2
//      previousIndex = minCouple._2
//    }ge float (0 to 0.1)

    --ratio string (0 to 0.1)

    --threshold_level int (1 and above)

    --population int (1 and above)

    --num_gen int (1 and above)

    --avatmode string ("avatlite", "avat", "avatlite_idsa")

    --mode string ("cluster", "estimate")

    --clean_cache boolean (true, false)

    --enable_plot (true,false)

    --enable_log (true,false)
 */

object MainCVC extends App {
  /*
    1. Parsing arguments
   */
  //Default arguments
  var fileCore = Array[String]("spambase.data")
  var prefixDir = "./datasets/streamkm/"
  var epsilon = 0.001
  var percentage = 0.85
  var ratio = 0.00004
  var thresholdLevel: Int = 1
  var population: Int = 50

  var numGen: Int = 150
  var avatMode = "avatlite"
  var mode = "cluster"
  var cleanCache = false
  var enableLog = false
  var enablePlot = false

  args.sliding(2, 2).toList.collect {
    case Array("--filecore", x: String)        => fileCore :+= x
    case Array("--prefix_dir", x: String)      => prefixDir = x
    case Array("--epsilon", x: String)         => epsilon = x.toDouble
    case Array("--percentage", x: String)      => percentage = x.toDouble
    case Array("--ratio", x: String)           => ratio = x.toDouble
    case Array("--threshold_level", x: String) => thresholdLevel = x.toInt
    case Array("--population", x: String)      => population = x.toInt
    case Array("--num_gen", x: String)         => numGen = x.toInt
    case Array("--avatmode", x: String)        => avatMode = x
    case Array("--mode", x: String)            => mode = x
    case Array("--clean_cache", x: String)     => cleanCache = x.toBoolean
    case Array("--enable_plot", x: String)     => enablePlot = x.toBoolean
    case Array("--enable_log", x: String)      => enableLog = x.toBoolean
  }

  /*
    2. Check clean cache
   */
  if (cleanCache) {
    SystemUtils.cleanDirectory()
  }

  if (fileCore.length != 0) {
    //declare pipeline
    var pipeline: Pipeline = new Pipeline()

    //normalization for dataset
    var normaliser: minMaxNormalise = new minMaxNormalise()
    pipeline.addOperator(normaliser)

    //initialize cluster algorithms for dataset
    var clusterAlgo = new CVCModel()
      .setEpsilon(epsilon)
      .setPercentage(percentage)
      .setAVATMode(avatMode)
      .setRatio(ratio)
      .setThresholdValueForIDSA(thresholdLevel)
      .setPopulationForIDSA(population)
      .setNumGenerationForIDSA(numGen)
      .setMode(mode)
      .setReturnMode("origin")
      .setCalMode("memory-based")
      .enableLog(enableLog)
      .enablePlot(enablePlot)
      .setSamplingMode("improved")

    pipeline.addOperator(clusterAlgo)

    //declaring metrics for evaluation
    var daviesBouldinIndex = new DaviesBouldinIndex()
    var dunnIndex = new DunnIndex()
    var cdbwIndex = new CDbwIndex()
    var silhouetteIndex = new SilhouetteIndex()
    if (avatMode != "avatlite_idsa") {
      pipeline.addOperator(daviesBouldinIndex)
      pipeline.addOperator(dunnIndex)
      pipeline.addOperator(cdbwIndex)
      pipeline.addOperator(silhouetteIndex)
    }

    for (file <- fileCore) {
      //Initialize dataset
      var dataset: Dataset = new Dataset()
        .setPrefix(prefixDir)
        .setTargetFile(file)
        .readFile(",")

      //fit dataset into pipeline
      pipeline.fit(dataset)

      //transform
      pipeline.transform()

      val bw = new BufferedWriter(
        new FileWriter(new File("./result/log/cvc/result_" + file + ".txt"),
                       true))
      bw.write(
        "\n########################################## RESULT FINISHED ##########################################\n")
      bw.write(s"Epsilon: ${epsilon}\n")
      bw.write(s"Percentage: ${percentage}\n")
      bw.write(s"Number of cluster: ${clusterAlgo.getEstimated().deep}\n")
      bw.write(s"Dataset name: ${dataset.getFileCore()}\n")
      bw.write(s"Origin: ${dataset.length}\n")
      if (avatMode != "avatlite_idsa") {
        bw.write(s"DaviesBouldinIndex : ${daviesBouldinIndex.getValue()}\n")
        bw.write(s"DunnIndex : ${dunnIndex.getValue()}\n")
        bw.write(s"CDbwIndex : ${cdbwIndex.getValue()}\n")
        bw.write(s"SilhouetteIndex : ${silhouetteIndex.getValue()}\n")
      }
      val mb = 1024 * 1024
      val runtime = Runtime.getRuntime
      bw.write(
        "** Used Memory:  " + (runtime.totalMemory - runtime.freeMemory) / mb + " MB\n")
      bw.write("** Free Memory:  " + runtime.freeMemory / mb + " MB\n")
      bw.write("** Total Memory: " + runtime.totalMemory / mb + " MB\n")
      bw.write("** Max Memory:   " + runtime.maxMemory / mb + " MB\n")
      bw.write(
        s"Time elspased (without file logging) in seconds: ${pipeline.timeElapsed().toDouble / pow(10, 9)} seconds\n")
      bw.write(
        "#####################################################################################################\n")
      bw.close()
    }

    clusterAlgo.closePythonConnection()
  } else {
    println("Warning: No file cores specified")
  }
}
