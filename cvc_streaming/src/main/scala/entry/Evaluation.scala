package entry

import utils.DataPoint

import metrics.{CDbwIndex, DaviesBouldinIndex, DunnIndex, SilhouetteIndex}
import utils.{DataPoint, Dataset, Pipeline, SystemUtils}

import scala.io.Source
import scala.collection.mutable.ArrayBuffer
import java.io.{BufferedWriter, File, FileWriter, FileReader}

object Evaluation {
  private var fileCore: String = ""
  private var clusterMap: Map[Int, Int] = Map()

  def setFileCore(newFileCore: String): Unit = {
    fileCore = newFileCore
  }

  private def readFileStreamKM(delimiter: String = " "): Dataset = {
    var fileDataset: Array[DataPoint] = Array[DataPoint]()
    val prefix: String = "./result/process/streamkmpp/"
    val fileName: String = prefix + fileCore + "_streamkmpp.txt"
    // csv
    val lines: Iterator[String] = Source.fromFile(fileName).getLines()
    var row: Int = 0

    for (line <- lines) {
      val params: Array[String] =
        line.replace(", ", ",").trim.split("\t").toArray
      val coordX = params(0)
        .replaceAll("\\[", "")
        .replaceAll("\\]", "")
        .replaceAll("\n", "")
        .split(",")
        .map(_.toDouble)
        .toArray
      val cluster = params(1).toInt
      if (!clusterMap.contains(cluster)) {
        clusterMap += (cluster -> 1)
      } else {
        clusterMap += (cluster -> (clusterMap(cluster) + 1))
      }

      val disToCentroid = params(2).toDouble

      fileDataset :+= new DataPoint(index = row,
                                    coord = coordX,
                                    disToRep = disToCentroid,
                                    cluster = cluster)
      row = row + 1
    }
    return new Dataset(fileDataset)
  }

  // In order to evaluate the result between things (anything you can imagine)
  def runEvaluationIndex(): Unit = {
    val data: Dataset = readFileStreamKM()

    println("hello")
    val pipeline: Pipeline = new Pipeline()
    val daviesBouldinIndex = new DaviesBouldinIndex()
    val dunnIndex = new DunnIndex()
    val silhouetteIndex = new SilhouetteIndex()

    pipeline.addOperator(daviesBouldinIndex)
//    pipeline.addOperator(dunnIndex)
    pipeline.addOperator(silhouetteIndex)
    pipeline.fit(data)
    pipeline.transform()

    // final result
    val bw = new BufferedWriter(
      new FileWriter(
        new File("./result/log/streamkmpp/result_" + fileCore + ".txt"),
        true))
    bw.write(
      "\n########################################## RESULT FINISHED ##########################################\n")
    bw.write(s"Number of cluster: ${clusterMap.size}\n")
    bw.write(s"Dataset name: ${fileCore}\n")
    bw.write(s"DaviesBouldinIndex : ${daviesBouldinIndex.getValue()}\n")
    bw.write(s"DunnIndex : ${dunnIndex.getValue()}\n")
    bw.write(s"SilhouetteIndex : ${silhouetteIndex.getValue()}\n")
    val mb = 1024 * 1024
    val runtime = Runtime.getRuntime
    bw.write(
      "** Used Memory:  " + (runtime.totalMemory - runtime.freeMemory) / mb + " MB\n")
    bw.write("** Free Memory:  " + runtime.freeMemory / mb + " MB\n")
    bw.write("** Total Memory: " + runtime.totalMemory / mb + " MB\n")
    bw.write("** Max Memory:   " + runtime.maxMemory / mb + " MB\n")
    bw.write(
      "#####################################################################################################\n")
    bw.close()
  }

  def main(args: Array[String]): Unit = {
    Evaluation.setFileCore("covtype_data")
    Evaluation.runEvaluationIndex()
  }
}
