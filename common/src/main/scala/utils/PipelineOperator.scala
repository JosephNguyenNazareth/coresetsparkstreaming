package utils

import scala.math.{pow, sqrt}
import math._
import java.lang.AutoCloseable
import java.io.IOException

import com.google.protobuf.Internal.DoubleList

abstract class PipelineOperator {
  protected var duration: Long = 0 //in nanoseconds

  def timeElapsed(): Long = {
    return duration
  }
  //process function for pipeline
  //Arg:
  //Input : Dataset
  //Output : Dataset
  def process(input: Dataset): Dataset
}

//pipeline operator used in normalizing dataset
class minMaxNormalise extends PipelineOperator {
  override def process(input: Dataset): Dataset = {
    println("Start normalize...")
    var minValue = Double.MaxValue
    var maxValue = Double.MinValue
    val data = input.getDataset()

    for (point <- data) {
      minValue = min(minValue, point.coord.min)
      maxValue = max(minValue, point.coord.max)
    }
    println(minValue, maxValue)
    val d: Int = input(0).coord.length
    val n: Int = input.length()

    for (i <- 0 until n) {
      for (x <- 0 until d) {
        input(i).coord =
          Normalise.minMaxNormalise(input(i).coord, x, minValue, maxValue)
      }
    }
    return input
  }
}

//pipeline operator used in normalizing dataset
class zScoreNormalise extends PipelineOperator {
  override def process(input: Dataset): Dataset = {
    //finding mean
    var num = input(0).coord.length * input.length
    var mean = input.getDataset().map(x => x.coord.sum).sum.toDouble / num
    var sigmaPow = input
      .getDataset()
      .map(x => x.coord.map(t => pow(mean - t, 2)).sum)
      .sum
      .toDouble / num
    var sigma = sqrt(sigmaPow)

    for (i <- 0 until input.length) {
      input(i).coord = Normalise.zScoreNormalise(input(i).coord, mean, sigma)
    }
    return input
  }
}
