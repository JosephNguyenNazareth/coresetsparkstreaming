import numpy as np

import matplotlib.pyplot as plt

from skimage import io, color

from scipy import ndimage
from scipy.ndimage.interpolation import zoom

from skimage.filters import gaussian, threshold_otsu
from skimage.morphology import watershed
from skimage.feature import peak_local_max
from skimage.measure import regionprops, label
from skimage import feature

import sys
import math
import os
import statistics
import math

from PIL import Image

import subprocess

def execute(command):
    popen = subprocess.Popen(command,stdout=subprocess.PIPE,stderr=subprocess.PIPE,bufsize=1)
    lines_iterator = iter(popen.stdout.readline,b"")
    while popen.poll() is None:
        for line in lines_iterator:
            nline = line.rstrip()
            print(nline.decode("latin"), end = "\r\n",flush =True) # yield line
    popen.wait()



def aVATDetect(imagefile, object_order_file):
    object_order_list = []
    with open(object_order_file, "r") as f:
        object_order_list = f.read().split('\n')
    # 0. Read files
    normal_img = io.imread(imagefile)
    gray_img = color.rgb2gray(normal_img)

    # 1. transform image using monotonic function
    max_val = np.max(gray_img)
    min_val = np.min(gray_img)
    gray_img = (gray_img - min_val) / (max_val - min_val)
    h = gray_img.shape[0]
    w = gray_img.shape[1]
    mean_val = 0.0
    # pass 1: cal mean value
    for y in range(0, h):
        for x in range(0, w):
            mean_val = mean_val + gray_img[y, x]
    mean_val = mean_val / (h * w)

    # pass 2: transformation
    for y in range(0, h):
        for x in range(0, w):
            t = gray_img[y, x]
            gray_img[y, x] = 1 - math.exp(-t * t / (mean_val * mean_val))
    # normalize
    max_val = np.max(gray_img)
    min_val = np.min(gray_img)
    gray_img = ((gray_img - min_val) / (max_val - min_val)) * 255
    gray_img = gray_img.astype("uint8")

    #avat preprocess save to image
    if not os.path.exists("./result/process/ivat_preprocessed/"):
        os.mkdir("./result/process/ivat_preprocessed/")
    io.imsave("./result/process/ivat_preprocessed/" + sys.argv[1] + ".png",gray_img) 

    # 2. otsu threshold
    binary_img = gray_img < threshold_otsu(gray_img)
    binary_img_for_save = binary_img.astype('uint8') * 255

    #pad image
    binary_img_for_save = np.pad(binary_img_for_save, pad_width=2, mode='constant', constant_values=0)
    #Reduce size to 100x100
    ratio = 200 / float(h + w)
    h = 100
    w = 100
    binary_img_for_save = zoom(binary_img_for_save, ratio)
    if not os.path.exists("./result/process/ivat_binarized/"):
        os.mkdir("./result/process/ivat_binarized/")
    io.imsave("./result/process/ivat_binarized/" + sys.argv[1] + "_avat.png",binary_img_for_save) 

    # 3. Create white image
    white_img = np.full((h + 4,w + 4),255,dtype="uint8") #due to padding
    white_img[0][:] = 0
    white_img[-1][:] = 0
    for x in range(h + 4): #due to padding
        white_img[x][0] = 0
        white_img[x][-1] = 0 
    canny_img = feature.canny(white_img)
    canny_pil = Image.fromarray(canny_img)
    canny_pil.save("./.log/template.pgm")

    #clean avat_temp.txt
    if os.path.isfile("./src/main/cpp/clustering/config/avat_temp.txt"):
        os.remove("./src/main/cpp/clustering/config/avat_temp.txt")

    #4. create fitline text file

    execute([
        './src/main/cpp/clustering/bin/fitline_work',
        "./.log/template.pgm"
    ])

    #5. perform chamfer matching
    execute([
        './src/main/cpp/clustering/bin/fdcm_work',
        "./result/process/ivat_binarized/{}_avat.png".format(sys.argv[1]),
        "./result/process/avat_segment/{}.png".format(sys.argv[1]),
        "./.log/{}_segment_info.txt".format(sys.argv[1])
    ])

    #7. read segments and return list of clusters
    k = 0
    cluster_list = []
    
    with open("./.log/{}_segment_info.txt".format(sys.argv[1])) as info_segment:
        lines = info_segment.readlines()
        if len(lines) == 0: #0 number of clusters
            k = 1
            c_list = []
            for i in range(0, len(object_order_list)):
                c_list.append(object_order_list[i])
            cluster_list.append(c_list)
            
            highlighter = np.full(gray_img.shape,255,'uint8')
        else:
            cluster_list = []
            block_range = []
            for line in lines[1:]:
                x,y,s_width,s_height = [int(x.strip("\n")) for x in line.split()]
                minr = y
                minc = x
                maxr = minr + s_height
                maxc = minc + s_width
                left_range = int(math.ceil(float(minr + minc) / 2))
                right_range = int(math.floor(float(maxr + maxc) / 2))
                group_list = []
                block_range.append((left_range,right_range))
                for i in range(left_range, right_range):
                    group_list.append(object_order_list[i])
                cluster_list.append(group_list)
            k = len(cluster_list)

            highlighter = np.full((gray_img.shape[0],gray_img.shape[1],3),(107,85,51),'uint8')
            color_holder = []
            for c in block_range:
                co = tuple(np.random.choice(range(256), size=3))
                while co in color_holder or co == (207,185,151):
                    co = tuple(np.random.choice(range(256), size=3))
                color_holder.append(co) #new color
                highlighter[c[0]:c[1],c[0]:c[1],:] = co

        io.imsave("./result/process/ivat_highlight/" + sys.argv[1] + "_avat.png",highlighter) 
    return k,cluster_list


def main():
    imagefile_core = sys.argv[1]

    imagefile = "./result/process/ivat/{}.png".format(imagefile_core)
    objectOrderFile = "./.log/{}_ivat_order.txt".format(imagefile_core)

    k_initial, cluster_list = aVATDetect(imagefile, objectOrderFile)

    # write down number cluster list
    with open("./.log/{}_avat_initial_cluster.txt".format(imagefile_core), "w") as kfile:
        for c in range(1, k_initial + 1):
            for mem in cluster_list[c - 1]:
                kfile.write(str(mem))
                if(mem != cluster_list[-1]):
                    kfile.write(" ")
            if c != k_initial:
                kfile.write("\n")


if __name__ == "__main__":
    main()
