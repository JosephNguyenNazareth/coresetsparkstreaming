package streaming

import java.util
import twitter4j.auth.OAuthAuthorization
import twitter4j.Status
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.sql.{Column, DataFrame, Row, SQLContext}
import configuration.BaseContext
import org.apache.spark.sql.functions._
import utils.DateUtils
import configuration.YAMLConfig
import twitter4j.conf._
import com.google.gson.Gson
import org.apache.spark.sql.types.LongType
import org.apache.spark.ml.feature.StopWordsRemover
import org.apache.spark.sql.UserDefinedFunction

//import moa.clusterers.streamkm.StreamKM

import scala.collection.JavaConversions._
import utils._
import java.util.Properties

class Producer extends BaseContext with Serializable {
  // configuration for starting spark
  val conf = buildSparkConf("Tweet Streaming Process")
  conf.set("spark.ui.port", "9050")
  conf.set("spark.executor.memory", "2g")
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")
  val sqlContext = new SQLContext(sc)
  val ssc = new StreamingContext(sc, Seconds(2))

//  val spark = sqlContext.sparkSession
  var gson: Gson = new Gson()

  // configuration for storing data
  val mongo: util.LinkedHashMap[String, Any] =
    YAMLConfig.get("mongodb").asInstanceOf[java.util.LinkedHashMap[String, Any]]
  val host: String = mongo.get("host").asInstanceOf[String]
  val database: String = mongo.get("database").asInstanceOf[String]
  val collectionStreaming: String =
    mongo.get("collection_streaming").asInstanceOf[String]
  val collectionEtl: String = mongo.get("collection_etl").asInstanceOf[String]

}

//Twitter API
//250 requests/month
object TweetKafkaProducer extends Producer {
  val fileCore: String = "twitter_streaming"

  protected def preProcess(rdd: DataFrame): Dataset = {
    import sqlContext.implicits._
    if (!rdd.head(1).isEmpty) {
      val inputTimeFormat: String = "MMM dd, yyyy h:mm:ss a"
      val outputTimeFormat: String = "yyyy-MM-dd HH:mm:ss"
      val currentTime = DateUtils.getCurrentDateTime(inputTimeFormat)
      val dataRaw: DataFrame = rdd
        .withColumn("createdAt",
                    when(col("createdAt").isNotNull, col("createdAt"))
                      .otherwise(lit(currentTime)))
        .withColumn("tweetTime",
                    normaliseDateTime(col("createdAt"),
                                      lit(inputTimeFormat),
                                      lit(outputTimeFormat)))
        .withColumn("ymd", substring(col("tweetTime"), 0, 10))
        .withColumn("tweetID", col("id").cast(LongType))
        .drop("id")
        .drop("tweetTime")
      MongoAction.saveData(dataRaw, host, database, collectionStreaming)

      val prepareDataset: Dataset = furtherETL(dataRaw, "TweetPoint")

      return prepareDataset
    }
    return new Dataset()
  }

  val textProcessing: UserDefinedFunction = udf((text: String) => {
    // 0. lowercase
    var preprocessText = text.toLowerCase()

    //1. remove emails
    val simpleEmailRegex =
      "([^.@\\s]+)(\\.[^.@\\s]+)*@([^.@\\s]+\\.)+([^.@\\s]+)".r
    preprocessText = simpleEmailRegex.replaceAllIn(preprocessText, " ")

    //2. urls
    val complexURLRegex =
      """(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?""".r
    preprocessText = complexURLRegex.replaceAllIn(preprocessText, " ")

    //3. mentions and hashtag
    val simpleMentionAndHashtagRegex = """\s([@#][\w_-]+)""".r
    preprocessText =
      simpleMentionAndHashtagRegex.replaceAllIn(preprocessText, " ")

    //4. Removing non-alphabet characters (which are special character, emojis, punctuations, ...)
    val nonAlphabetRegex = "[^A-Za-z0-9 ]".r
    preprocessText = nonAlphabetRegex.replaceAllIn(preprocessText, "")
    preprocessText = preprocessText.replaceAll(" +", " ").trim

    //5. remove stop words and stemming
    // these will be process in spark due to available libraries
    preprocessText
  })

  val concatenateBagOfWord: UserDefinedFunction = udf(
    (bagWord: Seq[String]) => {
      var concatString: String = ""
      for (x <- bagWord.indices) {
        concatString += bagWord(x)
        if (x != bagWord.length - 1)
          concatString += " "
      }
      concatString
    })

  val normaliseDateTime: UserDefinedFunction = udf(
    (createdAt: String, fromFormat: String, toFormat: String) => {
      DateUtils.convertStrDateToFormat(createdAt, fromFormat, toFormat)
    })

  def furtherETL(data: DataFrame,
                 dataType: String,
                 mode: String = "test"): Dataset = {
    import sqlContext.implicits._
    val considerAttribute: List[Column] = List("tweetID",
                                               "text",
                                               "createdAt",
                                               "user.id",
                                               "user.name",
                                               "user.friendsCount",
                                               "user.followersCount",
                                               "user.favouritesCount")
      .map(colName => col(colName))
    val finalConsiderAttribute: List[Column] = List("tweetID",
                                                    "content",
                                                    "tweetTime",
                                                    "timeStamp",
                                                    "userID",
                                                    "userName",
                                                    "friends",
                                                    "followers",
                                                    "favourites",
                                                    "ymd")
      .map(colName => col(colName))

    val inputTimeFormat: String = "MMM dd, yyyy h:mm:ss a"
    val outputTimeFormat: String = "yyyy-MM-dd HH:mm:ss"

    var storeData: DataFrame = data
      .select(considerAttribute: _*)
      .distinct()
      .withColumn("preContent", textProcessing(col("text")))
      .withColumn("contentWords", split(col("preContent"), " "))

    // remove stop word
    val stopWord = new StopWordsRemover()
      .setInputCol("contentWords")
      .setOutputCol("bagOfWord")
    storeData = stopWord
      .transform(storeData)
      .withColumn("content", concatenateBagOfWord(col("bagOfWord")))

    // stemming preprocessing
    //    val stemming = new Stemmer().setInputCol("contentWordRemoved").setOutputCol("bagOfWord")
    //      .setLanguage("English")
    //    storeData = stemming.transform(storeData)

    storeData = storeData
      .withColumn("tweetTime",
                  normaliseDateTime(col("createdAt"),
                                    lit(inputTimeFormat),
                                    lit(outputTimeFormat)))
      .withColumn("timeStamp",
                  unix_timestamp(col("tweetTime"), outputTimeFormat))
      .withColumn("userID", col("id").cast(LongType))
      .withColumn("userName", col("name"))

    if (storeData.columns.contains("friendsCount"))
      storeData =
        storeData.withColumn("friends", col("friendsCount").cast(LongType))
    else
      storeData = storeData.withColumn("friendsCount", lit(""))
    if (storeData.columns.contains("followersCount"))
      storeData =
        storeData.withColumn("followers", col("followersCount").cast(LongType))
    else
      storeData = storeData.withColumn("followersCount", lit(""))
    if (storeData.columns.contains("favouritesCount"))
      storeData = storeData.withColumn("favourites",
                                       col("favouritesCount").cast(LongType))
    else
      storeData = storeData.withColumn("favouritesCount", lit(""))

    storeData = storeData
      .withColumn("ymd", substring(col("tweetTime"), 0, 10))
      .select(finalConsiderAttribute: _*)

    val newOrderColumn: List[Column] =
      storeData.columns.sorted.toList.map(colName => col(colName))
    val finalData: DataFrame = storeData.select(newOrderColumn: _*).cache()
    finalData.show(2, truncate = false)

    if (mode.equals("real")) {
//      MongoAction.deleteOldData(mongo, finalData, collectionStreaming)
      MongoAction.saveData(finalData, host, database, collectionEtl)
    }

    // ce temps la, transferer les donnees Spark a la forme de classe TwitterData
    // puis, on les utilise dans la processus d'echantillonnage Coreset
    val dataList: List[TwitterData] = collectionAsScalaIterable(
      finalData.as[TwitterData].collectAsList()).toList
    var dataPointList: Array[DataPoint] = Array[DataPoint]()
    for (x <- dataList.indices) {
      val newDataPoint: DataPoint = new DataPoint()
      newDataPoint.index = x
      newDataPoint.tweet = new Twitter(dataList(x))
      dataPointList :+= newDataPoint
    }

    val prepareDataset: Dataset = new Dataset()
    prepareDataset.setDataset(dataPointList)

    return prepareDataset
  }

  def main(args: Array[String]): Unit = {
    // import spark.implicits._
//    val ssc = new StreamingContext(sc, Seconds(2))

    //1. Configuring Twitter API
    val builder = new ConfigurationBuilder()
    builder.setJSONStoreEnabled(true) //For storing as json
    builder.setDebugEnabled(true)
    builder.setOAuthConsumerKey(TwitterKey.API_KEY)
    builder.setOAuthConsumerSecret(TwitterKey.API_KEY_SECRET)
    builder.setOAuthAccessToken(TwitterKey.ACCESS_TOKEN)
    builder.setOAuthAccessTokenSecret(TwitterKey.ACCESS_TOKEN_SECRET)

    val configuration = builder.build()
    val tweets = TwitterUtils.createStream(
      ssc,
      Some(new OAuthAuthorization(configuration))) //TwitterInputDStream
    val englishTweets = tweets.filter(_.getLang == "en") //we focus on english onl y //JavaDStream
    val aggStream = englishTweets.flatMap(x => x.getText.split(" "))

    // setting which algorithm to go
    val algorithm: String = args(1);
    // create properties for producer to send received tweets
    val sendProps = new Properties()
    sendProps.put("key.serializer",
                  "org.apache.kafka.common.serialization.StringSerializer")
    sendProps.put("value.serializer",
                  "org.apache.kafka.common.serialization.StringSerializer")
    sendProps.put("bootstrap.servers", "localhost:9009")

    if (algorithm == "cvc")
      StreamingModel.setBatchSize(100)

    englishTweets.foreachRDD { (rdd, _) =>
      val currentTime = DateUtils.getCurrentDateTime("yyyy-MM-dd HH:mm:ss")
      println("========== START GETTING DATA AT " + currentTime + " ==========")
      var sizeOfTweet: Int = 0

      if (!rdd.isEmpty()) {
        rdd.foreachPartition { partitionIter =>
          var tweetList = List.empty[String]
          partitionIter.foreach { element =>
            val newTweet = gson.toJson(element)
            tweetList :+= newTweet
          }
          sizeOfTweet += tweetList.length
          println("size", sizeOfTweet)

          val rddData = sc.parallelize(tweetList)
          val dataTwitter = sqlContext.read.json(rddData)

          if (algorithm == "cvc") {
            val dataPreProcessed: Dataset = preProcess(dataTwitter)
            dataPreProcessed.setTargetFile(fileCore)
            StreamingModel.parseDataStreaming(dataPreProcessed)
          } else if (algorithm == "streamkm++") {}
          Thread.sleep(10 * 1000)
        }
      }
    }
    ssc.start()
    ssc.awaitTermination()
  }
//  sqlContext.clearCache()
//  sc.stop()
}
