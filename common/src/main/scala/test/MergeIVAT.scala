package entry

import sys.process._
import scala.math.{pow, sqrt}

import utils.{SystemUtils, Dataset, Pipeline, minMaxNormalise}
import clustering.{DBSCANModel}

import sampling.{IProTraS}

import estimation.{VATClus}

object MergeIVAT extends App {
  /*
    1. Parsing arguments
   */
  //Default arguments
  var fileCore = Array[String]()
  var prefixDir = "./datasets/"
  var epsilon = 0.015
  var percentage = 0.6
  var ratio = 0.00004
  var thresholdLevel: Int = 1
  var population: Int = 50
  var numGen: Int = 150
  var avatMode = "avatlite"
  var cleanCache = true

  args.sliding(2, 2).toList.collect {
    case Array("--filecore", x: String)        => fileCore :+= x
    case Array("--prefix_dir", x: String)      => prefixDir = x
    case Array("--epsilon", x: String)         => epsilon = x.toDouble
    case Array("--percentage", x: String)      => percentage = x.toDouble
    case Array("--ratio", x: String)           => ratio = x.toDouble
    case Array("--threshold_level", x: String) => thresholdLevel = x.toInt
    case Array("--population", x: String)      => population = x.toInt
    case Array("--num_gen", x: String)         => numGen = x.toInt
    case Array("--avatmode", x: String)        => avatMode = x
    case Array("--clean_cache", x: String)     => cleanCache = x.toBoolean
  }

  /*
    2. Check clean cache
   */
  if (cleanCache) {
    SystemUtils.cleanDirectory()
  }

  if (fileCore.length != 0) {
    val iprotras: IProTraS = new IProTraS()
      .setPercentage(percentage)
      .enablePlot(false)
      .enableLog(false)
      .setEpsilon(epsilon)
      .setCalMode("memory-based")
      .asInstanceOf[IProTraS]

    val vatclus = new VATClus()
      .setVATMode("ivat") //avat requires that an ivat image be used
      .setRatio(ratio)
      .setAVATMode(avatMode)
      .setThresholdValueForIDSA(thresholdLevel)
      .setPopulationForIDSA(population)
      .setNumGenerationForIDSA(numGen)

    val pipeline: Pipeline = new Pipeline(Array(iprotras, vatclus))

    for (file <- fileCore) {
      //Initialize dataset
      var dataset: Dataset = new Dataset()
        .setPrefix(prefixDir)
        .setTargetFile(file)
        .readFile()

      pipeline.fit(dataset)
      pipeline.transform()

      println(vatclus.getEstimated().deep)
    }
    vatclus.closePythonConnection()
  } else {
    println("Warning: No file cores specified")
  }
}
