import redis
import argparse
import ast
import plotly.graph_objects as go
from sklearn.decomposition import PCA
import numpy as np
import collections


def main():
    rc = redis.Redis(host='localhost', port=6379, db=2)
    while True:
        while True:
            request = rc.get("cluster_scatterplot_3d_request")
            if request is not None:
                rc.delete("cluster_scatterplot_3d_request")
                break
        # decode to string
        request = request.decode("utf-8")
        # convert to dictionary
        query = ast.literal_eval(request)
        num_dim = query['num_dim']
        if num_dim == 3:
            print(query)
            # get cluster
            num_cluster = query['cluster']
            cluster_list = []
            for num in range(num_cluster):
                cluster_list.append(go.Scatter3d(x=query[f'x_{num}'], y=query[f'y_{num}'], z=query[f'z_{num}'],
                                                 mode='markers'))
        else:
            # first column as index
            index_column = []
            data_column = []
            cluster_column = []

            for point in query['data']:
                index_column.append(point[0])
                data_column.append(point[1])
                cluster_column.append(point[2])

            data_column = np.asarray(data_column)
            # PCA
            pca = PCA(n_components=3)
            pca.fit_transform(data_column)  # shape(so diem,3)
            # gop lai thanh tung cluster
            data_dict = collections.defaultdict(dict)
            # Gom cluster
            for point, cluster_num in zip(data_column, cluster_column):
                if 'x' not in data_dict[cluster_num]:
                    data_dict[cluster_num]['x'] = []
                if 'y' not in data_dict[cluster_num]:
                    data_dict[cluster_num]['y'] = []
                if 'z' not in data_dict[cluster_num]:
                    data_dict[cluster_num]['z'] = []

                data_dict[cluster_num]['x'].append(point[0])
                data_dict[cluster_num]['y'].append(point[1])
                data_dict[cluster_num]['z'].append(point[2])
            # Chay plotly
            cluster_list = []
            for k in data_dict:
                cluster_list.append(go.Scatter3d(x=data_dict[k]['x'], y=data_dict[k]['y'], z=data_dict[k]['z'],
                                                 mode='markers'))
        # begin plotting
        fig = go.Figure(data=cluster_list)
        fig.update_layout({"title": query['message']})
        fig.show()


if __name__ == "__main__":
    main()
