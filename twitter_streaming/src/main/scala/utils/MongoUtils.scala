package utils

import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.WriteConfig
import org.apache.spark.sql.DataFrame
import org.bson.Document

object MongoAction {
  def saveData(storeData: DataFrame,
               host: String,
               database: String,
               collection: String): Unit = {
    println("===== Start saving to MongoDB =====")
    val urlDatabase = "mongodb://" + host + "/" + database + "." + collection
    println(urlDatabase)
    val writeConfig = WriteConfig(Map("uri" -> (urlDatabase)))
    MongoSpark.save(storeData.write.mode("append"), writeConfig)
    println("===== Save to " + urlDatabase + " successfully =====")
  }

  def deleteOldData(mongo: java.util.LinkedHashMap[String, Any],
                    oldData: DataFrame,
                    collection: String): Unit = {
    val oldTweet: List[Long] = oldData
      .select("tweetID")
      .rdd
      .map(r => r(0))
      .collect()
      .toList
      .map(x => x.toString.toLong)
    val deleteStatement = new Document()
    deleteStatement.append("tweetID", new Document("$in", oldTweet))

    MongoDBConnector.deleteDocument(deleteStatement, mongo, collection)
  }
}

class ConfigurationSearch {
  var condition = new Document()
  var project = new Document()

  def setCondition(newCondition: Document): Unit = {
    condition = newCondition
  }

  def setProject(newProjet: Document): Unit = {
    project = newProjet
  }

  def getCondition(): Document = {
    return condition
  }

  def getProject(): Document = {
    return project
  }
}
