package sampling

import scala.io.Source
import scala.reflect.io.File
import scala.collection.mutable.ArrayBuffer
import scala.math.Ordering

import Double._
import math._
import util.control.Breaks._
import sys.process._

import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartPanel
import org.jfree.chart.ChartUtils
import org.jfree.chart.JFreeChart
import org.jfree.chart.plot.XYPlot
import org.jfree.data.xy.XYDataset
import org.jfree.data.xy.XYSeries
import org.jfree.data.xy.XYSeriesCollection

import javax.swing.JFrame
import javax.swing.JPanel
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.lang.Thread
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import utils._

import utils.{Dataset, DataPoint}

class ProTraS extends SamplingAlgorithm[ProTraS] {

  def setEpsilon(newEp: Double): ProTraS = {
    if (newEp <= 0.0) {
      println(
        "Error setting epsilon for the ProTraS: Please choose epsilon that is larger than 0!")
    }
    epsilon = newEp
    return this
  }

  def fit(dataset: Dataset): Dataset = {
    val t0 = System.nanoTime()
    var coresetIndex = fitForProTraS(dataset)
    //Update dataset with its nearest representatives
    for (targetIdx <- 0 until dataset.length) {
      //For original dataset
      var minDist: Double = Double.MaxValue
      var repIdx: Int = -1
      //for each coresetIndex
      for (k <- coresetIndex) {
        var distance: Double = getDistance(dataset, k, targetIdx)
        if (distance < minDist) {
          minDist = distance
          repIdx = k
        }
      }
      //update coreset
      dataset(targetIdx).rep = repIdx
      dataset(targetIdx).disToRep = minDist
      /*********************************************************/
    }

    //create new sampling dataset
    var sampling: Dataset = new Dataset()
    for (i <- coresetIndex) {
      sampling :+= dataset(i)
    }
    val t1 = System.nanoTime()
    duration = t1 - t0
    sampling.setTargetFile(dataset.getFileCore())

    return sampling
  }

  protected def fitForProTraS(dataset: Dataset): Array[Int] = {
    if (dataset.length() > 1) {
      //receive a sampling subset of the original dataset
      //A map to rep to its nearest point (map using list of indices)
      //Find initial point
      var initialPointIndex: Int = getInitialPointIndex(dataset)
      //Assigning initial value
      dataset(initialPointIndex).rep = initialPointIndex //it is the representative of itself

      representative += (initialPointIndex -> ArrayBuffer[Int]())
      representative(initialPointIndex) += initialPointIndex

      var coresetIndex = Array[Int]()
      coresetIndex :+= initialPointIndex

      //initialize cost
      var cost: Double = 0.0
      //size
      var dataSize: Int = dataset.length
      var maxLength: Double = 0.0

      //find maximum distance
      for (targetIdx <- 0 until dataSize) {
        var distance = getDistance(dataset, initialPointIndex, targetIdx)
        if (distance > maxLength) {
          maxLength = distance
        }
      }

      //initial representative
      for (targetIdx <- 0 until dataset.length) {
        dataset(targetIdx).rep = initialPointIndex
        if (targetIdx != initialPointIndex) {
          representative(initialPointIndex) += targetIdx
          var distance = getDistance(dataset, initialPointIndex, targetIdx)
          dataset(targetIdx).disToRep = distance
        } else {
          dataset(targetIdx).disToRep = 0.0
        }
      }

      /** ************************************************************************* */
      breakable {
        do {
          //Step 1: For each representative, find cost and largest distance
          cost = 0.0
          var theLionKing: Int = -1
          var maxWD: Double = 0.0
          var maxDistance: Double = 0.0;
          var largestCostRepIdx: Int = 0
          var sth: Double = 0.0;
          //for each representative
          for ((k, v) <- representative) {
            //for each member of representative, find farthest in group and (version1Order)th farthest in group
            var farthestDataPoint: DataPoint = new DataPoint()
            //for each index of representative group
            var normMaxDist: Double = 0.0

            for (idx <- v) {
              val currentDistance: Double = dataset(idx).disToRep
              if (currentDistance > normMaxDist) {
                normMaxDist = currentDistance
                farthestDataPoint = dataset(idx)
              }
            }
            // println(normMaxDist, farthestDataPoint)

            val pk: Double = representative(k).length * normMaxDist
            if (pk >= maxWD) {
              maxDistance = normMaxDist
              maxWD = pk
              theLionKing = farthestDataPoint.index
              largestCostRepIdx = k
            }
            cost = cost + (pk / (dataSize * maxLength))
            sth = pk
          }
          if (theLionKing == -1) {
            println(sth, representative.size)
            break
          }
          ///////////////////////////////////////////////////////////////////////////////////////////////////////////
          //step 2: add new point to the group
          representative += (theLionKing -> ArrayBuffer[Int]())
          representative(theLionKing) += theLionKing
          dataset(theLionKing).rep = theLionKing //  what for
          dataset(theLionKing).disToRep = 0.0

          //step 3: clear group
          for ((k, v) <- representative) {
            v.clear()
            v += k
          }

          //Step 4: Find group for each representative
          //For each DataPoint
          for (targetIdx <- 0 until dataset.length) {
            if (!(dataset(targetIdx).rep == targetIdx)) {
              //find distance with new rep
              var distance = getDistance(dataset, theLionKing, targetIdx)
              if (distance < dataset(targetIdx).disToRep) {
                representative(theLionKing) += targetIdx
                dataset(targetIdx).rep = theLionKing
                dataset(targetIdx).disToRep = distance
              } else {
                representative(dataset(targetIdx).rep) += targetIdx
              }
            }
          }
          ///////////////////////////////////////////////////////////////////////////////////////////////////
          //Step 5: Add new representative - new member of coreset - THE LION KING - to variables

          coresetIndex :+= theLionKing
          //      println(coresetIndex.length)
        } while (cost > epsilon && coresetIndex.length < dataset.length)
      }
      return coresetIndex
    } else {
      return Array[Int](dataset(0).index)
    }
  }
}
