//package clustering
//
//import java.util.UUID.randomUUID
//import java.util.concurrent.TimeUnit
//
//import com.redis.RedisClient
//import configuration.YAMLConfig
//import org.influxdb.InfluxDBFactory
//import org.influxdb.dto.{Point, Query}
//import utils.{DataPoint, Dataset, Measure, PipelineOperator}
//import vat_family.{IVAT, VAT}
//
//import scala.collection.mutable.Map
//import scala.sys.process._
//import scala.util.control.Breaks.{break, breakable}
//
//class COREKM extends PipelineOperator {
//  /*
//    A clustering algorithm designed by KK team
//   */
//  private var vatMode: String = "ivat" //vat
//  private var avatMode
//    : String = "avatlite" //avat, avatlite, avatlite_idsa, avatlite_idsa_streaming
//  private var distanceFunction =
//    (firstPoint: DataPoint, secondPoint: DataPoint) => {
//      Measure.euclide(firstPoint, secondPoint)
//    }
//  private var numClus = Array[Int]()
//  private var ratio: Double = 0.00088
//  private var epsilon: Double = 0.5
//  private var percentage: Double = 0.8
//  private var thresholdLevel: Int = 2 //for idsa
//  private var numGen: Int = 200
//  private var population: Int = 50
//  private var avatLiteThread: Option[Thread] = None
//  private val redisConfig =
//    YAMLConfig.get("redis").asInstanceOf[java.util.LinkedHashMap[String, Any]]
//  private val redisAVATLite = redisConfig
//    .get("redis_avatlite")
//    .asInstanceOf[java.util.LinkedHashMap[String, Any]]
//  private var orderList = Array[Int]()
//  private var clusterList = Array[Array[Int]]()
//  private var rawClusterList = Array[String]()
//  private var parents = Array[Int]()
//  private var idsaClusterList = Array[Array[Array[Int]]]() //for unmerge idsa
//  private var cheatMode: String = "naive"
//
//  // declaration for influxdb connection
//  private val influxdb = InfluxDBFactory.connect("http://localhost:8086",
//                                                 "superadmin",
//                                                 "your_password_here")
//  private val measurementList: Array[String] =
//    Array[String]("community", "community_common", "word")
//
//  //special variable only for streaming on social network
//  private val redisVisual = redisConfig
//    .get("redis_visual")
//    .asInstanceOf[java.util.LinkedHashMap[String, Any]]
//  private var visualThread: Option[Thread] = None
//  private var streamingVATImage = Array[Array[Double]]()
//  private var streamingIVATImage = Array[Array[Double]]()
//
//  def setAVATLiteThread(t: Option[Thread]): COREKM = {
//    avatLiteThread = t
//    return this
//  }
//
//  def getEstimated(): Array[Int] = {
//    return numClus
//  }
//
//  def getClusterList(): Array[Array[Int]] = {
//    return clusterList
//  }
//
//  def getIDSAClusterList(): Array[Array[Array[Int]]] = {
//    return idsaClusterList
//  }
//
//  def setVATMode(v: String): COREKM = {
//    if (v != "ivat" && v != "vat") {
//      println("Unknown vat mode: Using default")
//      vatMode = "ivat"
//    } else {
//      vatMode = v
//    }
//    return this
//  }
//
//  def setRatio(m: Double): COREKM = {
//    ratio = m
//    return this
//  }
//
//  def setEpsilon(m: Double): COREKM = {
//    epsilon = m
//    return this
//  }
//
//  def setPercentage(m: Double): COREKM = {
//    percentage = m
//    return this
//  }
//
//  def setDistanceFunction(df: (DataPoint, DataPoint) => Double): COREKM = {
//    distanceFunction = df
//    return this
//  }
//
//  def setAVATMode(av: String): COREKM = {
//    if (av != "avat" && av != "avatlite" && av != "avatlite_idsa" && av != "avatlite_idsa_streaming") {
//      println("Unknown avat mode: Using default")
//      avatMode = "avatlite"
//    } else {
//      avatMode = av
//    }
//    return this
//  }
//
//  def setThresholdValueForIDSA(value: Int): COREKM = {
//    /*
//        Set threshold value only for IDSA
//     */
//    if (value <= 0) {
//      println("Invalid value for idsa")
//    } else {
//      thresholdLevel = value
//    }
//    return this
//  }
//
//  def setPopulationForIDSA(value: Int): COREKM = {
//    /*
//        Set number of population for IDSA
//     */
//    if (value <= 0) {
//      print("Invalid value for idsa")
//    } else {
//      population = value
//    }
//    return this
//  }
//
//  def setNumGenerationForIDSA(value: Int): COREKM = {
//    /*
//        Set number of generation for IDSA
//     */
//    if (value <= 0) {
//      print("Invalid value for idsa")
//    } else {
//      numGen = value
//    }
//    return this
//  }
//
//  def setCheatMode(newCheatMode: String): COREKM = {
//    cheatMode = newCheatMode
//    return this
//  }
//
//  def process(dataset: Dataset): Dataset = {
//    return fit(dataset)
//  }
//
//  def initInfluxDBConnection(databaseName: String = "cvc"): COREKM = {
//    influxdb.query(new Query("drop database " + databaseName, databaseName))
//    if (!influxdb.describeDatabases().contains(databaseName)) {
//      influxdb.query(new Query("create database " + databaseName, databaseName))
//      influxdb.query(new Query(
//        "CREATE RETENTION POLICY \"a_day\" ON \"" + databaseName + "\" DURATION 1d REPLICATION 1"))
//    }
//    influxdb.setDatabase(databaseName)
//
//    return this
//  }
//
//  private def initPythonClient(): Unit = {
//    if (avatMode == "avatlite" || avatMode == "avatlite_idsa" || avatMode == "avatlite_idsa_streaming") {
//      if (avatLiteThread.isEmpty) {
//        val avatLiteHost = redisAVATLite.get("host").asInstanceOf[String]
//        val avatLitePort = redisAVATLite.get("port").asInstanceOf[Int]
//        val database = redisAVATLite.get("database").asInstanceOf[Int]
//        val re = new RedisClient(avatLiteHost, avatLitePort, database)
//        avatLiteThread = Some(new Thread {
//          override def run() {
//            s"python3 -m common_python.estimation.aVATLite --host ${avatLiteHost} --port ${avatLitePort} --db ${database}" !
//          }
//        })
//        //start thread
//        avatLiteThread.get.start()
//        //waiting for python to finish processing
//        breakable {
//          //maximum: 10s = 10000 milliseconds
//          var start: Long = System.currentTimeMillis
//          while (true) {
//            val status = re.get("avatlite_status")
//            if (status.isDefined) {
//              if (status.get == "success") {
//                break
//              } else {
//                // incorrect signal
//                println(
//                  "Failed connecting to Python through Redis. Exiting ...")
//                avatLiteThread.get.stop()
//                System.exit(1)
//              }
//            }
//            if (System.currentTimeMillis - start > 10000) {
//              println("Failed connecting to Python through Redis. Exiting ...")
//              avatLiteThread.get.stop()
//              System.exit(1)
//            } else {
//              Thread.sleep(1000)
//              println("Connecting ..")
//            }
//          }
//        }
//      }
//    }
//  }
//
//  private def avatlite(dataset: Dataset) = {
//    val avatLiteHost = redisAVATLite.get("host").asInstanceOf[String]
//    val avatLitePort = redisAVATLite.get("port").asInstanceOf[Int]
//    val database = redisAVATLite.get("database").asInstanceOf[Int]
//    val re = new RedisClient(avatLiteHost, avatLitePort, database)
//    var uuid: String = randomUUID.toString
//    //send a request to python process
//    //process order list got from vat or ivat
//    //TODO: Using join
//    var joinedObjectOrder: String = orderList.mkString(" ")
//    var request = s"""{
//            "id" : "${uuid}",
//            "order" : "${joinedObjectOrder}",
//            "file_core" : "${dataset.getFileCore()}",
//            "mode" : "${avatMode}",
//            "epsilon" : ${epsilon.toString},
//            "percentage" : ${percentage.toString},
//            "ratio" : ${ratio.toString},
//            "num_gen" : ${numGen},
//            "population" : ${population},
//            "threshold_level" : ${thresholdLevel},
//            "status" : "wait",
//        }"""
//    //while python has not yet deleted this key
//    while (re.get("avatlite_request").isDefined) {
//      Thread.sleep(1000)
//    }
//
//    re.set("avatlite_request", request)
//
//    // waiting to get result
//    // getting result
//    var status = re.hget(uuid, "status")
//    while (status.isEmpty) {
//      Thread.sleep(1000)
//      status = re.hget(uuid, "status")
//    }
//    if (status.get == "fail") {
//      println("Failed with aVATLite")
//      System.exit(1)
//    }
//
//    if (avatMode == "avatlite_idsa") {
//      idsaClusterList = new Array[Array[Array[Int]]](thresholdLevel)
//      for (level <- 0 until thresholdLevel) {
//        idsaClusterList(level) = Array[Array[Int]]()
//        var k = re.hget(uuid, s"level_${level}")
//        for (i <- 0 until k.get.toInt) {
//          var cluster = re.hget(uuid, s"cluster_${level}_${i}")
//          val cList = cluster.get.trim
//            .replaceAll(" +", " ")
//            .split(" +")
//            .map(_.trim.toInt)
//          idsaClusterList(level) :+= cList
//        }
//      }
//    } else if (avatMode == "avatlite") {
//      var k = re.hget(uuid, "cluster")
//      for (i <- 0 until k.get.toInt) {
//        var cluster = re.hget(uuid, s"cluster_${i}")
//        val cList = cluster.get.trim
//          .replaceAll(" +", " ")
//          .split(" +")
//          .map(_.trim.toInt)
//          .toArray
//          .asInstanceOf[Array[Int]]
//        clusterList :+= cList
//      }
//    } else if (avatMode == "avatlite_idsa_streaming") {
//      clusterList = Array[Array[Int]]()
//      rawClusterList = Array[String]()
//      parents = Array[Int]()
//      var k = re.hget(uuid, "cluster")
//      for (i <- 0 until k.get.toInt) {
//        var cluster = re.hget(uuid, s"cluster_${i}")
//        var parent = re.hget(uuid, s"cluster_${i}_parent")
//        while (cluster.isEmpty) {
//          Thread.sleep(1000)
//          cluster = re.hget(uuid, s"cluster_${i}")
//        }
//        while (parent.isEmpty) {
//          Thread.sleep(1000)
//          parent = re.hget(uuid, s"cluster_${i}_parent")
//        }
//        val cList = cluster.get.trim
//          .replaceAll(" +", " ")
//          .split(" +")
//          .map(_.trim.toInt)
//          .toArray
//          .asInstanceOf[Array[Int]]
//        clusterList :+= cList
//        parents :+= parent.get.toInt
//      }
//    }
//    re.del(uuid)
//  }
//
//  def fit(dataset: Dataset): Dataset = {
//    //0. Open separate process for avatlite or avatlite_idsa
//    initPythonClient()
//    //run vat/ivat
//    if (vatMode == "vat") {
//      var vat = new VAT()
//        .setDistanceFunction(distanceFunction)
//      if (avatMode != "avat") {
//        vat.enableLogMode(false)
//      }
//      vat.fit(dataset)
//      //get object order
//      if (avatMode != "avat") {
//        orderList = vat.getVATOrder()
//      }
//      if (avatMode == "avatlite_idsa_streaming") {
//        streamingVATImage = vat.getVATImage()
//      }
//      duration = duration + vat.timeElapsed()
//    } else {
//      var ivat = new IVAT()
//        .setDistanceFunction(distanceFunction)
//        .setEpsilon(epsilon)
//        .setPercentage(percentage)
//      if (avatMode != "avat") {
//        ivat.enableLogMode(false)
//      }
//      ivat.fit(dataset)
//      //get objecct order
//      if (avatMode != "avat") {
//        orderList = ivat.getIVATOrder()
//      }
//      if (avatMode == "avatlite_idsa_streaming") {
//        streamingVATImage = ivat.getVATImage()
//        streamingIVATImage = ivat.getIVATImage()
//      }
//      duration = duration + ivat.timeElapsed()
//    }
//
//    //run avat, or avatlite
//    val t0 = System.nanoTime()
//    if (avatMode == "avat") {
//      s"python3 -m src.main.python.estimation.aVAT ${dataset.getFileCore()}" !
//    } else {
//      avatlite(dataset)
//    }
//
//    val t1 = System.nanoTime()
//
//    //readfile to get number of cluster
//    if (avatMode == "avatlite" || avatMode == "avat") {
//      numClus = Array[Int](clusterList.length)
//    } else if (avatMode == "avatlite_idsa") {
//      for (image <- idsaClusterList) {
//        numClus :+= image.length
//      }
//    } else if (avatMode == "avatlite_idsa_streaming") {
//      // temporary method for dashboard
//      for (measurement <- measurementList)
//        influxdb.query(new Query("drop measurement " + measurement))
//
//      var indexMapper = Map[Int, Int]()
//      for (i <- 0 until dataset.length) {
//        indexMapper += (dataset(i).index -> i)
//      }
//      numClus :+= clusterList.length
//      //add parents
//      //information that will be sent
//      //mean variance skewness median
//      //cluster information with size for sunburn
//      //mean friend favourite follower count
//
//      //for each cluster, calculate means of friend count, follower count
//      var clusterID = 0
//      var mostUsedWord = Map[String, Double]()
//      for (cluster <- clusterList) {
//        //calculate counts
//        var meanFollowersCount = cluster
//          .map(x => dataset(indexMapper(x)).tweet.followers)
//          .sum
//          .toDouble / cluster.length
//        var meanFriendsCount = cluster
//          .map(x => dataset(indexMapper(x)).tweet.friends)
//          .sum
//          .toDouble / cluster.length
//        var meanFavouriteCount = cluster
//          .map(x => dataset(indexMapper(x)).tweet.favourites)
//          .sum
//          .toDouble / cluster.length
//
//        //        requestAttribute :+= s""""cluster_${clusterID}_size" : ${cluster.length}"""
//        //        requestAttribute :+= s""""cluster_${clusterID}_followers" : ${meanFollowersCount}"""
//        //        requestAttribute :+= s""""cluster_${clusterID}_friends" : ${meanFriendsCount}"""
//        //        requestAttribute :+= s""""cluster_${clusterID}_favourite" : ${meanFavouriteCount}"""
//
//        //find significance score of words in cluster
//        //store word length
//        var wordProbability = Map[String, Double]()
//        var wordFreInDoc = Map[String, Int]()
//        var wordSizeList = Array[Int]()
//        var tweetPerUser = Map[String, Int]()
//        //for each index of cluster
//        for (index <- cluster) {
//          var userID = dataset(indexMapper(index)).tweet.userID
//          if (!(tweetPerUser isDefinedAt userID)) {
//            tweetPerUser += (userID -> 0)
//          }
//          tweetPerUser(userID) += 1
//          var wordFre = Map[String, Double]()
//          var content = dataset(indexMapper(index)).tweet.content
//          var words = content.trim.replaceAll(" +", " ").split(" +")
//          wordSizeList :+= words.length
//          for (word <- words) {
//            if (!(wordFre isDefinedAt word)) {
//              wordFre += (word -> 0.0)
//            }
//            wordFre(word) += 1
//          }
//          // //find max
//          // var maxFre = 0.0
//          // for ((k, v) <- wordFre) {
//          //   if (v > maxFre) {
//          //     maxFre = v
//          //   }
//          // }
//          //divide all by maxFre
//          for ((k, v) <- wordFre) {
//            //add to probability
//            if (!(wordProbability isDefinedAt k)) {
//              wordProbability += (k -> wordFre(k))
//            } else {
//              wordProbability(k) += wordFre(k)
//            }
//          }
//        }
//
//        //calculate tweet per user
//        var tpu = 0.0
//        for ((k, v) <- tweetPerUser) {
//          tpu += v
//        }
//        tpu /= tweetPerUser.size
//        //        requestAttribute :+= s""""cluster_${clusterID}_user" : ${tweetPerUser.size}"""
//        //        requestAttribute :+= s""""cluster_${clusterID}_tpu" : ${tpu}"""
//
//        // write to influxdb, community information, group by cluster id
//        val clusterSize: Int = cluster.length
//        val newPoint = Point
//          .measurement(measurementList(0))
//          .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
//          .tag("parent", parents(clusterID).toString)
//          .addField("clusterID", clusterID)
//          .addField("size", clusterSize)
//          .addField("tpu", tpu)
//          .addField("followers", meanFollowersCount)
//          .addField("friends", meanFriendsCount)
//          .addField("favourites", meanFavouriteCount)
//          .build()
//
//        influxdb.write(newPoint)
//
//        wordSizeList = wordSizeList.distinct
//
//        //calculate mean of score
//        for ((k, v) <- wordProbability) {
//          if (!(mostUsedWord isDefinedAt k)) {
//            mostUsedWord += (k -> 0.0)
//          }
//          val value = wordProbability(k) / cluster.length
//          if (value > mostUsedWord(k)) {
//            mostUsedWord(k) = value
//          }
//        }
//
//        clusterID += 1
//      }
//
//      // variance
//      val clusterSizeMean: Double = clusterList
//        .map(x => x.length)
//        .sum / clusterList.length
//      var variance: Double = 0.0
//      var skewness: Double = 0.0
//      var kurtosis: Double = 0.0
//
//      for (cluster <- clusterList) {
//        variance += math.pow(cluster.length - clusterSizeMean, 2)
//        skewness += math.pow(cluster.length - clusterSizeMean, 3)
//        kurtosis += math.pow(cluster.length - clusterSizeMean, 4)
//      }
//      variance = variance / clusterList.length
//      skewness = skewness / (clusterList.length * math.pow(math.sqrt(variance),
//                                                           3))
//      kurtosis = kurtosis / (clusterList.length * math.pow(math.sqrt(variance),
//                                                           4))
//
//      val newCommonPoint = Point
//        .measurement(measurementList(1))
//        .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
//        .addField("skewness", skewness)
//        .addField("kurtosis", kurtosis)
//        .addField("variance", variance)
//        .build()
//      influxdb.write(newCommonPoint)
//
//      //      requestAttribute :+= s""""num_of_word" : ${mostUsedWord.size}"""
//
//      var wordID = 0
//      for ((k, v) <- mostUsedWord) {
//        if (v != "") {
//          //          requestAttribute :+= s""""word_${wordID}" : "${k}""""
//          //          requestAttribute :+= s""""word_${wordID}_score" : ${v}"""
//          val newWordPoint = Point
//            .measurement(measurementList(2))
//            .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
//            .tag("wordID", k)
//            .addField("score", v)
//            .build()
//          influxdb.write(newWordPoint)
//
//          wordID = wordID + 1
//        }
//      }
//
//      //      var request = "{" + requestAttribute.mkString(",") + "}"
//
//      //Send to python to visualize this
//      //      val host = redisVisual.get("host").asInstanceOf[String]
//      //      val port = redisVisual.get("port").asInstanceOf[Int]
//      //      val db = redisVisual.get("database").asInstanceOf[Int]
//      //      val re = new RedisClient(host, port, db)
//
//      //      while (re.get("visual_request").isDefined) {
//      //        Thread.sleep(1000)
//      //      }
//      //      re.set("visual_request", request)
//      //
//      //      //check that avatite receive
//      //      while (re.get("visual_receive").isEmpty) {
//      //        Thread.sleep(1000)
//      //      }
//      //      re.del("visual_receive")
//      //      println("Visual request sent")
//    }
//
//    duration = duration + (t1 - t0)
////    println("Start KMeans..")
////    val kmeans = new KMEANS()
////      .setCoreset(clusterList)
////      .setCheatMode("cheat")
////
////    val clusteredDataset: Dataset = kmeans.fit(dataset)
//
//    return dataset
//  }
//
//  def closeInfluxDBConnection(): Unit = {
//    influxdb.close()
//  }
//
//  def closePythonConnection(): Unit = {
//    //send message to close connection
//    val avatLiteHost = redisAVATLite.get("host").asInstanceOf[String]
//    val avatLitePort = redisAVATLite.get("port").asInstanceOf[Int]
//    val database = redisAVATLite.get("database").asInstanceOf[Int]
//    val re = new RedisClient(avatLiteHost, avatLitePort, database)
//    re.set("is_join", "true")
//    breakable {
//      //maximum: 10s = 10000 milliseconds
//      var start: Long = System.currentTimeMillis
//      while (true) {
//        val status = re.get("is_close")
//        if (status.isDefined) {
//          re.del("is_close")
//          if (status.get == "true") {
//            break
//          } else {
//            Thread.sleep(1000)
//          }
//        }
//      }
//    }
//
//    //    if (avatMode == "avatlite_idsa_streaming") {
//    //      val visualHost = redisVisual.get("host").asInstanceOf[String]
//    //      val visualPort = redisVisual.get("port").asInstanceOf[Int]
//    //      val vDatabase = redisVisual.get("database").asInstanceOf[Int]
//    //      val re = new RedisClient(visualHost, visualPort, vDatabase)
//    //      re.set("is_join", "true")
//    //      breakable {
//    //        //maximum: 10s = 10000 milliseconds
//    //        var start: Long = System.currentTimeMillis
//    //        while (true) {
//    //          val status = re.get("is_close")
//    //          if (status.isDefined) {
//    //            re.del("is_close")
//    //            if (status.get == "true") {
//    //              println("Successfully close Python visual module process")
//    //              break
//    //            } else {
//    //              Thread.sleep(1000)
//    //              println("Closing ..")
//    //            }
//    //          }
//    //          Thread.sleep(1000)
//    //        }
//    //      }
//    //    }
//  }
//}
