package metrics

import scala.math.{sqrt, abs, pow}
import scala.collection.mutable.Map
import utils._

import utils.{Dataset, Measure, DataPoint}

class CDbwIndex extends Index[CDbwIndex] {
  def process(dataset: Dataset): Dataset = {
    //find sample set
    value = 0.0
    var sampleSet = new Dataset()
    for (dp <- dataset.getDataset()) {
      if (dp.index == dp.rep) { //representative point
        sampleSet :+= dp
      }
    }

    //find grouper of sample set
    var grouper = Map[Int, Array[Int]]()
    //group points that are in the same cluster
    for (dp <- sampleSet.getDataset()) {
      if (!grouper.keySet.exists(_ == dp.cluster)) {
        grouper = grouper + (dp.cluster -> Array[Int]())
      }
      grouper(dp.cluster) :+= dp.index
    }
    var co = cohesion(grouper, dataset)
    var sep = separation(grouper, dataset)

    value = co * sep

    return dataset
  }

  private def cr(i: Int,
                 j: Int,
                 grouper: Map[Int, Array[Int]],
                 dataset: Dataset): Set[Set[Int]] = {
    //CR(i)(j)
    var crSet = Set[Set[Int]]()
    for (v <- grouper(i)) {
      var minDist = Double.MaxValue
      var minIdx = -1
      for (t <- grouper(j)) {
        var dist = distanceFunction(dataset(v), dataset(t))
        if (dist < minDist) {
          minDist = dist
          minIdx = t
        }
      }
      crSet += Set[Int](v, minIdx)
    }
    return crSet
  }

  private def rcr(i: Int,
                  j: Int,
                  grouper: Map[Int, Array[Int]],
                  dataset: Dataset): Array[Set[Int]] = {
    var rcrArray =
      cr(i, j, grouper, dataset).intersect(cr(j, i, grouper, dataset)).toArray
    return rcrArray
  }

  private def stdev(clusters: Array[Int],
                    grouper: Map[Int, Array[Int]],
                    dataset: Dataset): Double = {
    var avgStd = 0.0
    for (index <- clusters) {
      var mean = new Array[Double](dataset(0).coord.size)
      for (x <- grouper(index)) {
        for (t <- 0 to dataset(0).coord.size - 1) {
          mean(t) = mean(t) + dataset(x).coord(t) / grouper(index).size
        }
      }
      var dev = 0.0
      for (x <- grouper(index)) {
        dev += pow(distanceFunction(dataset(x), new DataPoint(coord = mean)), 2)
      }
      avgStd += sqrt(dev / grouper(index).size)
    }
    return avgStd / clusters.size
  }

  private def cardinality(middlePoint: Array[Double],
                          i: Int,
                          j: Int,
                          radius: Double,
                          grouper: Map[Int, Array[Int]],
                          dataset: Dataset): Double = {
    //first cluster iteration
    var car = 0.0
    var iterator = Array[Array[Int]](grouper(i), grouper(j))
    for (g <- iterator) {
      for (n <- g) {
        var dist =
          distanceFunction(new DataPoint(coord = middlePoint), dataset(n))
        if (dist < radius) {
          car = car + 1
        }
      }
    }
    return car / (grouper(i).size + grouper(j).size)
  }

  private def cardinality(point: Array[Double],
                          i: Int,
                          radius: Double,
                          grouper: Map[Int, Array[Int]],
                          dataset: Dataset): Double = {
    //first cluster iteration
    var car = 0.0
    for (n <- grouper(i)) {
      var dist = distanceFunction(new DataPoint(coord = point), dataset(n))
      if (dist < radius) {
        car = car + 1
      }
    }
    return car / grouper(i).size

  }

  private def dens(i: Int,
                   j: Int,
                   grouper: Map[Int, Array[Int]],
                   dataset: Dataset): Double = {
    var rcrArray = rcr(i, j, grouper, dataset)
    var densVal = 0.0
    //find stdev
    var s = stdev(Array[Int](i, j), grouper, dataset)
    for (v <- rcrArray) {
      //find u
      var arr = v.toArray
      var u = new Array[Double](dataset(0).coord.size)
      for (t <- 0 to dataset(0).coord.size - 1) {
        u(t) = (dataset(arr(0)).coord(t) + dataset(arr(1)).coord(t)) / 2
      }
      var dClosSep = distanceFunction(dataset(arr(0)), dataset(arr(1)))
      var car = cardinality(u, i, j, s, grouper, dataset)
      densVal = densVal + ((dClosSep * car) / (2 * s))
    }
    return densVal / rcrArray.size
  }

  private def interDens(grouper: Map[Int, Array[Int]],
                        dataset: Dataset): Double = {
    var interDensVal = 0.0
    if (grouper.size <= 1) {
      print("0 or 1 cluster. returning 0.0")
      return 0.0
    }
    for ((i, vi) <- grouper) {
      var maxDens = 0.0
      for ((j, vj) <- grouper) {
        if (i != j) {
          var d = dens(i, j, grouper, dataset)
          if (d > maxDens) {
            maxDens = d
          }
        }
      }
      interDensVal = interDensVal + maxDens
    }
    return interDensVal / grouper.size
  }

  private def cen(i: Int,
                  grouper: Map[Int, Array[Int]],
                  dataset: Dataset): Array[Double] = {
    var cenPoint = new Array[Double](dataset(0).coord.size)
    for (n <- grouper(i)) {
      for (idx <- 0 to cenPoint.size - 1) {
        cenPoint(idx) = cenPoint(idx) + (dataset(n).coord(idx) / grouper(i).size)
      }
    }
    return cenPoint
  }

  private def densCL(radius: Double,
                     grouper: Map[Int, Array[Int]],
                     dataset: Dataset,
                     s: Double = 0.0): Double = {
    var densCLVal = 0.0
    for ((i, v) <- grouper) {
      var jVal = 0.0
      for (n <- v) {
        var shrunkRep = new Array[Double](dataset(0).coord.size)
        var cenPoint = cen(i, grouper, dataset)
        for (d <- 0 to shrunkRep.size - 1) {
          shrunkRep(d) = dataset(n).coord(d) + s * (cenPoint(d) - dataset(n)
            .coord(d))
        }
        for (t <- 0 to shrunkRep.size - 1)
          jVal = jVal + cardinality(shrunkRep, i, radius, grouper, dataset)
      }
      jVal = jVal / grouper(i).size
      densCLVal = densCLVal + jVal
    }
    return densCLVal
  }

  private def intraDens(grouper: Map[Int, Array[Int]],
                        dataset: Dataset,
                        s: Double = 0.0): Double = {
    if (grouper.size <= 1) {
      return 0.0
    }
    var clusters = Array[Int]()
    for ((k, v) <- grouper) {
      clusters :+= k
    }

    var st = stdev(clusters, grouper, dataset)
    var densCLVal = densCL(st, grouper, dataset, s)
    var c = grouper.size
    return densCLVal / (c * st)

  }

  private def sep(grouper: Map[Int, Array[Int]], dataset: Dataset): Double = {
    var sepVal = 0.0
    if (grouper.size <= 1) {
      print("0 or 1 cluster. returning 0.0")
      return 0.0
    }
    for ((i, vi) <- grouper) {
      var minDist = Double.MaxValue
      for ((j, vj) <- grouper) {
        if (i != j) {
          var dist = 0.0
          var rcrArray = rcr(i, j, grouper, dataset)
          for (v <- rcrArray) {
            var arr = v.toArray
            dist = dist + distanceFunction(dataset(arr(0)), dataset(arr(1)))
          }
          dist = dist / rcrArray.size
          if (dist < minDist) {
            minDist = dist
          }
        }
      }
      sepVal = sepVal + minDist
    }
    sepVal = sepVal / grouper.size
    return sepVal / (1 + interDens(grouper, dataset))
  }

  private def compactness(grouper: Map[Int, Array[Int]],
                          dataset: Dataset): Double = {
    var accuS = 0.0
    var times = 0
    for (s <- 0.1 to 0.8 by 0.1) {
      times = times + 1
      accuS = accuS + intraDens(grouper, dataset, s)
    }
    return accuS / times
  }

  private def intraChange(grouper: Map[Int, Array[Int]],
                          dataset: Dataset): Double = {
    var accuIntraChange = 0.0
    var times = 0
    for (s <- 0.2 to 0.8 by 0.1) {
      times = times + 1
      accuIntraChange = accuIntraChange + abs(
        intraDens(grouper, dataset, s) - intraDens(grouper, dataset, s - 0.1))
    }
    return accuIntraChange / times
  }

  private def cohesion(grouper: Map[Int, Array[Int]],
                       dataset: Dataset): Double = {
    return compactness(grouper, dataset) / (1 + intraChange(grouper, dataset))
  }

  private def separation(grouper: Map[Int, Array[Int]],
                         dataset: Dataset): Double = {
    return sep(grouper, dataset) * compactness(grouper, dataset)
  }
}
