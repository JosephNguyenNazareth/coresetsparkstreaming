package estimation

import scala.math._
import scala.util.Random

import utils.{Dataset, DataPoint, Measure, PipelineOperator}

abstract class LLLML[T <: LLLML[T]] extends PipelineOperator {
  protected var distanceFunction =
    (firstPoint: DataPoint, secondPoint: DataPoint) => {
      Measure.euclide(firstPoint, secondPoint)
    }
  protected var numClus: Int = 0

  def setDistanceFunction(df: (DataPoint, DataPoint) => Double): T = {
    distanceFunction = df
    return this.asInstanceOf[T]
  }

  def getEstimated(): Int = {
    return numClus
  }

  def process(dataset: Dataset): Dataset = {
    return fit(dataset)
  }

  private def _simpleKmeansFit(dataset: Dataset, k: Int): Array[DataPoint] = {
    var samples = _ramdomizeCentroids(dataset, k)
    var maxIter = 100
    for (iter <- 1 to maxIter) {
      samples = _updateCentroids(dataset, samples)
      _assignNearest(dataset, samples)
    }

    return samples
  }

  private def _ramdomizeCentroids(dataset: Dataset,
                                  k: Int): Array[DataPoint] = {
    var randomIndices = Array[Int]()
    val random = new Random()
    while (randomIndices.length < k) {
      randomIndices :+= random.nextInt(dataset.length)
    }

    var initialCentroids: Array[DataPoint] = Array[DataPoint]()

    //create a copy of centroids
    for (i <- randomIndices) {
      dataset(i).cluster = i
      initialCentroids :+= new DataPoint(dataset(i))
      initialCentroids(initialCentroids.length - 1).index = -1
      initialCentroids(initialCentroids.length - 1).cluster = -1
    }

    _assignNearest(dataset, initialCentroids)

    return initialCentroids

  }

  private def _updateCentroids(
      dataset: Dataset,
      centroids: Array[DataPoint]): Array[DataPoint] = {
    var n: Int = dataset.length
    var k: Int = centroids.length
    var d: Int = centroids(0).coord.length

    var newCentroids: Array[DataPoint] = Array[DataPoint]()

    for (cluster <- 0 to k - 1) {
      centroids(cluster).coord = Array.fill[Double](d)(0.0)
      var size: Int = 0
      for (i <- 0 to n - 1) {
        if (dataset(i).cluster == cluster) {
          size = size + 1
          if (size == 1) {
            newCentroids :+= new DataPoint(centroids(cluster))
          }
          var last: Int = newCentroids.length - 1
          for (j <- 0 to d - 1) {
            newCentroids(last)
              .coord(j) = newCentroids(last).coord(j) + dataset(i).coord(j)
          }
        }
      }

      var last: Int = newCentroids.length - 1

      if (size >= 1) {
        for (j <- 0 to d - 1) {
          newCentroids(last).coord(j) = newCentroids(last).coord(j) / size
        }
      }
    }
    return newCentroids
  }

  private def _assignNearest(dataset: Dataset, centroids: Array[DataPoint]) {
    //update cluster index of dataset first time only
    for (i <- 0 to dataset.length - 1) {
      var minDist: Double = Double.MaxValue
      var minIdx = -1
      for (c <- 0 to centroids.length - 1) {
        if (minDist > distanceFunction(centroids(c), dataset(i))) {
          minDist = distanceFunction(centroids(c), dataset(i))
          minIdx = c
        }
      }
      dataset(i).cluster = minIdx
    }
  }

  //abstract function
  def leapModule(centers: Array[DataPoint]): Double

  def fit(dataset: Dataset): Dataset = {
    //run LL
    var kMin: Int = 2
    var kMax: Int = pow(dataset.length, 0.5).toInt
    for (k <- kMin to kMax) {
      var cens = _simpleKmeansFit(dataset, k)
      numClus = leapModule(cens).toInt

    }
    return dataset
  }
}
