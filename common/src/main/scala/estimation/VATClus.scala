package estimation

import scala.io.Source
import sys.process._
import scala.math._

import java.awt.image.BufferedImage
import java.awt.image.WritableRaster
import java.awt.image.Raster
import java.awt.Graphics2D
import javax.imageio.ImageIO
import java.awt.Color

import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartPanel
import org.jfree.chart.ChartUtils
import org.jfree.chart.JFreeChart
import org.jfree.chart.plot.XYPlot
import org.jfree.data.xy.XYDataset
import org.jfree.data.xy.XYSeries
import org.jfree.data.xy.XYSeriesCollection

import javax.swing.JFrame
import javax.swing.JPanel
import java.util.UUID.randomUUID
import java.io.IOException

import scala.util.matching.Regex
import reflect.io._
import scala.io._

import java.util.Properties
import java.util.Arrays

import scala.collection.JavaConversions._

import scala.util.control.Breaks._

import PartialFunction.{cond => when}
import utils._
import vat_family._

import utils.{PipelineOperator, DataPoint, Dataset, Measure}

import vat_family.{IVAT, VAT}

import com.redis._

import scala.util.matching.Regex
import reflect.io._

import PartialFunction.{cond => when}

import configuration.YAMLConfig

import scala.collection.mutable.{Map}

class VATClus extends PipelineOperator {
  private var vatMode: String = "ivat" //vat
  private var avatMode
    : String = "avatlite" //avat, avatlite, avatlite_idsa, avatlite_idsa_streaming
  private var distanceFunction =
    (firstPoint: DataPoint, secondPoint: DataPoint) => {
      Measure.euclide(firstPoint, secondPoint)
    }
  private var numClus = Array[Int]()
  private var ratio: Double = 0.00088
  private var epsilon: Double = 0.5
  private var percentage: Double = 0.8
  private var thresholdLevel: Int = 2 //for idsa
  private var numGen: Int = 200
  private var population: Int = 50
  private var avatLiteThread: Option[Thread] = None
  private val redisConfig =
    YAMLConfig.get("redis").asInstanceOf[java.util.LinkedHashMap[String, Any]]
  private val redisAVATLite = redisConfig
    .get("redis_avatlite")
    .asInstanceOf[java.util.LinkedHashMap[String, Any]]
  private var orderList = Array[Int]()
  private var clusterList = Array[Array[Int]]()
  private var rawClusterList = Array[String]()
  private var parents = Array[Int]()
  private var idsaClusterList = Array[Array[Array[Int]]]() //for unmerge idsa

  //special variable only for streaming on social network
  private val redisVisual = redisConfig
    .get("redis_visual")
    .asInstanceOf[java.util.LinkedHashMap[String, Any]]
  private var visualThread: Option[Thread] = None
  private var streamingVATImage = Array[Array[Double]]()
  private var streamingIVATImage = Array[Array[Double]]()

  def setAVATLiteThread(t: Option[Thread]): VATClus = {
    avatLiteThread = t
    return this
  }

  def getEstimated(): Array[Int] = {
    return numClus
  }

  def getClusterList(): Array[Array[Int]] = {
    return clusterList
  }

  def getIDSAClusterList(): Array[Array[Array[Int]]] = {
    return idsaClusterList
  }

  def setVATMode(v: String): VATClus = {
    if (v != "ivat" && v != "vat") {
      println("Unknown vat mode: Using default")
      vatMode = "ivat"
    } else {
      vatMode = v
    }
    return this
  }

  def setRatio(m: Double): VATClus = {
    ratio = m
    return this
  }

  def setEpsilon(m: Double): VATClus = {
    epsilon = m
    return this
  }

  def setPercentage(m: Double): VATClus = {
    percentage = m
    return this
  }

  def setDistanceFunction(df: (DataPoint, DataPoint) => Double): VATClus = {
    distanceFunction = df
    return this
  }

  def setAVATMode(av: String): VATClus = {
    if (av != "avat" && av != "avatlite" && av != "avatlite_idsa" && av != "avatlite_idsa_streaming") {
      println("Unknown avat mode: Using default")
      avatMode = "avatlite"
    } else {
      avatMode = av
    }
    return this
  }

  def setThresholdValueForIDSA(value: Int): VATClus = {
    /*
        Set threshold value only for IDSA
     */
    if (value <= 0) {
      println("Invalid value for idsa")
    } else {
      thresholdLevel = value
    }
    return this
  }

  def setPopulationForIDSA(value: Int): VATClus = {
    /*
        Set number of population for IDSA
     */
    if (value <= 0) {
      print("Invalid value for idsa")
    } else {
      population = value
    }
    return this
  }

  def setNumGenerationForIDSA(value: Int): VATClus = {
    /*
        Set number of generation for IDSA
     */
    if (value <= 0) {
      print("Invalid value for idsa")
    } else {
      numGen = value
    }
    return this
  }

  def process(dataset: Dataset): Dataset = {
    return fit(dataset)
  }

  private def initPythonClient(): Unit = {
    if (avatMode == "avatlite" || avatMode == "avatlite_idsa" || avatMode == "avatlite_idsa_streaming") {
      if (avatLiteThread.isEmpty) {
        val avatLiteHost = redisAVATLite.get("host").asInstanceOf[String]
        val avatLitePort = redisAVATLite.get("port").asInstanceOf[Int]
        val database = redisAVATLite.get("database").asInstanceOf[Int]
        val re = new RedisClient(avatLiteHost, avatLitePort, database)
        avatLiteThread = Some(new Thread {
          override def run() {
            s"python3 -m common_python.estimation.aVATLite --host ${avatLiteHost} --port ${avatLitePort} --db ${database}" !
          }
        })
        //start thread
        avatLiteThread.get.start()
        //waiting for python to finish processing
        breakable {
          //maximum: 10s = 10000 milliseconds
          var start: Long = System.currentTimeMillis
          while (true) {
            val status = re.get("avatlite_status")
            if (status.isDefined) {
              if (status.get == "success") {
                println("Successfully connect to python through Redis.")
                break
              } else {
                // incorrect signal
                println(
                  "Failed connecting to Python through Redis. Exiting ...")
                avatLiteThread.get.stop()
                System.exit(1)
              }
            }
            if (System.currentTimeMillis - start > 10000) {
              println("Failed connecting to Python through Redis. Exiting ...")
              avatLiteThread.get.stop()
              System.exit(1)
            } else {
              Thread.sleep(1000)
              println("Connecting ..")
            }
          }
        }
      }
    }

    //visual connection for avatlite_idsa_streaming
    if (avatMode == "avatlite_idsa_streaming") {
      if (visualThread.isEmpty) {
        val visualHost = redisVisual.get("host").asInstanceOf[String]
        val visualPort = redisVisual.get("port").asInstanceOf[Int]
        val vDatabase = redisVisual.get("database").asInstanceOf[Int]
        val re = new RedisClient(visualHost, visualPort, vDatabase)
//        visualThread = Some(new Thread {
//          override def run {
////            s"python3 -m common_python.visual.dashboard --host ${visualHost} --port ${visualPort} --db ${vDatabase}" !
//          }
//        })
//        //start thread
//        visualThread.get.start
        //waiting for python to finish processing
        breakable {
          var start: Long = System.currentTimeMillis
          while (true) {
            val status = re.get("visual_status")
            if (status.isDefined) {
              if (status.get == "success") {
                println(
                  "Successfully connect to python visual module through Redis.")
                break
              } else {
                //incorrect signal
                println(
                  "Failed connecting to Python visual module through Redis. Exiting ...")
                avatLiteThread.get.stop()
                System.exit(1)
              }
            }
            if (System.currentTimeMillis - start > 10000) {
              println("Failed connecting to Python through Redis. Exiting ...")
              visualThread.get.stop()
              System.exit(1)
            } else {
              Thread.sleep(1000)
              println("Connecting ..")
            }
          }
        }
      }
    }
  }

  private def avatlite(dataset: Dataset) = {
    val avatLiteHost = redisAVATLite.get("host").asInstanceOf[String]
    val avatLitePort = redisAVATLite.get("port").asInstanceOf[Int]
    val database = redisAVATLite.get("database").asInstanceOf[Int]
    val re = new RedisClient(avatLiteHost, avatLitePort, database)
    var uuid: String = randomUUID.toString
    //send a request to python process
    //process order list got from vat or ivat
    //TODO: Using join
    var joinedObjectOrder: String = orderList.mkString(" ")
    var request = s"""{
            "id" : "${uuid}",
            "order" : "${joinedObjectOrder}",
            "file_core" : "${dataset.getFileCore()}",
            "mode" : "${avatMode}",
            "epsilon" : ${epsilon.toString},
            "percentage" : ${percentage.toString},
            "ratio" : ${ratio.toString},
            "num_gen" : ${numGen},
            "population" : ${population},
            "threshold_level" : ${thresholdLevel},
            "status" : "wait",
        }"""
    //while python has not yet deleted this key
    while (re.get("avatlite_request").isDefined) {
      Thread.sleep(1000)
    }

    re.set("avatlite_request", request)

    // waiting to get result
    // getting result
    var status = re.hget(uuid, "status")
    while (status.isEmpty) {
      Thread.sleep(1000)
      status = re.hget(uuid, "status")
    }
    if (status.get == "fail") {
      println("Failed with aVATLite")
      System.exit(1)
    }

    if (avatMode == "avatlite_idsa") {
      idsaClusterList = new Array[Array[Array[Int]]](thresholdLevel)
      for (level <- 0 until thresholdLevel) {
        idsaClusterList(level) = Array[Array[Int]]()
        var k = re.hget(uuid, s"level_${level}")
        for (i <- 0 until k.get.toInt) {
          var cluster = re.hget(uuid, s"cluster_${level}_${i}")
          val cList = cluster.get.trim
            .replaceAll(" +", " ")
            .split(" +")
            .map(_.trim.toInt)
          idsaClusterList(level) :+= cList
        }
      }
    } else if (avatMode == "avatlite") {
      println("Start Mapping...")
      var k = re.hget(uuid, "cluster")
      for (i <- 0 until k.get.toInt) {
        var cluster = re.hget(uuid, s"cluster_${i}")
        val cList = cluster.get.trim
          .replaceAll(" +", " ")
          .split(" +")
          .map(_.trim.toInt)
          .toArray
          .asInstanceOf[Array[Int]]
        clusterList :+= cList
      }
    } else if (avatMode == "avatlite_idsa_streaming") {
      clusterList = Array[Array[Int]]()
      rawClusterList = Array[String]()
      parents = Array[Int]()
      var k = re.hget(uuid, "cluster")
      for (i <- 0 until k.get.toInt) {
        var cluster = re.hget(uuid, s"cluster_${i}")
        var parent = re.hget(uuid, s"cluster_${i}_parent")
        while (cluster.isEmpty) {
          Thread.sleep(1000)
          cluster = re.hget(uuid, s"cluster_${i}")
        }
        while (parent.isEmpty) {
          Thread.sleep(1000)
          parent = re.hget(uuid, s"cluster_${i}_parent")
        }
        val cList = cluster.get.trim
          .replaceAll(" +", " ")
          .split(" +")
          .map(_.trim.toInt)
          .toArray
          .asInstanceOf[Array[Int]]
        clusterList :+= cList
        parents :+= parent.get.toInt
      }
    }
    re.del(uuid)
  }

  def fit(dataset: Dataset): Dataset = {
    //0. Open separate process for avatlite or avatlite_idsa
    println("Start CoreK...")
    initPythonClient()
    println("Opened Python client...")
    //run vat/ivat
    if (vatMode == "vat") {
      var vat = new VAT()
        .setDistanceFunction(distanceFunction)
      if (avatMode != "avat") {
        vat.enableLogMode(false)
      }
      vat.fit(dataset)
      //get object order
      if (avatMode != "avat") {
        orderList = vat.getVATOrder()
      }
      if (avatMode == "avatlite_idsa_streaming") {
        streamingVATImage = vat.getVATImage()
      }
      duration = duration + vat.timeElapsed()
    } else {
      var ivat = new IVAT()
        .setDistanceFunction(distanceFunction)
        .setEpsilon(epsilon)
        .setPercentage(percentage)
      if (avatMode != "avat") {
        ivat.enableLogMode(false)
      }
      ivat.fit(dataset)
      //get objecct order
      if (avatMode != "avat") {
        orderList = ivat.getIVATOrder()
      }
      if (avatMode == "avatlite_idsa_streaming") {
        streamingVATImage = ivat.getVATImage()
        streamingIVATImage = ivat.getIVATImage()
      }
      duration = duration + ivat.timeElapsed()
    }

    //run avat, or avatlite
    val t0 = System.nanoTime()
    if (avatMode == "avat") {
      s"python3 -m src.main.python.estimation.aVAT ${dataset.getFileCore()}" !
    } else {
      println("Start aVATlite...")
      avatlite(dataset)
      println("Stop aVATlite...")
    }

    val t1 = System.nanoTime()

    //readfile to get number of cluster
    if (avatMode == "avatlite" || avatMode == "avat") {
      numClus = Array[Int](clusterList.length)
    } else if (avatMode == "avatlite_idsa") {
      for (image <- idsaClusterList) {
        numClus :+= image.length
      }
    } else if (avatMode == "avatlite_idsa_streaming") {
      //
      var indexMapper = Map[Int, Int]()
      for (i <- 0 until dataset.length) {
        indexMapper += (dataset(i).index -> i)
      }
      numClus :+= clusterList.length
      var requestAttribute = Array[String]()
      requestAttribute :+= s""""num_cluster" : ${clusterList.length}"""
      //add parents
      for (i <- parents.indices) {
        requestAttribute :+= s""""cluster_${i}_parent" : ${parents(i)}"""
      }
      //information that will be sent
      //mean variance skewness median
      //cluster information with size for sunburn
      //mean friend favourite follower count

      //for each cluster, calculate means of friend count, follower count
      var clusterID = 0
      var mostUsedWord = Map[String, Double]()
      for (cluster <- clusterList) {
        //calculate counts
        var meanFollowersCount = cluster
          .map(x => dataset(indexMapper(x)).tweet.followers)
          .sum
          .toDouble / cluster.length
        var meanFriendsCount = cluster
          .map(x => dataset(indexMapper(x)).tweet.friends)
          .sum
          .toDouble / cluster.length
        var meanFavouriteCount = cluster
          .map(x => dataset(indexMapper(x)).tweet.favourites)
          .sum
          .toDouble / cluster.length

        requestAttribute :+= s""""cluster_${clusterID}_size" : ${cluster.length}"""
        requestAttribute :+= s""""cluster_${clusterID}_followers" : ${meanFollowersCount}"""
        requestAttribute :+= s""""cluster_${clusterID}_friends" : ${meanFriendsCount}"""
        requestAttribute :+= s""""cluster_${clusterID}_favourite" : ${meanFavouriteCount}"""

        //find significance score of words in cluster
        //store word length
        var wordProbability = Map[String, Double]()
        var wordFreInDoc = Map[String, Int]()
        var wordSizeList = Array[Int]()
        var tweetPerUser = Map[String, Int]()
        //for each index of cluster
        for (index <- cluster) {
          var userID = dataset(indexMapper(index)).tweet.userID
          if (!(tweetPerUser isDefinedAt userID)) {
            tweetPerUser += (userID -> 0)
          }
          tweetPerUser(userID) += 1
          var wordFre = Map[String, Double]()
          var content = dataset(indexMapper(index)).tweet.content
          var words = content.trim.replaceAll(" +", " ").split(" +")
          wordSizeList :+= words.length
          for (word <- words) {
            if (!(wordFre isDefinedAt word)) {
              wordFre += (word -> 0.0)
            }
            wordFre(word) += 1
          }
          // //find max
          // var maxFre = 0.0
          // for ((k, v) <- wordFre) {
          //   if (v > maxFre) {
          //     maxFre = v
          //   }
          // }
          //divide all by maxFre
          for ((k, v) <- wordFre) {
            //add to probability
            if (!(wordProbability isDefinedAt k)) {
              wordProbability += (k -> wordFre(k))
            } else {
              wordProbability(k) += wordFre(k)
            }
          }
        }

        //calculate tweet per user
        var tpu = 0.0
        for ((k, v) <- tweetPerUser) {
          tpu += v
        }
        tpu /= tweetPerUser.size
        requestAttribute :+= s""""cluster_${clusterID}_user" : ${tweetPerUser.size}"""
        requestAttribute :+= s""""cluster_${clusterID}_tpu" : ${tpu}"""

        wordSizeList = wordSizeList.distinct

        //calculate mean of score
        for ((k, v) <- wordProbability) {
          if (!(mostUsedWord isDefinedAt k)) {
            mostUsedWord += (k -> 0.0)
          }
          val value = wordProbability(k) / cluster.length
          if (value > mostUsedWord(k)) {
            mostUsedWord(k) = value
          }
        }

        clusterID += 1
      }

      requestAttribute :+= s""""num_of_word" : ${mostUsedWord.size}"""

      var wordID = 0
      for ((k, v) <- mostUsedWord) {
        if (v != "") {
          requestAttribute :+= s""""word_${wordID}" : "${k}""""
          requestAttribute :+= s""""word_${wordID}_score" : ${v}"""
          wordID = wordID + 1
        }
      }

      var request = "{" + requestAttribute.mkString(",") + "}"

      //Send to python to visualize this
      val host = redisVisual.get("host").asInstanceOf[String]
      val port = redisVisual.get("port").asInstanceOf[Int]
      val db = redisVisual.get("database").asInstanceOf[Int]
      val re = new RedisClient(host, port, db)

      while (re.get("visual_request").isDefined) {
        Thread.sleep(1000)
      }
      re.set("visual_request", request)

      //check that avatite receive
      while (re.get("visual_receive").isEmpty) {
        Thread.sleep(1000)
      }
      re.del("visual_receive")
      println("Visual request sent")
    }

    duration = duration + (t1 - t0)

    return dataset
  }

  def closePythonConnection(): Unit = {
    //send message to close connection
    val avatLiteHost = redisAVATLite.get("host").asInstanceOf[String]
    val avatLitePort = redisAVATLite.get("port").asInstanceOf[Int]
    val database = redisAVATLite.get("database").asInstanceOf[Int]
    val re = new RedisClient(avatLiteHost, avatLitePort, database)
    re.set("is_join", "true")
    breakable {
      //maximum: 10s = 10000 milliseconds
      var start: Long = System.currentTimeMillis
      while (true) {
        val status = re.get("is_close")
        if (status.isDefined) {
          re.del("is_close")
          if (status.get == "true") {
            println("Successfully close Python process")
            break
          } else {
            Thread.sleep(1000)
            println("Closing ..")
          }
        }
      }
    }

    if (avatMode == "avatlite_idsa_streaming") {
      val visualHost = redisVisual.get("host").asInstanceOf[String]
      val visualPort = redisVisual.get("port").asInstanceOf[Int]
      val vDatabase = redisVisual.get("database").asInstanceOf[Int]
      val re = new RedisClient(visualHost, visualPort, vDatabase)
      re.set("is_join", "true")
      breakable {
        //maximum: 10s = 10000 milliseconds
        var start: Long = System.currentTimeMillis
        while (true) {
          val status = re.get("is_close")
          if (status.isDefined) {
            re.del("is_close")
            if (status.get == "true") {
              println("Successfully close Python visual module process")
              break
            } else {
              Thread.sleep(1000)
              println("Closing ..")
            }
          }
          Thread.sleep(1000)
        }
      }
    }
  }
}
