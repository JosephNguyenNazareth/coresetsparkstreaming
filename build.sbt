organization in ThisBuild := "KK"
version := "1.0"
scalaVersion in ThisBuild := "2.11.12"

lazy val coresetsparkstreaming = (project in file("."))
  .settings(
    settings,
    name := "coresetsparkstreaming",
    libraryDependencies ++= commonDependencies
  )
  .aggregate(
    common,
    cvc_streaming,
    twitter_streaming
  )
  .dependsOn(
    common,
    cvc_streaming,
    twitter_streaming
  )

lazy val common = project
  .settings(
    name := "common",
    settings,
    libraryDependencies ++= commonDependencies ++ Seq(
      "org.apache.spark" %% "spark-streaming" % "1.6.3"
    )
  )

lazy val cvc_streaming = project
  .settings(
    name := "cvc_streaming",
    settings,
    libraryDependencies ++= commonDependencies ++ Seq(
      "org.apache.spark" %% "spark-streaming-kafka" % "1.6.3",
      "org.apache.spark" %% "spark-streaming" % "1.6.3",
      "org.apache.spark" %% "spark-core" % "2.0.0" % "provided",
      "org.apache.spark" % "spark-mllib_2.11" % "2.4.4",
      "org.mongodb.spark" % "mongo-spark-connector_2.11" % "2.4.2",
      "org.mongodb" % "bson" % "4.0.5",
      "net.jpountz.lz4" % "lz4" % "1.3.0"
    )
  )
  .aggregate(common)
  .dependsOn(common)

lazy val twitter_streaming = project
  .settings(
    name := "twitter_streaming",
    settings,
    libraryDependencies ++= commonDependencies ++ Seq(
//      "org.apache.spark" %% "spark-streaming-kafka" % "1.5.2",
      "org.apache.spark" %% "spark-streaming" % "1.6.3",
      "org.apache.spark" %% "spark-core" % "1.6.3",
//      "org.apache.spark" %% "spark-sql" % "1.6.3",
      "org.apache.spark" % "spark-mllib_2.11" % "1.6.3",
      "org.apache.spark" % "spark-streaming-twitter_2.11" % "1.6.3",
//      "org.twitter4j" % "twitter4j-core" % "4.0.7",
      "org.twitter4j" % "twitter4j-stream" % "4.0.4",
      "org.mongodb.spark" % "mongo-spark-connector_2.11" % "1.1.0"
//      "org.mongodb.mongo-hadoop" % "mongo-hadoop-spark" % "1.5.2"
    )
  )
  .aggregate(common)
  .dependsOn(common)

lazy val settings =
  commonSettings ++
    wartremoverSettings ++
    scalafmtSettings

lazy val compilerOptions = Seq(
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps",
  "-deprecation",
  "-encoding",
  "utf8"
)

lazy val commonSettings = Seq(
  scalacOptions ++= compilerOptions,
  resolvers ++= Seq(
    "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
  )
)

lazy val wartremoverSettings = Seq(
  wartremoverWarnings in (Compile, compile) ++= Warts.allBut(Wart.Throw),
  wartremoverWarnings ++= Warts.all
)

lazy val scalafmtSettings =
  Seq(
    scalafmtOnCompile := true,
    scalafmtTestOnCompile := true,
    scalafmtVersion := "1.2.0"
  )

lazy val commonDependencies = Seq(
  "org.jfree" % "jfreechart-fx" % "1.0.1",
  "org.scalatest" %% "scalatest" % "3.0.8" % "test",
  "org.scalanlp" %% "breeze" % "1.0",
  "org.scalanlp" %% "breeze-natives" % "1.0",
  "org.scalanlp" %% "breeze-viz" % "1.0",
  "org.scala-lang.modules" %% "scala-swing" % "2.1.1",
  "junit" % "junit" % "4.12" % "test",
  "com.novocode" % "junit-interface" % "0.11" % "test",
  "org.json4s" %% "json4s-native" % "3.6.7",
  "com.google.code.gson" % "gson" % "2.8.6",
  "com.github.haifengl" %% "smile-scala" % "1.5.0",
  "org.yaml" % "snakeyaml" % "1.25",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.9.2",
  "org.apache.commons" % "commons-text" % "1.8",
  "net.debasishg" %% "redisclient" % "3.30",
  "com.google.code.gson" % "gson" % "2.8.6",
  "org.scala-lang.modules" % "scala-xml_2.11" % "1.3.0",
  "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.11.0",
  "com.vdurmont" % "emoji-java" % "5.1.1",
  "com.github.master" % "spark-stemming_2.10" % "0.2.1",
  "commons-io" % "commons-io" % "2.7",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.2",
  "org.influxdb" % "influxdb-java" % "2.15",
  "com.opencsv" % "opencsv" % "5.3",
//  "dsol" % "dsol" % "1.6.9",
  "nz.ac.waikato.cms.moa" % "moa-pom" % "2020.07.1"
//  "nz.ac.waikato.cms.moa" % "moa" % "2020.07.1"
)

lazy val assemblySettings = Seq(
  assemblyJarName in assembly := name.value + "-assembly.jar"
)
