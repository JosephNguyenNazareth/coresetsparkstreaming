package metrics

import utils.Dataset

import scala.collection.mutable.Map
import scala.math.min

class SilhouetteIndex extends Index[SilhouetteIndex] {
  private def intraClusterDistance(
      dataset: Dataset,
      grouper: Map[Int, Array[Int]]): Array[Double] = {
    var intra: Array[Double] = Array[Double]()
    for ((i, _) <- grouper) {
      val currentGroupSize: Double = grouper(i).length.toDouble
      for (x <- grouper(i)) {
        var singlePointTotalDistance: Double = 0.0
        for (y <- grouper(i)) {
          if (x != y) {
            singlePointTotalDistance += distanceFunction(dataset(x), dataset(y))
          }
        }
        intra :+= singlePointTotalDistance / (currentGroupSize)
      }
    }

    return intra
  }

  private def interClusterDistance(
      dataset: Dataset,
      grouper: Map[Int, Array[Int]]): Array[Double] = {
    var inter: Array[Double] = Array[Double]()
    for ((i, _) <- grouper) {
      for (x <- grouper(i)) {
        var minInter: Double = Double.MaxValue
        for ((j, _) <- grouper) {
          if (i != j) {
            val currentGroupSize: Double = grouper(j).length.toDouble
            var singlePointTotalDistance: Double = 0.0
            for (y <- grouper(j))
              singlePointTotalDistance += distanceFunction(dataset(x),
                                                           dataset(y))
            minInter =
              min(minInter, singlePointTotalDistance / currentGroupSize)
          }
        }
        inter :+= minInter
      }
    }

    return inter
  }

  private def silhouette(intra: Array[Double], inter: Array[Double]): Double = {
    var silhouetteValue: Double = 0.0
    for (i <- intra.indices) {
      var singlePointSilhouette: Double = 0.0
      if (intra(i) < inter(i))
        singlePointSilhouette = 1 - intra(i) / inter(i)
      else if (intra(i) > inter(i))
        singlePointSilhouette = inter(i) / intra(i) - 1
      silhouetteValue += singlePointSilhouette
    }

    return silhouetteValue / inter.length
  }

  def process(dataset: Dataset): Dataset = {
    var grouper = Map[Int, Array[Int]]()

    //group points that are in the same cluster
    for (dp <- dataset.getDataset()) {
      if (!grouper.keySet.contains(dp.cluster)) {
        grouper(dp.cluster) = Array[Int]()
      }
      grouper(dp.cluster) +:= dp.index
    }

    val intraDistance: Array[Double] = intraClusterDistance(dataset, grouper)
    val interDistance: Array[Double] = interClusterDistance(dataset, grouper)
    value = silhouette(intraDistance, interDistance)
    println("Done Silhouette")

    return dataset
  }
}
