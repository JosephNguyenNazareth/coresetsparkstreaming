package test

import clustering.KMEANS
import metrics.{CDbwIndex, DaviesBouldinIndex, DunnIndex, SilhouetteIndex}
import utils.{DataPoint, Dataset, Pipeline, SystemUtils}
import java.io.{BufferedWriter, File, FileWriter}

import scala.util.control.Breaks._
import streaming.StreamingModel
import scala.math.pow

import utils.DataFile

object SimulationOffline {
  val TRACE: Boolean = true
  def main(args: Array[String]): Unit = {
    // Default arguments
    var datafile: Array[DataFile] = Array[DataFile]();
    // datafile :+= new DataFile("spambase.data", -1, 2000, 20, 0.1)
//    datafile :+= new DataFile("kddcup.data_new", 1000, 8000, 2000, 0.005)
    datafile :+= new DataFile("covtype_data", -1, 8000, 500, 0.7)
//    datafile :+= new DataFile("USCensus1990.data", -1, 8000, 500, 0.7)
    // datafile :+= new DataFile("BigCross.data", -1, 10000, 5000, 0.5)

    var prefixDir = "./datasets/streamkm/"
    var cleanCache = false

    if (cleanCache) {
      SystemUtils.cleanDirectory()
    }
    var counter: Int = 0
    if (datafile.length != 0) {
      for (i <- datafile.indices) {
        val file: String = datafile(i).fileCore
        println(
          "======= Streaming Coreset Clustering on dataset " + file + " =======")
        //Initialize dataset
        var row: Int = 0;
        val batchSize: Int = datafile(i).batchSize
        StreamingModel.setAlgorithm("corekmeans")
        StreamingModel.setBatchSize(datafile(i).streamingBatchSize)
        StreamingModel.setEpsilon(datafile(i).epsilon)

        val t1 = System.nanoTime()
        var dataset: Dataset = new Dataset()
          .setPrefix(prefixDir)
          .setTargetFile(file)
          .setLabelColumn(datafile(i).labelColumn)
        val ds: Array[Array[Double]] =
          dataset.readFileStreaming(",")

        breakable {
          while (row < ds.length) {
            var prepareDataPointList: Array[DataPoint] = Array[DataPoint]()
            breakable {
              while (true) {
                prepareDataPointList :+= new DataPoint(row, coord = ds(row))
                if ((row != 0 && (row + 1) % batchSize == 0) || row == ds.length - 1) {
                  row = row + 1
                  break
                }
                row = row + 1
              }
            }
            val partDataset: Dataset = new Dataset()
              .setPrefix(prefixDir)
              .setTargetFile(file)
            partDataset.setDataset(prepareDataPointList)

//            val dataLeft: Dataset = dataset
            StreamingModel.parseDataStreaming(partDataset)
            println("Processed line " + row)
          }
        }
        var clusterData: Dataset = new Dataset()
        if (TRACE) {
//          val sampleData: Dataset = StreamingModel.getOutput()
          StreamingModel.clustering()
          clusterData = StreamingModel.getOutput()
          StreamingModel.writeToFileStreaming(clusterData)
//          StreamingModel.saveScatterChart(dataset)
        }

        val t2 = System.nanoTime()
        val duration: Double = (t2 - t1) / 1000000000.0
//        println("Duration: " + duration + "s")
        StreamingModel.closeConnection()

        // time for some metrics
        // first reassign point index
        val dataPointProcess: Array[DataPoint] = clusterData.getDataset()
        for (x <- 0 until clusterData.length()) {
          dataPointProcess(x).index = x
        }
        val clusterDataReassign: Dataset = new Dataset(dataPointProcess)

        val pipeline: Pipeline = new Pipeline()
        val daviesBouldinIndex = new DaviesBouldinIndex()
        val dunnIndex = new DunnIndex()
        val silhouetteIndex = new SilhouetteIndex()

        pipeline.addOperator(daviesBouldinIndex)
        pipeline.addOperator(dunnIndex)
        pipeline.addOperator(silhouetteIndex)
        pipeline.fit(clusterDataReassign)
        pipeline.transform()

        // final result
        val bw = new BufferedWriter(
          new FileWriter(
            new File("./result/log/streaming/result_" + file + ".txt"),
            true))
        bw.write(
          "\n########################################## RESULT FINISHED ##########################################\n")
        bw.write(s"Epsilon: ${datafile(i).epsilon}\n")
//        bw.write(s"Percentage: ${percentage}\n")
        bw.write(s"Number of cluster: ${StreamingModel.getCluster()}\n")
        bw.write(s"Dataset name: ${dataset.getFileCore()}\n")
        bw.write(s"Coreset: ${clusterData.length()}\n")
        bw.write(s"Origin: ${ds.length}\n")
        bw.write(s"DaviesBouldinIndex : ${daviesBouldinIndex.getValue()}\n")
        bw.write(s"DunnIndex : ${dunnIndex.getValue()}\n")
        bw.write(s"SilhouetteIndex : ${silhouetteIndex.getValue()}\n")
        val mb = 1024 * 1024
        val runtime = Runtime.getRuntime
        bw.write(
          "** Used Memory:  " + (runtime.totalMemory - runtime.freeMemory) / mb + " MB\n")
        bw.write("** Free Memory:  " + runtime.freeMemory / mb + " MB\n")
        bw.write("** Total Memory: " + runtime.totalMemory / mb + " MB\n")
        bw.write("** Max Memory:   " + runtime.maxMemory / mb + " MB\n")
        bw.write(s"Duration: ${duration}s\n")
        bw.write(
          "#####################################################################################################\n")
        bw.close()
      }
    } else {
      println("Warning: No file cores specified")
    }
  }
}
