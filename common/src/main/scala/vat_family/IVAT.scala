/*
private var areaThreshold : Double = 0.00088

def setAreaThreshold(a : Double) : T = {
        areaThreshold = a
        return this.asInstanceOf[T]

    }
 */
package vat_family

import utils.{Dataset, DataPoint}

class IVAT extends VAT {
  override def toString: String = "ivat"

  private var ivatImage: Array[Array[Double]] = Array[Array[Double]]()
  private var ivatObjectOrder: Array[Int] = Array[Int]()

  override def process(dataset: Dataset): Dataset = {
    duration = 0
    return fit(dataset)
  }

  override def setDistanceFunction(
      df: (DataPoint, DataPoint) => Double): IVAT = {
    distanceFunction = df
    return this
  }

  override def setEpsilon(ep: Double): IVAT = {
    epsilon = ep
    return this
  }

  override def setPercentage(per: Double): IVAT = {
    percentage = per
    return this
  }

  def getIVATImage(): Array[Array[Double]] = {
    return ivatImage
  }

  def getIVATOrder(): Array[Int] = {
    return ivatObjectOrder
  }

  override def fit(dataset: Dataset): Dataset = {
    //call VAT first
    super.fit(dataset) //Caution: duration already calculated at this moment
    ivatObjectOrder = objectOrder

    //perform iVAT
    val t0 = System.nanoTime()
    ivatImage = new Array[Array[Double]](objectOrder.length)
    for (i <- ivatObjectOrder.indices) {
      ivatImage(i) = new Array[Double](ivatObjectOrder.length)
    }

    for (r <- 1 until vatImage.length) {
      var minDistance: Double = Double.MaxValue
      var j: Int = -1
      for (k <- 0 until r) {
        if (minDistance > vatImage(r)(k)) {
          minDistance = vatImage(r)(k)
          j = k
        }
      }
      ivatImage(r)(j) = vatImage(r)(j)

      var theC: Set[Int] = (0 until r).toSet
      theC = theC - j
      for (c <- theC) {
        ivatImage(r)(c) = scala.math.max(vatImage(r)(j), ivatImage(j)(c))
      }
    }

    for (r <- 1 until vatImage.length) {
      var theC: Set[Int] = (0 until r).toSet
      for (c <- theC) {
        ivatImage(c)(r) = ivatImage(r)(c)
      }
    }
    val t1 = System.nanoTime()
    duration = duration + t1 - t0

    if (vatLogMode) {
      saveObjectOrder(ivatObjectOrder, dataset.getFileCore())
    }
    if (vatPlotMode) {
      saveImage(ivatImage, dataset.getFileCore())
    }

    return dataset
  }
}
