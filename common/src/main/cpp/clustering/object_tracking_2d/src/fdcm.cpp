/*
Copyright 2011, Ming-Yu Liu

All Rights Reserved 

Permission to use, copy, modify, and distribute this software and 
its documentation for any non-commercial purpose is hereby granted 
without fee, provided that the above copyright notice appear in 
all copies and that both that copyright notice and this permission 
notice appear in supporting documentation, and that the name of 
the author not be used in advertising or publicity pertaining to 
distribution of the software without specific, written prior 
permission. 

THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, 
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
ANY PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR 
ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN 
AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING 
OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 
*/

//#include "stdafx.h"
//#include <cxcore.h>
//#include <cv.h>
//#include <highgui.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "Image/Image.h"
#include "Image/ImageIO.h"
#include "Fitline/LFLineFitter.h"
#include "Fdcm/LMLineMatcher.h"

#include <iostream>
#include <string>
#include <fstream>

void DrawDetWind(IplImage *image,int x,int y,int detWindWidth,int detWindHeight,CvScalar scalar,int thickness)
{
    cvLine(image,cvPoint( x,y),cvPoint( x+detWindWidth,y),scalar,thickness);
    cvLine(image,cvPoint( x+detWindWidth,y),cvPoint( x+detWindWidth, y+detWindHeight),scalar,thickness);
    cvLine(image,cvPoint( x+detWindWidth,y+detWindHeight),cvPoint( x, y+detWindHeight),scalar,thickness);
    cvLine(image,cvPoint( x, y+detWindHeight),cvPoint( x, y),scalar,thickness);
}


int main(int argc, char *argv[])
{

    if(!(argc == 4)){
        std::cerr<<"[Syntax] fdcm_work \
        input_realImage.png \
        output_image.png \
        output_segment_info.txt"<<std::endl;
        exit(0);
    }

    LFLineFitter lf;
    LMLineMatcher lm;
    lf.Configure("./src/main/cpp/clustering/config/para_line_fitter.txt");
    lm.Configure("./src/main/cpp/clustering/config/para_line_matcher.txt");


    //Image *inputImage=NULL;
    IplImage *inputImage=NULL;
    //Image<uchar> *inputImage=NULL;
    IplImage *edgeImage = NULL;

    string templateFileName("./src/main/cpp/clustering/config/avat_template.txt");
    string displayImageName(argv[1]);

    IplImage* img = cvLoadImage(displayImageName.c_str(),0);
    inputImage = cvCloneImage(img);
    //cvCanny(img, inputImage, 20, 40, 3);
    cvCanny(img, inputImage, 20, 80, 3);
    //cvCanny(img, inputImage, 80, 120, 3);
    cvReleaseImage(&img);

    edgeImage = cvCloneImage(inputImage);

    lf.Init();
    lm.Init(templateFileName.c_str());


    // Line Fitting
    lf.FitLine(inputImage);

    // FDCM Matching
    vector<LMDetWind> detWind;
    //lm.Match(lf, detWind);

    vector<vector<LMDetWind> > detWinds(lm.ndbImages_);
    vector<LMDetWind> detWindAll;
    double maxThreshold = 0.30;
    for(int i=0; i<lm.ndbImages_; i++)
    {
        std::cout << "[" << i << "]-th template ..." << std::endl;
        lm.SingleShapeDetectionWithVaryingQuerySize(lf, i, maxThreshold, detWinds[i]);
        for(size_t j=0; j<detWinds[i].size(); j++)
        {
            detWindAll.push_back(detWinds[i][j]);
        }
    }

    // Sort the window array in the ascending order of matching cost.
    LMDetWind *tmpWind = new LMDetWind[detWindAll.size()];
    for(size_t i=0;i<detWindAll.size();i++)
        tmpWind[i] = detWindAll[i];
    MMFunctions::Sort(tmpWind, detWindAll.size());
    for(size_t i=0;i<detWindAll.size();i++)
        detWind.push_back(tmpWind[i]);
    delete [] tmpWind;

    //create file to write infor segment
    std::ofstream info_segment(argv[3],std::ofstream::trunc);

    std::cout << detWind.size() << " detections..." << std::endl;
    info_segment << detWind.size() << "\n";

    
    IplImage* dispImage = cvLoadImage(displayImageName.c_str());

    for(size_t i=0; i<detWind.size(); i++)
    {
        std::cout << detWind[i].x_ << " " << detWind[i].y_ << " " << detWind[i].width_ << " " << detWind[i].height_ << " " << detWind[i].cost_ << " " << detWind[i].count_ << " " << detWind[i].scale_ << " " << detWind[i].aspect_ << " " << detWind[i].tidx_ << std::endl;
        info_segment << detWind[i].x_ << " " << detWind[i].y_ << " " << detWind[i].width_ << " " << detWind[i].height_ << "\n";
    }

    for(size_t i=1; i<(detWind.size()<10?detWind.size():10); i++)
        DrawDetWind(dispImage, detWind[i].x_, detWind[i].y_, detWind[i].width_, detWind[i].height_, cvScalar(255,255,0), i==1?2:1);

    if(detWind.size() > 0)
        DrawDetWind(dispImage, detWind[0].x_, detWind[0].y_, detWind[0].width_, detWind[0].height_, cvScalar(0,255,255), 2);

    //cvNamedWindow("edge", 1);
    //cvNamedWindow("output", 1);
    //cvShowImage("edge", edgeImage);
    //cvShowImage("output", dispImage);
    cvSaveImage(argv[2], dispImage);
    //cvWaitKey(0);

    //cvDestroyWindow("edge");
    //cvDestroyWindow("output");

    if(inputImage)  cvReleaseImage(&inputImage);
    if(dispImage)   cvReleaseImage(&dispImage);
    if(edgeImage)   cvReleaseImage(&edgeImage);

    //close file
    info_segment.close();
    
    return 0;
};
