package test

import configuration.BaseContext
import org.apache.spark.SparkContext
import org.apache.spark.sql.{Column, DataFrame, SQLContext}
import utils.{
  ConfigurationSearch,
  DataPoint,
  Dataset,
  DateUtils,
  MongoAction,
  MongoDBConnector,
  SystemUtils,
  Twitter,
  TwitterData
}
import configuration.YAMLConfig
import org.apache.spark.ml.feature.StopWordsRemover
import org.apache.spark.sql.expressions.{UserDefinedFunction, Window}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.LongType
import streaming.StreamingModel
import org.bson.Document
import org.apache.spark.mllib.feature.Stemmer

import scala.util.matching.Regex
import scala.util.control.Breaks._
import scala.collection.JavaConversions.collectionAsScalaIterable
import streaming.StreamingModel

class Simulation extends BaseContext with Serializable {
  protected var databaseConfig: java.util.LinkedHashMap[String, Any] = null
  protected var collectionTest: String = ""
  protected var collectionTestEtl: String = ""
  var fileCore: String = "tweet"

  protected var searchConfig: ConfigurationSearch = new ConfigurationSearch()
}

object KafkaProducerSimulation extends Simulation {
  // demarrer la Spark Session de processus
  val conf = buildSparkConf("Load Data Simulation 2")
  conf.set("spark.ui.port", "9050")
  conf.set("spark.executor.memory", "2g")
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")
  val sqlContext = new SQLContext(sc)
  val spark = sqlContext.sparkSession

  // configurer les parametres pour lire les donnees simulees
  val mongoConfig = YAMLConfig
    .get("mongodb_config")
    .asInstanceOf[java.util.LinkedHashMap[String, Any]]
  databaseConfig = mongoConfig
    .get("twitter")
    .asInstanceOf[java.util.LinkedHashMap[String, Any]]
  val mongo =
    YAMLConfig.get("mongodb").asInstanceOf[java.util.LinkedHashMap[String, Any]]
  val host = mongo.get("host").asInstanceOf[String]
  val database = mongo.get("database").asInstanceOf[String]
  collectionTest = mongo.get("collection_raw").asInstanceOf[String]
  collectionTestEtl = mongo.get("collection_etl").asInstanceOf[String]

  def main(args: Array[String]): Unit = {
    import spark.implicits._
    // lire les donnees comme les configurations ci-dessus
    SystemUtils.cleanDirectory()

    val project: Document = new Document()
    project.append("tweetID", 1)
    project.append("_id", 0)
    project.append("createdAt", 1)
    project.append("text", 1)
    project.append("user", 1)
    searchConfig.setProject(project)
    searchConfig.setCondition(null)

    var data: DataFrame = MongoDBConnector
      .readCollectionByClient(spark,
                              databaseConfig,
                              collectionTest,
                              searchConfig)
      .cache()
    data.show(2, truncate = false)

    val prepareDataset: Dataset = furtherETL(data, "TweetPoint", "test")
    // -----------------------------------------------------------------

    // quelques parametres necessaires pour simuler les temps reels processus
    val batchSize: Int = 1000
    var loopIndex: Int = 1
    val dataLeft: Dataset = prepareDataset
    StreamingModel.setBatchSize(400)
    breakable {
      while (true) {
//        if (dataLeft.length() <= 0) break
        if (loopIndex == 3) break
        val dataFragment: Dataset = new Dataset(
          dataLeft.getDataset().filter(x => x.index < batchSize * loopIndex))
        dataFragment.setTargetFile(fileCore)
        StreamingModel.setAlgorithm("corekmeans")
        StreamingModel.parseDataStreaming(dataFragment)

        dataLeft.setDataset(
          dataLeft.getDataset().filter(x => x.index >= batchSize * loopIndex))
        loopIndex += 1

        Thread.sleep(5 * 1000)
      }
    }
    Thread.sleep(5 * 1000)
    StreamingModel.closeConnection()

    sqlContext.clearCache()
    sc.stop()
  }

  val textProcessing: UserDefinedFunction = udf((text: String) => {
    // 0. lowercase
    var preprocessText = text.toLowerCase()

    //1. remove emails
    val simpleEmailRegex =
      "([^.@\\s]+)(\\.[^.@\\s]+)*@([^.@\\s]+\\.)+([^.@\\s]+)".r
    preprocessText = simpleEmailRegex.replaceAllIn(preprocessText, " ")

    //2. urls
    val complexURLRegex =
      """(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?""".r
    preprocessText = complexURLRegex.replaceAllIn(preprocessText, " ")

    //3. mentions and hashtag
    val simpleMentionAndHashtagRegex = """\s([@#][\w_-]+)""".r
    preprocessText =
      simpleMentionAndHashtagRegex.replaceAllIn(preprocessText, " ")

    //4. Removing non-alphabet characters (which are special character, emojis, punctuations, ...)
    val nonAlphabetRegex = "[^A-Za-z0-9 ]".r
    preprocessText = nonAlphabetRegex.replaceAllIn(preprocessText, "")
    preprocessText = preprocessText.replaceAll(" +", " ").trim

    //5. remove stop words and stemming
    // these will be process in spark due to available libraries
    preprocessText
  })

  val concatenateBagOfWord: UserDefinedFunction = udf(
    (bagWord: Seq[String]) => {
      var concatString: String = ""
      for (x <- bagWord.indices) {
        concatString += bagWord(x)
        if (x != bagWord.length - 1)
          concatString += " "
      }
      concatString
    })

  val normaliseDateTime: UserDefinedFunction = udf(
    (createdAt: String, fromFormat: String, toFormat: String) => {
      DateUtils.convertStrDateToFormat(createdAt, fromFormat, toFormat)
    })

  def furtherETL(data: DataFrame,
                 dataType: String,
                 mode: String = "test"): Dataset = {
    import spark.implicits._
    val considerAttribute: List[Column] = List("tweetID",
                                               "text",
                                               "createdAt",
                                               "user.id",
                                               "user.name",
                                               "user.friendsCount",
                                               "user.followersCount",
                                               "user.favouritesCount")
      .map(colName => col(colName))
    val finalConsiderAttribute: List[Column] = List("tweetID",
                                                    "content",
                                                    "tweetTime",
                                                    "timeStamp",
                                                    "userID",
                                                    "userName",
                                                    "friends",
                                                    "followers",
                                                    "favourites",
                                                    "ymd")
      .map(colName => col(colName))

    val inputTimeFormat: String = "MMM dd, yyyy h:mm:ss a"
    val outputTimeFormat: String = "yyyy-MM-dd HH:mm:ss"

    var storeData: DataFrame = data
      .select(considerAttribute: _*)
      .distinct()
      .withColumn("preContent", textProcessing(col("text")))
      .withColumn("contentWords", split(col("preContent"), " "))

    // remove stop word
    val stopWord = new StopWordsRemover()
      .setInputCol("contentWords")
      .setOutputCol("bagOfWord")
    storeData = stopWord
      .transform(storeData)
      .withColumn("content", concatenateBagOfWord(col("bagOfWord")))

    // stemming preprocessing
//    val stemming = new Stemmer().setInputCol("contentWordRemoved").setOutputCol("bagOfWord")
//      .setLanguage("English")
//    storeData = stemming.transform(storeData)

    storeData = storeData
      .withColumn("tweetTime",
                  normaliseDateTime(col("createdAt"),
                                    lit(inputTimeFormat),
                                    lit(outputTimeFormat)))
      .withColumn("timeStamp",
                  unix_timestamp(col("tweetTime"), outputTimeFormat))
      .withColumn("userID", col("id").cast(LongType))
      .withColumn("userName", col("name"))
      .withColumn("friends", col("friendsCount").cast(LongType))
      .withColumn("followers", col("followersCount").cast(LongType))
      .withColumn("favourites", col("favouritesCount").cast(LongType))
      .withColumn("ymd", substring(col("tweetTime"), 0, 10))
      .select(finalConsiderAttribute: _*)

    val newOrderColumn: List[Column] =
      storeData.columns.sorted.toList.map(colName => col(colName))
    val finalData: DataFrame = storeData.select(newOrderColumn: _*).cache()
    finalData.show(2, truncate = false)

    if (mode.equals("real")) {
      MongoAction.deleteOldData(mongo, finalData, collectionTestEtl)
      MongoAction.saveData(finalData, host, database, collectionTestEtl)
    }

    // ce temps la, transferer les donnees Spark a la forme de classe TwitterData
    // puis, on les utilise dans la processus d'echantillonnage Coreset
    val dataList: List[TwitterData] = collectionAsScalaIterable(
      finalData.as[TwitterData].collectAsList()).toList
    var dataPointList: Array[DataPoint] = Array[DataPoint]()
    for (x <- dataList.indices) {
      val newDataPoint: DataPoint = new DataPoint()
      newDataPoint.index = x
      newDataPoint.tweet = new Twitter(dataList(x))
      dataPointList :+= newDataPoint
    }

    val prepareDataset: Dataset = new Dataset()
    prepareDataset.setDataset(dataPointList)

    return prepareDataset
  }
}
