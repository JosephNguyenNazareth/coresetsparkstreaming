package utils

import java.io.{File, PrintWriter, FileOutputStream}

import scala.io.Source
import sys.process._
import scala.collection.mutable.Map
import scala.collection.mutable.ArrayBuffer
import org.jfree.chart.{ChartFactory, ChartUtils}
import org.jfree.data.xy.{XYSeries, XYSeriesCollection}

import scala.util.control.Breaks._
import com.redis._
import configuration.YAMLConfig
//dataset reader implementation
class Dataset(f: String = null) {
  /*
    A class to store data
   */
  private var prefix = "./datasets/"
  private var dataset: Array[DataPoint] = Array[DataPoint]()
  var fileCore: String = f
  private var labelColumn: Int = 1000;

  def setLabelColumn(labelCol: Int): Dataset = {
    labelColumn = labelCol
    return this
  }
  def length(): Int = {
    return dataset.length
  }

  def setPrefix(p: String): Dataset = {
    prefix = p
    return this
  }

  def setTargetFile(f: String): Dataset = {
    fileCore = f
    return this
  }

  def getFileCore(): String = {
    return fileCore
  }

  private def _readFile(delimiter: String = " "): Array[DataPoint] = {
    var fileDataset: Array[DataPoint] = Array[DataPoint]()
    var fileName: String = prefix + fileCore + ".txt"
    if (!java.nio.file.Files
          .exists(java.nio.file.Paths.get(fileName))) { // what for ?
      //csv
      fileName = prefix + fileCore + ".csv"
      val lines: Iterator[String] = Source.fromFile(fileName).getLines()
      for (line <- lines) {
        fileDataset :+= new DataPoint(line)
      }
    } else {
      val lines: Iterator[String] = Source.fromFile(fileName).getLines()
      var ds: Array[Array[Double]] = lines
        .map(
          _.trim
            .replaceAll(delimiter + "+", delimiter)
            .split(delimiter + "+")
            .map(_.trim.toDouble))
        .toArray
      var order: Int = 0
      for (p <- ds) {
        fileDataset :+= new DataPoint(order, coord = p)
        order = order + 1
      }
    }
    return fileDataset
  }

  private def _readFileStreaming(
      delimiter: String = " ",
      extension: String = ".txt",
      hasHeader: Boolean = false): Array[Array[Double]] = {
    var fileDataset: Array[DataPoint] = Array[DataPoint]()
    var ds: Array[Array[Double]] = Array[Array[Double]]()
    var fileName: String = prefix + fileCore + extension
    if (!java.nio.file.Files
          .exists(java.nio.file.Paths.get(fileName))) { // what for ?
      //csv
      fileName = prefix + fileCore + extension
      val buffer = Source.fromFile(fileName)
      val lines: Iterator[String] = buffer.getLines()
      for (line <- lines) {
        fileDataset :+= new DataPoint(line)
      }
      buffer.close
    } else {
      val buffer = Source.fromFile(fileName)
      var lines: Iterator[String] = buffer.getLines()
      if (hasHeader)
        lines.next
      ds = lines
        .map(
          _.trim
            .replaceAll(delimiter + "+", delimiter)
            .split(delimiter + "+")
            .map(_.trim.toDouble))
        .toArray
      if (labelColumn != 1000) {
        if (labelColumn < 0) {
          labelColumn = -labelColumn
        } else {
          labelColumn = ds(0).length - labelColumn
        }
        ds = ds.map(x => x.dropRight(labelColumn))
      }
      buffer.close
    }
    return ds
  }

  private def _readFileStreaming2(
      delimiter: String = " ",
      col_symbol: Array[Int]): Array[Array[Double]] = {
    var ds: Array[Array[Double]] = Array[Array[Double]]()
    val fileName: String = prefix + fileCore + ".txt"
    val core = new PrintWriter(
      new FileOutputStream(
        new File("./datasets/streamkm/" + fileCore + "_new.txt"),
        true))
//    val line : String = ""
    val buffer = Source.fromFile(fileName)
    var order: Int = 0
    for (line <- buffer.getLines()) {
      val tmp: Array[String] = line.trim
        .replaceAll(delimiter + "+", delimiter)
        .split(delimiter + "+")
      var real_line: Array[Double] = Array[Double]()
      for (i <- tmp.indices) {
        if (!col_symbol.contains(i)) {
          real_line :+= tmp(i).trim.toDouble
        }
      }
      core.append(real_line.map(_.toString).mkString(",") + "\n")
//      ds :+= real_line
    }
    core.close()

    if (labelColumn != 1000) {
      if (labelColumn < 0) {
        labelColumn = -labelColumn
      } else {
        labelColumn = ds(0).length - labelColumn
      }
      ds = ds.map(x => x.dropRight(labelColumn))
    }
    buffer.close
    return ds
  }

  private def _readPreExistingClusterFile(): Array[DataPoint] = {
    var fileDataset: Array[DataPoint] = Array[DataPoint]()
    var fileName: String = prefix + fileCore + ".txt"
    if (!java.nio.file.Files.exists(java.nio.file.Paths.get(fileName))) {
      //csv
      fileName = prefix + fileCore + ".csv"
      val lines: Iterator[String] = Source.fromFile(fileName).getLines()
      for (line <- lines) {
        fileDataset :+= new DataPoint(line)
      }
    } else {
      val lines: Iterator[String] = Source.fromFile(fileName).getLines()
      var ds: Array[Array[Double]] = lines
        .map(
          _.trim
            .replaceAll(" +", " ")
            .split(" +")
            .map(_.trim.toDouble))
        .toArray
      //Last value is the cluster -> extract it
      var order: Int = 0
      for (p <- ds) {
        val clusterVal = p(p.size - 1).toInt
        var newP = p.dropRight(1)
        fileDataset :+= new DataPoint(order, coord = newP, cluster = clusterVal)
        order = order + 1
      }
    }
    return fileDataset
  }

  def readFile(delimiter: String = " "): Dataset = {
    println("Reading data...")
    dataset = _readFile(delimiter)
    println("Reading finished.")
    return this
  }

  def readFileStreaming(delimiter: String = " ",
                        extension: String = ".txt",
                        hasHeader: Boolean = false): Array[Array[Double]] = {
    println("Reading data streaming...")
    var ds: Array[Array[Double]] =
      _readFileStreaming(delimiter, extension, hasHeader)
    println("Reading finished halfway...")
    return ds
  }

  def readPreExistingClusterFile(): Dataset = {
    println("Reading pre-existing cluster file...")
    dataset = _readPreExistingClusterFile()
    println("Reading finished")
    return this
  }

  override def clone: Dataset = {
    var newDataset: Dataset = new Dataset()
    newDataset.setTargetFile(fileCore)
    //copy data point
    for (dp <- dataset) {
      newDataset.dataset :+= new DataPoint(dp)
    }
    return newDataset
  }

  def this(newDataset: Array[DataPoint]) {
    this()
    dataset = newDataset
  }

  def getDataset(): Array[DataPoint] = {
    return dataset
  }

  def setDataset(newDataset: Array[DataPoint]): Unit = {
    dataset = newDataset
  }

  def apply(i: Int) = dataset(i)

  def :+=(that: DataPoint) {
    dataset :+= that
  }

  def ++=(that: Dataset) {
    dataset ++= that.getDataset()
  }

  def intersect(that: Dataset): Dataset = {
    val newDataset = this.clone
    newDataset.dataset = dataset.intersect(that.getDataset())

    return newDataset
  }

  def diff(that: Dataset): Dataset = {
    val newDataset = this.clone
    newDataset.dataset = dataset.diff(that.getDataset())

    return newDataset
  }

  def generateFromHAWK(
      targetSilhouette: Double = (new scala.util.Random nextDouble),
      exampleNum: Int = 1000 + (new scala.util.Random nextInt 5000),
      clusterNum: Int = 1 + (new scala.util.Random nextInt 10),
      equalSize: String =
        if ((new scala.util.Random nextInt 2) == 1) "True" else "False",
      overlap: Double = (new scala.util.Random nextDouble),
      limit: String =
        if ((new scala.util.Random nextInt 2) == 1) "upper" else "lower"
  ): Dataset = {
    fileCore =
      s"hawk_ts_${targetSilhouette}_en_${exampleNum}_cn_${clusterNum}_es_${equalSize}_ol_${overlap}_lm_${limit}"

    "python3 ./src/main/python/dataset_generator/hawks.py" + " " + targetSilhouette.toString +
      " " + exampleNum.toString + " " + clusterNum.toString + " " + equalSize + " " +
      overlap.toString + " " + limit + " " + fileCore !

    return readFile()

  }

  def readTextFromFolder(): Dataset = {
    val file = new java.io.File(prefix)

    var textFiles: List[String] = file.listFiles
      .filter(_.isFile)
      .filter(_.getName.endsWith(".txt"))
      .map(_.getPath)
      .toList
      .sorted

    //for each text file, read content and save to text(
    var tempDataset: Array[DataPoint] = Array[DataPoint]()
    var i = 0
    for (f <- textFiles) {
      val t = scala.io.Source.fromFile(f)
      val status = t.mkString
      t.close()
      var p = new DataPoint()
      p.index = i
      p.tweet.content = status
      tempDataset :+= p
      i = i + 1
    }
    dataset = tempDataset

    return this
  }

  def saveScatterChart(path: String, plotName: String): Dataset = {

    if (this(0).coord.size >= 3) {
      println("Warning: Unoptimized 3d mode")
      println("Please open clusteredScatterPlot3D.py")
      val redisConfig =
        YAMLConfig
          .get("redis")
          .asInstanceOf[java.util.LinkedHashMap[String, Any]]
      val redis3D = redisConfig
        .get("redis_3d_visual")
        .asInstanceOf[java.util.LinkedHashMap[String, Any]]
      val host = redis3D.get("host").asInstanceOf[String]
      val port = redis3D.get("port").asInstanceOf[Int]
      val db = redis3D.get("database").asInstanceOf[Int]
      val re = new RedisClient(host, port, db)
      var pointList = ""
      if (this(0).coord.size == 3) {
        //calculating list of x, y and z
        var mapper = Map[String, Array[Double]]()
        var totalCluster = 0
        for (point <- dataset) {
          if (!(mapper isDefinedAt s"x_${point.cluster}")) {
            totalCluster += 1
            mapper += (s"x_${point.cluster}" -> Array[Double]())
          }
          if (!(mapper isDefinedAt s"y_${point.cluster}")) {
            mapper += (s"y_${point.cluster}" -> Array[Double]())
          }
          if (!(mapper isDefinedAt s"z_${point.cluster}")) {
            mapper += (s"z_${point.cluster}" -> Array[Double]())
          }
          mapper(s"x_${point.cluster}") :+= point.coord(0)
          mapper(s"y_${point.cluster}") :+= point.coord(1)
          mapper(s"z_${point.cluster}") :+= point.coord(2)
        }
        pointList = s""""cluster" : ${totalCluster},"""
        for ((k, v) <- mapper) {
          pointList += s""""${k}" : [${v.mkString(",")}],"""
        }
      } else {
        var dataList = Array[String]()
        var totalCluster = 0
        for (point <- dataset) {
          //add index , point and cluster
          var p = point.coord.mkString(",")
          dataList :+= s"""[${point.index},[${p}],${point.cluster}]"""
        }
        pointList = s""""data" : [${dataList.mkString(",")}],"""
      }
      var request = s"""{
        "num_dim" : ${this(0).coord.size},
        ${pointList}
        "message" : "clustered"
      }"""
      re.set("cluster_scatterplot_3d_request", request)
      return this
    }
    if (this(0).userId != "" || this(0).tweet.content != "") {
      println(
        "Cannot plot scatter chart using Dataset containing social user information")
      return this
    }
    var clusterSeries: Array[XYSeries] = Array[XYSeries]()
    var plotData = new XYSeriesCollection
    var s: Int = 0
    for (dp <- this.getDataset()) {
      if (dp.cluster != -2 && dp.cluster != -1) {
        if (dp.cluster + 1 >= clusterSeries.length) {
          for (t <- 0 to dp.cluster - clusterSeries.length) {
            clusterSeries :+= new XYSeries(s"Cluster $s")
            s = s + 1
          }
        }
        clusterSeries(dp.cluster).add(dp.coord(0), dp.coord(1))
      }
    }
    for (x <- clusterSeries) {
      plotData.addSeries(x)
    }

    var scatterChart =
      ChartFactory.createScatterPlot(plotName, "X-Axis", "Y-Axis", plotData)
    ChartUtils.saveChartAsPNG(new java.io.File(path), scatterChart, 1024, 729)

    return this
  }

  def writeToFile(path: String): Dataset = {
    val core = new java.io.PrintWriter(new java.io.File(path))
    core.write(this.getDataset().map(dp => dp.toString).mkString("\n"))
    core.close

    return this
  }
}
