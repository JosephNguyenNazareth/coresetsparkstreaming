package test

import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.WriteConfig
import configuration.{BaseContext, YAMLConfig}
import org.apache.spark.SparkContext
import org.apache.spark.sql.functions.{col, lit, substring, udf, when}
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.{DataFrame, SQLContext}
import utils.{DateUtils, MongoAction}

object SaveJsonToMongo extends BaseContext with Serializable {
  val mongo =
    YAMLConfig.get("mongodb").asInstanceOf[java.util.LinkedHashMap[String, Any]]
  val host = mongo.get("host").asInstanceOf[String]
  val database = mongo.get("database").asInstanceOf[String]
  val collectionRaw = mongo.get("collection_raw").asInstanceOf[String]

  val normaliseDateTime = udf(
    (createdAt: String, fromFormat: String, toFormat: String) => {
      DateUtils.convertStrDateToFormat(createdAt, fromFormat, toFormat)
    })

  def main(args: Array[String]): Unit = {
    val conf = buildSparkConf("Save Data Simulation")
    conf.set("spark.ui.port", "9050")
    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")
    val sqlContext = new SQLContext(sc)
    val spark = sqlContext.sparkSession

    val locate = "./twitter_data"

    val log = sqlContext.read.json(sc.textFile(locate)).cache()
    log.show(2, false)

    val inputTimeFormat: String = "MMM dd, yyyy h:mm:ss a"
    val outputTimeFormat: String = "yyyy-MM-dd HH:mm:ss"
    val currentTime = DateUtils.getCurrentDateTime(inputTimeFormat)
    val dataRaw: DataFrame = log
      .withColumn("createdAt",
                  when(col("createdAt").isNotNull, col("createdAt")).otherwise(
                    lit(currentTime)))
      .withColumn("tweetTime",
                  normaliseDateTime(col("createdAt"),
                                    lit(inputTimeFormat),
                                    lit(outputTimeFormat)))
      .withColumn("ymd", substring(col("tweetTime"), 0, 10))
      .filter(col("user.id").isNotNull)
      .withColumn("tweetID", col("id").cast(LongType))
      .drop("id", "tweetTime")
    dataRaw.show(2, false)
    MongoAction.saveData(dataRaw, host, database, collectionRaw)

    sc.stop()
  }

  def saveData(storeDate: DataFrame, collection: String): Unit = {
    println("===== Start saving to MongoDB =====")
    val urlDatabase = "mongodb://" + host + "/" + database + "." + collection
    val writeConfig = WriteConfig(Map("uri" -> (urlDatabase)))
    MongoSpark.save(storeDate.write.mode("append"), writeConfig)
    println("===== Save to " + urlDatabase + " successfully =====")
  }
}
