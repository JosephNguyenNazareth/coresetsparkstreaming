package entry

import clustering.{CVCModel, KMEANS}
import entry.MainCVC._
import metrics.{CDbwIndex, DaviesBouldinIndex, DunnIndex, SilhouetteIndex}
import sampling.ProTraS
import utils._

import scala.math.pow

/*
These are the following arguments that need to be passed:
    --filecore string (name of file)

    --prefix_dir string (prefix of directory)

    --clean_cache boolean (true, false)

    --min_pts int (1 or above)

    --sigma float  (1.0 and above)

    --epsilon float (0 to sqrt(2))

    --protras_epsilon (0 to 1)

    --enable_plot (true,false)

    --enable_log (true,false)
 */

object MainKMEANSWithProTraS extends App {
  /*
    1. Parsing arguments
   */
  //Default arguments
  var fileCore = Array[String]("hawk_5_0.47")
  var prefixDir = "./datasets/"
  var cleanCache = false
  var minPts = 2
  var epsilon = 0.22
  var protrasEpsilon = 0.001
  var enableLog = true
  var enablePlot = true

  args.sliding(2, 2).toList.collect {
    case Array("--filecore", x: String)        => fileCore :+= x
    case Array("--prefix_dir", x: String)      => prefixDir = x
    case Array("--clean_cache", x: String)     => cleanCache = x.toBoolean
    case Array("--min_pts", x: String)         => minPts = x.toInt
    case Array("--epsilon", x: String)         => epsilon = x.toDouble
    case Array("--protras_epsilon", x: String) => protrasEpsilon = x.toDouble
    case Array("--enable_plot", x: String)     => enablePlot = x.toBoolean
    case Array("--enable_log", x: String)      => enableLog = x.toBoolean
  }

  /*
    2. Check clean cache
   */
  if (cleanCache) {
    SystemUtils.cleanDirectory()
  }

  if (fileCore.length != 0) {

    //declare pipeline
    var pipeline: Pipeline = new Pipeline()

    //normalization for dataset
    var normaliser: minMaxNormalise = new minMaxNormalise()
    pipeline.addOperator(normaliser)

    //sampling to prevent computer exhaust
    var samplingAlgo = new ProTraS()
      .setEpsilon(protrasEpsilon)
      .enableLog(enableLog)
      .enablePlot(enablePlot)
      .setCalMode("memory-based")

    var clusterDetection = new CVCModel()
      .setEpsilon(epsilon)
      .setPercentage(percentage)
      .setAVATMode(avatMode)
      .setRatio(ratio)
      .setThresholdValueForIDSA(thresholdLevel)
      .setPopulationForIDSA(population)
      .setNumGenerationForIDSA(numGen)
      .setMode(mode)
      .setReturnMode("origin")
      .setCalMode("memory-based")
      .enableLog(enableLog)
      .enablePlot(enablePlot)
      .setSamplingMode("improved")
      .setApplyKmeans(true)

    pipeline.addOperator(clusterDetection)

    for (file <- fileCore) {
      //Initialize dataset
      var dataset: Dataset = new Dataset()
        .setPrefix(prefixDir)
        .setTargetFile(file)
        .readFile()

      //fit dataset into pipeline
      pipeline.fit(dataset)

      //transform
      pipeline.transform()

      val result = pipeline.getOutput()
      var clusterSet: Set[Int] = Set[Int]()
      for (point <- result.getDataset())
        clusterSet += point.cluster

      println(clusterSet.size)

      val clusterAlgo = new KMEANS()
        .setK(clusterSet.size)
        .enableLog(true)
        .enablePlot(true)
        .setCalMode("memory-based")

      val secondPipeline: Pipeline = new Pipeline()
      secondPipeline.addOperator(clusterAlgo)
      secondPipeline.fit(result)
      secondPipeline.transform()

      //perform generalization
      val t0 = System.nanoTime() //begin algorithm
      var samplingDataset = secondPipeline.getOutput()
      for (t <- 0 until samplingDataset.length) {
        if (samplingDataset(t).cluster == -1) {
          var minDistance: Double = Double.MaxValue
          var cIdx: Int = -1
          for (targetDp <- samplingDataset.getDataset()) {
            if (targetDp.index != samplingDataset(t).index && targetDp.cluster != -1) { //not the point itself and must be the clustered coreset point
              var dis: Double = Measure.euclide(targetDp, samplingDataset(t))
              if (dis < minDistance) {
                minDistance = dis
                cIdx = targetDp.index
              }
            }
          }
          dataset(samplingDataset(t).index).cluster = dataset(cIdx).cluster
          samplingDataset(t).cluster = dataset(cIdx).cluster
        }
      }

      //for other point
      for (x <- 0 until dataset.length) {
        dataset(x).cluster = dataset(dataset(x).rep).cluster
      }
      val t1 = System.nanoTime() //end algorithm

      //declaring metrics for evaluation
      var p: Pipeline = new Pipeline()
      var daviesBouldinIndex = new DaviesBouldinIndex()
      var dunnIndex = new DunnIndex()
      var cdbwIndex = new CDbwIndex()
      var silhouetteIndex = new SilhouetteIndex()
      p.addOperator(daviesBouldinIndex)
      p.addOperator(dunnIndex)
      p.addOperator(cdbwIndex)
      p.addOperator(silhouetteIndex)
      p.fit(dataset)
      p.transform()

      dataset.saveScatterChart(
        s"./result/kmeans/image/${dataset.getFileCore()}.png",
        "dbscan")
      dataset.writeToFile(s"./result/kmeans/data/${dataset.getFileCore()}.csv")

      println(
        "\n########################################## RESULT FINISHED ##########################################")
      println(s"Dataset name: ${dataset.getFileCore()}")
      println(s"Origin: ${dataset.length}")
      println(s"DaviesBouldinIndex : ${daviesBouldinIndex.getValue()}")
      println(s"DunnIndex : ${dunnIndex.getValue()}")
      println(s"CDbwIndex : ${cdbwIndex.getValue()}")
      println(s"SilhouetteIndex : ${silhouetteIndex.getValue()}")
      println(s"Time elapsed (without file logging) in seconds: ${(pipeline
        .timeElapsed() + t1 - t0 + p.timeElapsed()).toDouble / pow(10, 9)} seconds")
      println(
        "#####################################################################################################\n")
    }
  } else {
    println("Warning: No file cores specified")
  }
}
