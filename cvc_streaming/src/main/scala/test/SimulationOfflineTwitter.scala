package test

import utils.{DataPoint, Dataset, Pipeline, SystemUtils, Twitter, TwitterData}
import java.io.{BufferedWriter, File, FileWriter}

import org.apache.spark.SparkContext
import org.apache.spark.ml.feature.StopWordsRemover
import org.apache.spark.sql.functions.{col, split, udf}
import org.apache.spark.sql.{Column, DataFrame, SQLContext}

import scala.util.control.Breaks._
import streaming.StreamingModel
import configuration.BaseContext
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{row_number, lit}

import scala.collection.JavaConversions.collectionAsScalaIterable
import scala.math.pow
import utils.DataFile

object SimulationOfflineTwitter extends BaseContext with Serializable {
  val TRACE: Boolean = true
  // demarrer la Spark Session de processus
  val conf = buildSparkConf("Simulation Streaming Twitter")
  conf.set("spark.ui.port", "9050")
  conf.set("spark.executor.memory", "3g")
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")
  val sqlContext = new SQLContext(sc)
  val spark = sqlContext.sparkSession

  def main(args: Array[String]): Unit = {
    // Default arguments
    var datafile: Array[DataFile] = Array[DataFile]();
    datafile :+= new DataFile("train", -6, 2000, 200, 0.95, ".csv")

    var prefixDir = "./datasets/spam_twitter/"
    var cleanCache = false

    if (cleanCache) {
      SystemUtils.cleanDirectory()
    }
    var counter: Int = 0
    if (datafile.length != 0) {
      for (i <- datafile.indices) {
        val file: String = datafile(i).fileCore
        println(
          "======= Streaming Coreset Clustering on dataset " + file + " =======")
        //Initialize dataset
        var row: Int = 0;
        val batchSize: Int = datafile(i).batchSize
        StreamingModel.setAlgorithm("corekm_tweet")
        StreamingModel.setBatchSize(datafile(i).streamingBatchSize)
        StreamingModel.setEpsilon(datafile(i).epsilon)
        StreamingModel.setConstant(2)
        println(prefixDir + file + datafile(i).extension)
        var data: DataFrame = spark.read
          .format("csv")
          .option("header", value = true)
          .option("delimiter", value = ",")
          .load(prefixDir + file + datafile(i).extension)

        data = data
          .withColumn("rank",
                      row_number().over(Window.partitionBy().orderBy("Tweet")))
          .cache()
        data.select("Tweet", "rank").show(20, truncate = false)
        val t1 = System.nanoTime()

        var loopIndex: Int = 1
        breakable {
          while (true) {
            val prepareDataset: DataFrame = data.filter(col("rank") >= lit(
              batchSize * (loopIndex - 1)) && col("rank") < lit(
              batchSize * (loopIndex)))
            val dataLeft: Dataset =
              furtherETL(prepareDataset, "TweetPoint", "test")
            if (dataLeft.length() <= 0) break

            dataLeft.setTargetFile(file)
            println("Get in")
            StreamingModel.parseDataStreaming(dataLeft)
            row = row + batchSize
            loopIndex = loopIndex + 1
            println("Processed line " + row)
          }
        }
        var clusterData: Dataset = new Dataset()
        if (TRACE) {
//          val sampleData: Dataset = StreamingModel.getOutput()
          StreamingModel.clustering()
          clusterData = StreamingModel.getOutput()
          StreamingModel.writeToFileStreaming(clusterData)
//          StreamingModel.saveScatterChart(clusterData)

          val t2 = System.nanoTime()
          val duration: Double = (t2 - t1) / 1000000000.0
          StreamingModel.closeConnection()
          sc.stop()
        }
      }
    } else {
      println("Warning: No file cores specified")
    }
  }

  def furtherETL(data: DataFrame,
                 dataType: String,
                 mode: String = "test"): Dataset = {
    import spark.implicits._
    val considerAttribute: List[Column] = List("Tweet",
                                               "following",
                                               "followers",
                                               "actions",
                                               "is_retweet",
                                               "location",
                                               "Type")
      .map(colName => col(colName))
    val finalConsiderAttribute: List[Column] =
      List("content").map(colName => col(colName))

    val inputTimeFormat: String = "MMM dd, yyyy h:mm:ss a"
    val outputTimeFormat: String = "yyyy-MM-dd HH:mm:ss"

    var storeData: DataFrame = data
      .select(considerAttribute: _*)
      .distinct()
      .withColumn("preContent", textProcessing(col("Tweet")))
      .withColumn("contentWords", split(col("preContent"), " "))

    // remove stop word
    val stopWord = new StopWordsRemover()
      .setInputCol("contentWords")
      .setOutputCol("bagOfWord")
    storeData = stopWord
      .transform(storeData)
      .withColumn("content", concatenateBagOfWord(col("bagOfWord")))

    // stemming prepconcatenateBagOfWord,processing
    // val stemming = new Stemmer()
    //   .setInputCol("contentWordRemoved")
    //   .setOutputCol("bagOfWord")
    //   .setLanguage("English")
    // storeData = stemming.transform(storeData)

    storeData = storeData.select(finalConsiderAttribute: _*)

    val newOrderColumn: List[Column] =
      storeData.columns.sorted.toList.map(colName => col(colName))
    val finalData: DataFrame = storeData.select(newOrderColumn: _*)
    finalData.show(2, truncate = false)

    // ce temps la, transferer les donnees Spark a la forme de classe TwitterData
    // puis, on les utilise dans la processus d'echantillonnage Coreset
    val dataList: List[String] = collectionAsScalaIterable(
      finalData.as[String].collectAsList()).toList

    var dataPointList: Array[DataPoint] = Array[DataPoint]()
    for (x <- dataList.indices) {
      val newDataPoint: DataPoint = new DataPoint()
      newDataPoint.index = x
      newDataPoint.tweet = new Twitter(content = dataList(x))
      dataPointList :+= newDataPoint
    }

    val prepareDataset: Dataset = new Dataset()
    prepareDataset.setDataset(dataPointList)

    return prepareDataset
  }

  val textProcessing: UserDefinedFunction = udf((text: String) => {
    // 0. lowercase
    var preprocessText = text.toLowerCase()

    //1. remove emails
    val simpleEmailRegex =
      "([^.@\\s]+)(\\.[^.@\\s]+)*@([^.@\\s]+\\.)+([^.@\\s]+)".r
    preprocessText = simpleEmailRegex.replaceAllIn(preprocessText, " ")

    //2. urls
    val complexURLRegex =
      """(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?""".r
    preprocessText = complexURLRegex.replaceAllIn(preprocessText, " ")

    //3. mentions and hashtag
    val simpleMentionAndHashtagRegex = """\s([@#][\w_-]+)""".r
    preprocessText =
      simpleMentionAndHashtagRegex.replaceAllIn(preprocessText, " ")

    //4. Removing non-alphabet characters (which are special character, emojis, punctuations, ...)
    val nonAlphabetRegex = "[^A-Za-z0-9 ]".r
    preprocessText = nonAlphabetRegex.replaceAllIn(preprocessText, "")
    preprocessText = preprocessText.replaceAll(" +", " ").trim

    //5. remove stop words and stemming
    // these will be process in spark due to available libraries
    preprocessText
  })

  val concatenateBagOfWord: UserDefinedFunction = udf(
    (bagWord: Seq[String]) => {
      var concatString: String = ""
      for (x <- bagWord.indices) {
        concatString += bagWord(x)
        if (x != bagWord.length - 1)
          concatString += " "
      }
      concatString
    })
}
