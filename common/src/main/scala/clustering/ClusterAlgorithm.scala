package clustering

import scala.io.Source
import scala.reflect.io.File
import scala.collection.mutable.ArrayBuffer
import scala.math.Ordering

import Double._
import math._
import util.control.Breaks._
import sys.process._

import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartPanel
import org.jfree.chart.ChartUtils
import org.jfree.chart.JFreeChart
import org.jfree.chart.plot.XYPlot
import org.jfree.data.xy.XYDataset
import org.jfree.data.xy.XYSeries
import org.jfree.data.xy.XYSeriesCollection

import javax.swing.JFrame
import javax.swing.JPanel
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.lang.Thread
import java.awt.Graphics2D
import java.awt.image.BufferedImage

import utils.{DataPoint, Dataset, PipelineOperator, Pipeline, Measure}

//pipeline operator used in finding cluster for the dataset
abstract class ClusterAlgorithm[T <: ClusterAlgorithm[T]]
    extends PipelineOperator {
  protected var distanceFunction =
    (firstPoint: DataPoint, secondPoint: DataPoint) => {
      Measure.euclide(firstPoint, secondPoint)
    }
  protected var numClus: Array[Int] = Array[Int]()
  protected var plotEnable: Boolean = false //check whether user wants to plot
  protected var logEnable
    : Boolean = false //check whether user wants to do logging
  private var distanceMap = Map[Set[Int], Double]()
  protected var calMode: String = "ram-based" //"ram-based, memory-based"
  protected var tmd: Option[Array[Array[Double]]] = None
  protected var n: Int = 0
  //state mode: save distance
  //instant: calculate from raw

  def getEstimated(): Array[Int] = {
    return numClus
  }

  def setDistanceFunction(df: (DataPoint, DataPoint) => Double): T = {
    distanceFunction = df
    return this.asInstanceOf[T]
  }

  def setCalMode(mode: String): T = {
    if (calMode != "memory-based" && calMode != "ram-based") {
      println("Unknown calculation mode: Choosing default")
      calMode = "ram-based"
    }
    calMode = mode
    return this.asInstanceOf[T]
  }

  protected def getDistance(dataset: Dataset, d1: Int, d2: Int): Double = {
    if (calMode == "memory-based") {
      var first = -1
      var second = -1
      if (d1 <= d2) {
        first = d1
        second = d2 - d1
      } else {
        first = d2
        second = d1 - d2
      }
      if (tmd.get(first)(second) < 0.0) {
        var distance = distanceFunction(dataset(d1), dataset(d2))
        tmd.get(first)(second) = distance
      }
      return tmd.get(first)(second)
    } else {
      return distanceFunction(dataset(d1), dataset(d2))
    }
  }

  def enablePlot(e: Boolean): T = {
    plotEnable = e
    return this.asInstanceOf[T]
  }

  def enableLog(e: Boolean): T = {
    logEnable = e
    return this.asInstanceOf[T]
  }

  def _fit(dataset: Dataset): Dataset = {
    if (calMode == "memory-based") {
      //initialize upper triangular matrix
      n = dataset.length
      tmd = Some(new Array[Array[Double]](n))
      for (i <- 0 to n - 1) {
        tmd.get(i) = Array.fill[Double](n - i)(-1.0)
        tmd.get(i)(0) = 0.0
      }
    }
    return fit(dataset)
  }

  def fit(dataset: Dataset): Dataset

  def saveScatterChart(dataset: Dataset) {
    //check dimension of dataset
    val path: String = "./result/" + this.toString + "/image/" + dataset
      .getFileCore() + ".png"
    val plotName = this.toString
    dataset.saveScatterChart(path, plotName)
  }

  def writeToFile(dataset: Dataset) {
    val path: String = "./result/" + this.toString + "/data/" + dataset
      .getFileCore() + ".csv"
    dataset.writeToFile(path)
  }

  //abstract implementation
  def toString: String

  override def process(input: Dataset): Dataset = {
    //set timer to zero
    duration = 0
    var newInput = _fit(input)
    return newInput
  }
}
