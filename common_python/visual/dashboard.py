import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import plotly.graph_objects as go

from pandas.io.json import json_normalize
from math import sqrt

import sys

sys.path.insert(0, "./")
from common_python.visual.receiver import DataReceiver
import argparse

# Arguments for redis database containing visual information


parser = argparse.ArgumentParser(description='Argument parser for Visual process')
parser.add_argument('--host', default='localhost', type=str,
                    help='host for query connection')
parser.add_argument('--port', default=6379, type=int,
                    help='port for query connection')
parser.add_argument('--db', default=1, type=int,
                    help='database index')
recv = None

# ------------- 0. Initialize variable to calculate data --------------------
# ------------- Overall ------------------
# data of last batch
data_previous = {
    'clusterID': [],
    'clusterParent': [],
    'noTweet': [],
    'tpu': []
}
df_previous = pd.DataFrame(data_previous)

# data of current batch    
data = {
    'clusterID': [],
    'clusterParent': [],
    'noTweet': [],
    'tpu': []
}
df_current = pd.DataFrame(data)

# ----------------- Sunburst --------------------------
# ----------------- Table ------------------------------


# ----------------- Histogram --------------------------
data_histogram = {
    'clusterID': [],
    'user': [],
    'friends': [],
    'followers': [],
    'favourites': []
}
df_histogram = pd.DataFrame(data_histogram)

# ----------------- Word Chart ------------------------
data_word = {
    "word": [],
    "prob": []
}
df_word = pd.DataFrame(data_word)


def community_creation():
    global df_previous, df_current
    print("nah, i'm still running")
    print(df_current)

    current_variance = pow(df_current["noTweet"] - df_current["noTweet"].mean(), 2).sum() / df_current["noTweet"].count()
    previous_variance = pow(df_current["noTweet"] - df_current["noTweet"].mean(), 2).sum() / df_current["noTweet"].count()

    fig2 = go.Figure()

    fig2.add_trace(go.Indicator(
        mode="number+delta",
        title={"text": "Communities"},
        value=df_current['clusterID'].count(),
        domain={'x': [0, 0.125], 'y': [0, 1]},
        delta={'reference': df_previous['clusterID'].count(), 'relative': True}))

    fig2.add_trace(go.Indicator(
        mode="number+delta",
        value=df_current['noTweet'].max(),
        title={"text": "Largest<br>Community"},
        delta={'reference': df_previous['noTweet'].max(), 'relative': True},
        domain={'x': [0.126, 0.25], 'y': [0, 1]}))

    fig2.add_trace(go.Indicator(
        mode="number+delta",
        value=df_current['noTweet'].min(),
        title={"text": "Smallest<br>Community"},
        delta={'reference': df_previous['noTweet'].min(), 'relative': True},
        domain={'x': [0.26, 0.375], 'y': [0, 1]}))

    fig2.add_trace(go.Indicator(
        mode="number+delta",
        value=round(df_current['noTweet'].mean(), 2),
        title={"text": "Average<br>Community"},
        delta={'reference': round(df_previous['noTweet'].mean(), 2), 'relative': True},
        domain={'x': [0.376, 0.5], 'y': [0, 1]}))

    fig2.add_trace(go.Indicator(
        mode="number+delta",
        value=round(pow(df_current["noTweet"] - df_current["noTweet"].mean(), 3).sum() / (
                df_current["noTweet"].count() * pow(sqrt(current_variance), 3)), 2),
        title={"text": "Skewness"},
        delta={'reference': round(pow(df_previous["noTweet"] - df_previous["noTweet"].mean(), 3).sum() / (
                df_previous["noTweet"].count() * pow(sqrt(previous_variance), 3)), 2), 'relative': True},
        domain={'x': [0.51, 0.625], 'y': [0, 1]}))

    fig2.add_trace(go.Indicator(
        mode="number+delta",
        value=round(pow(df_current["noTweet"] - df_current["noTweet"].mean(), 4).sum() / (
                df_current["noTweet"].count() * pow(sqrt(current_variance), 4)), 2),
        title={"text": "Kurtosis"},
        delta={'reference': round(pow(df_previous["noTweet"] - df_previous["noTweet"].mean(), 4).sum() / (
                df_previous["noTweet"].count() * pow(sqrt(previous_variance), 4)), 2), 'relative': True},
        domain={'x': [0.626, 0.75], 'y': [0, 1]}))

    fig2.add_trace(go.Indicator(
        mode="number+delta",
        value=round(current_variance, 2),
        title={"text": "Variance"},
        delta={'reference': round(previous_variance, 2), 'relative': True},
        domain={'x': [0.76, 0.875], 'y': [0, 1]}))

    fig2.add_trace(go.Indicator(
        mode="number+delta",
        value=df_current['tpu'].mean(),
        title={"text": "Tweet/User"},
        delta={'reference': df_previous['tpu'].mean(), 'relative': True},
        domain={'x': [0.876, 1], 'y': [0, 1]}))

    fig2.update_layout(height=200, margin=dict(l=30, r=30, b=30, t=80))

    return fig2


# ------------------ Detail Metrics -------------------------
# ------------------ Sunburst -------------------------------
def sunburst_creation():
    global df_current
    return px.sunburst(df_current, path=['clusterParent'], values='noTweet')


# ------------------ Table ----------------------
def table_creation():
    global df_current
    fig_table = go.Figure()

    fig_table.add_trace(go.Table(
        header=dict(
            values=df_current.columns.values.tolist(),
            font=dict(size=12),
            align="left"
        ),
        cells=dict(
            values=[df_current[k].tolist() for k in df_current.columns],
            align="left"),
        domain={'x': [0, 1], 'y': [0, 1]}
    ))

    fig_table.update_layout(
        height=500,
        showlegend=False,
        title_text="Cluster Detail",
    )

    return fig_table


# #### histogram
def hist_friend_creation(df_histogram):
    return px.histogram(df_histogram, x="friends", color_discrete_sequence=['indianred'])


def hist_follow_creation(df_histogram):
    return px.histogram(df_histogram, x="followers", color_discrete_sequence=['indianred'])


def hist_fav_creation(df_histogram):
    return px.histogram(df_histogram, x="favourites", color_discrete_sequence=['indianred'])


# --------------- word chart ----------------------#
def word_chart():
    global df_word
    top20 = df_word.sort_values(by=["prob"], ascending=False).head(20)
    return px.bar(top20, x='word', y='prob', color='word')


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    dcc.Interval(id='interval', interval=10 * 1000, n_intervals=0),
    html.H1(children='CVC Key Metrics', style={'font-weight': 'bold', 'textAlign': 'center'}),

    html.H5(children='''Realtime report on streaming CVC''', style={'textAlign': 'center'}),

    html.H3(children='OVERALL METRICS', style={'font-weight': 'bold', 'color': '#2E86C1'}),

    html.Div(id='community_graph', children=dcc.Graph(
        id="community",
        figure=community_creation())),
    html.H3(children='DETAIL METRICS', style={'font-weight': 'bold', 'color': '#2E86C1'}),
    html.Div([
        html.Div(id='cluster_graph', children=dcc.Graph(
            id='hierarchy-cluster',
            figure=sunburst_creation()
        ), className='six columns'),
        html.Div(id='table_graph', children=dcc.Graph(
            id='cluster-table',
            figure=table_creation()
        ), className='six columns')
    ], className='row'),

    html.H5(children='Community Interaction', style={'font-weight': 'bold', 'color': '#2E86C1'}),
    html.Div(id='hist_graph', children=[
        dcc.Graph(
            id='hist-friend',
            figure=hist_friend_creation(df_histogram)
        ),
        dcc.Graph(
            id='hist-follower',
            figure=hist_follow_creation(df_histogram)
        ),
        dcc.Graph(
            id='hist-favorite',
            figure=hist_fav_creation(df_histogram)
        ),
    ], style={'columnCount': 3}),

    html.H5(children='Top Trending Words', style={'font-weight': 'bold', 'color': '#2E86C1'}),
    html.Div(id='bar_chart', children=dcc.Graph(
        id='word-chart',
        figure=word_chart()
    ))
])


@app.callback(dash.dependencies.Output('community', 'figure'),
              [dash.dependencies.Input('interval', 'n_intervals')])
def update_community_graph(n):
    global df_previous, df_current
    global query, recv
    query = recv.get("visual_request")

    update_data(query)

    return community_creation()


@app.callback(dash.dependencies.Output('hierarchy-cluster', 'figure'),
              [dash.dependencies.Input('interval', 'n_intervals')])
def update_cluster_graph(n):
    return sunburst_creation()


@app.callback(dash.dependencies.Output('cluster-table', 'figure'),
              [dash.dependencies.Input('interval', 'n_intervals')])
def update_table_graph(n):
    return table_creation()


@app.callback(dash.dependencies.Output('hist-friend', 'figure'),
              [dash.dependencies.Input('interval', 'n_intervals')])
def update_histogram_friend_graph(n):
    global df_histogram

    return hist_friend_creation(df_histogram)


@app.callback(dash.dependencies.Output('hist-follower', 'figure'),
              [dash.dependencies.Input('interval', 'n_intervals')])
def update_histogram_follower_graph(n):
    global df_histogram

    return hist_follow_creation(df_histogram)


@app.callback(dash.dependencies.Output('hist-favorite', 'figure'),
              [dash.dependencies.Input('interval', 'n_intervals')])
def update_histogram_favorite_graph(n):
    global df_histogram

    return hist_fav_creation(df_histogram)


def update_data(query):
    query_df = json_normalize(query)
    global df_previous, df_current, df_histogram, df_word

    df_previous = df_current
    num_cluster = query_df.at[0, "num_cluster"]

    new_data = {
        'clusterID': [],
        'clusterParent': [],
        'noTweet': [],
        'tpu': []
    }
    new_df_current = pd.DataFrame(new_data)

    new_data_histogram = {
        'clusterID': [],
        'user': [],
        'friends': [],
        'followers': [],
        'favourites': []
    }
    new_df_histogram = pd.DataFrame(new_data_histogram)

    cluster_list = []
    clusters_size = []
    clusters_parent = []
    clusters_friends = []
    clusters_followers = []
    clusters_favourites = []
    clusters_tpu = []
    clusters_user = []

    for cluster_id in range(num_cluster):
        cluster_list.append(str(cluster_id))
        clusters_size.append(query_df.at[0, "cluster_" + str(cluster_id) + "_size"])
        clusters_parent.append(query_df.at[0, "cluster_" + str(cluster_id) + "_parent"])
        clusters_friends.append(query_df.at[0, "cluster_" + str(cluster_id) + "_friends"])
        clusters_followers.append(query_df.at[0, "cluster_" + str(cluster_id) + "_followers"])
        clusters_favourites.append(query_df.at[0, "cluster_" + str(cluster_id) + "_favourite"])
        clusters_user.append(query_df.at[0, "cluster_" + str(cluster_id) + "_user"])
        clusters_tpu.append(query_df.at[0, "cluster_" + str(cluster_id) + "_tpu"])

    new_df_current["clusterID"] = cluster_list
    new_df_current["clusterParent"] = clusters_parent
    new_df_current["noTweet"] = clusters_size
    new_df_current["tpu"] = clusters_tpu
    df_current = new_df_current

    new_df_histogram["clusterID"] = cluster_list
    new_df_histogram["user"] = clusters_user
    new_df_histogram["friends"] = clusters_friends
    new_df_histogram["followers"] = clusters_followers
    new_df_histogram["favourites"] = clusters_favourites
    df_histogram = new_df_histogram

    # for word metrics
    num_word = query_df.at[0, "num_of_word"]
    word_list = []
    word_list_prob = []

    new_data_word = {
        "word": [],
        "prob": []
    }
    new_df_word = pd.DataFrame(new_data_word)

    for i in range(num_word):
        word_list.append(query_df.at[0, "word_" + str(i)])
        word_list_prob.append(query_df.at[0, "word_" + str(i) + "_score"])

    new_df_word["word"] = word_list
    new_df_word["prob"] = word_list_prob
    df_word = new_df_word


@app.callback(dash.dependencies.Output('word-chart', 'figure'),
              [dash.dependencies.Input('interval', 'n_intervals')])
def update_word_data(n):
    # global df_word
    #
    # word_chart_update = dcc.Graph(
    #     id='cluster-table',
    #     figure=word_chart()
    # )
    return word_chart()


if __name__ == '__main__':
    # 1. Getting parameters
    args = parser.parse_args()
    recv = DataReceiver(args.host, args.port, args.db)
    app.run_server()
