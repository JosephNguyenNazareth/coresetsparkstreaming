package streaming

import scala.io.Source
import Double._
import math._
import java.io._

import util.control.Breaks._
import scala.collection.mutable.ArrayBuffer
import scala.math.Ordering
import sys.process._
import configuration.YAMLConfig
import java.util
import java.util.UUID.randomUUID
import java.util.Properties

import scala.collection.JavaConverters._
import utils._
import sampling.IProTraS
import estimation.VATClus
import estimation.VATClus2
import clustering.{CVCModel, KMEANS}
import utils.{DataPoint, Dataset, Pipeline}
import sampling.IProTraS

object StreamingModel {

  private var rankList: List[Int] = List[Int]()
  private var coresetQ: Array[Dataset] = Array[Dataset]()
  private var coresetK: Array[Dataset] = Array[Dataset]()
  private var coresetTemp: Dataset = new Dataset()
  private var samplingSet: Dataset = new Dataset()
  private var clusteredSamplingSet: Dataset = new Dataset()

  // protras configuration
  private var coordinate: Int = 100
  private var remainingRankZeroSize: Int = coordinate
  private var epsilon: Double = 0.03
  private var percentage: Double = 0.8
  private var fileCore: String = ""
  private var globalOrder: Int = 0
  private var algorithm: String = ""
  private var magicalConstant: Double = 5

  // ivat configuration
  private val avatMode: String = "avatlite" //avat, avatlite, avatlite_idsa
  private val distanceFunction =
    (firstPoint: DataPoint, secondPoint: DataPoint) => {
      Measure.euclide(firstPoint, secondPoint)
    }
  private var numClus = Array[Int]()
  private val ratio: Double = 0.000800000001
  private val thresholdLevel: Int = 2 //for idsa
  private val numGen: Int = 200
  private val population: Int = 50
  private val avatLiteThread: Option[Thread] = None
  private val avatLiteHost = "localhost"
  private val avatLitePort = 6379
  private var switchOnInflux = false
  private val kmeansCall: Int = 3
  private var kmeansOrder: Int = 1
  private var estimatedCluster: Int = 0
  private var originalIndex: Map[Int, Int] = Map()

  private val iprotras: IProTraS = new IProTraS()
  private val vatclus = new VATClus2()
//  private val corekm = new COREKM()

  def setAlgorithm(algo: String, prob: String = "skip"): Unit = {
    algorithm = algo
    if (algo == "cvc_idsa" && prob == "text") {
      iprotras
        .setDistanceFunction(Measure.normalizedLevenshteinDistance)
        .setCosineFunction(Measure.cosineAngleFromText)
        .setPercentage(percentage)
        .enablePlot(false)
        .enableLog(false)
        .setEpsilon(epsilon)
        .setCalMode("memory-based")
        .asInstanceOf[IProTraS]

      vatclus
        .setVATMode("ivat") //avat requires that an ivat image be used
        .setRatio(ratio)
        .setAVATMode(avatMode)
        .setThresholdValueForIDSA(thresholdLevel)
        .setPopulationForIDSA(population)
        .setNumGenerationForIDSA(numGen)
        .setDistanceFunction(Measure.normalizedLevenshteinDistance)
        .setAVATLiteThread(avatLiteThread)
        .initInfluxDBConnection()
        .initInfluxDBConnection()
      switchOnInflux = true
    } else if (algo == "cvc") {
      iprotras
        .setDistanceFunction(Measure.euclide)
        .setCosineFunction(Measure.cosineAngleFromPoint)
        .setPercentage(percentage)
        .enablePlot(false)
        .enableLog(false)
        .setEpsilon(epsilon)
        .setCalMode("memory-based")
        .asInstanceOf[IProTraS]

      vatclus
        .setVATMode("ivat") //avat requires that an ivat image be used
        .setRatio(ratio)
        .setAVATMode(avatMode)
        .setDistanceFunction(Measure.euclide)
        .setAVATLiteThread(avatLiteThread)
    } else if (algo == "corekmeans") {
      iprotras
        .setDistanceFunction(Measure.euclide)
        .setCosineFunction(Measure.cosineAngleFromPoint)
        .setPercentage(percentage)
        .enablePlot(false)
        .enableLog(false)
        .setEpsilon(epsilon)
        .setCalMode("memory-based")
        .asInstanceOf[IProTraS]

      vatclus
        .setVATMode("ivat") //avat requires that an ivat image be used
        .setRatio(ratio)
        .setAVATMode(avatMode)
        .setDistanceFunction(Measure.euclide)
        .setAVATLiteThread(avatLiteThread)
    } else if (algo == "corekm_tweet") {
      iprotras
        .setDistanceFunction(Measure.normalizedLevenshteinDistance)
        .setCosineFunction(Measure.cosineAngleFromText)
        .setPercentage(percentage)
        .enablePlot(false)
        .enableLog(false)
        .setEpsilon(epsilon)
        .setCalMode("memory-based")
        .asInstanceOf[IProTraS]

      vatclus
        .setVATMode("ivat") //avat requires that an ivat image be used
        .setRatio(ratio)
        .setAVATMode(avatMode)
        .setDistanceFunction(Measure.normalizedLevenshteinDistance)
        .setAVATLiteThread(avatLiteThread)
    }
  }

  def setConstant(newConstant: Double): Unit = {
    magicalConstant = newConstant
  }

  def setEpsilon(newEpsilon: Double): Unit = {
    epsilon = newEpsilon
  }

  def getCluster(): Int = {
    return estimatedCluster
  }

  def setTargetFile(newFileCore: String): Boolean = {
    fileCore = newFileCore
    globalOrder = 0
    deleteInitialClusterFile()
  }

  def closeConnection(): Unit = {
    vatclus.closePythonConnection()
    if (switchOnInflux)
      vatclus.closeInfluxDBConnection()
  }

  def deleteInitialClusterFile(): Boolean = {
    new File(
      "./result/initial_cluster/data/" + fileCore + "/" + fileCore + "_v1_origin.csv")
      .delete()
  }

  def checkConsecutiveRank(startRank: Int): Int = {
    val sortRankList: List[Int] = rankList.sorted.distinct
    var endRank: Int = startRank
    var stop: Boolean = false
    breakable {
      for (j <- sortRankList.indexOf(startRank) to sortRankList.length - 2) {
        if (sortRankList(j + 1) != sortRankList(j) + 1) {
          endRank = sortRankList(j)
          stop = true
          break
        }
      }
    }
    if (!stop) {
      endRank = sortRankList.last
    }
    return endRank
  }

  def setBatchSize(newCoordinate: Int = 100): Unit = {
    coordinate = newCoordinate
    remainingRankZeroSize = newCoordinate
  }

  def parseDataStreaming(dataset: Dataset, newCoordinate: Int = 100): Unit = {
    _parseDataStreaming(dataset)
//    println("coresetK0", coresetK(0).length(), coresetQ(0).length())
  }

  def _parseDataStreaming(dataset: Dataset): Unit = {
    fileCore = dataset.getFileCore()
    val batchDataSize: Int = dataset.length()
    if (batchDataSize < remainingRankZeroSize) {
      if (coresetQ.length == 0) {
        coresetQ :+= dataset
        coresetK :+= dataset
      } else {
        coresetQ(0) ++= dataset
      }
      remainingRankZeroSize = remainingRankZeroSize - batchDataSize
    } else if (batchDataSize == remainingRankZeroSize) {
      if (coresetQ.length == 0) {
        coresetQ :+= dataset
        coresetK :+= dataset
      } else {
        coresetQ(0) ++= dataset
      }
      coresetTemp = coresetQ(0).clone
      val maxRankResconstructed: Int = mergeCoresetQ(1, calculateSigma(1))
      mergeCoresetK(maxRankResconstructed)
      remainingRankZeroSize = coordinate
      coresetQ(0) = new Dataset()

      if (kmeansOrder == 0) {
//        clustering()
      }
      kmeansOrder = (kmeansOrder + 1) % kmeansCall
    } else {
      if (coresetQ.length == 0) {
        coresetQ :+= new Dataset()
        coresetK :+= new Dataset()
      }
      for (i <- 0 until remainingRankZeroSize) {
        coresetQ(0) :+= dataset(i)
      }
      coresetTemp = coresetQ(0)
      val nextDataPoint: Dataset = dataset.diff(coresetTemp)

      val maxRankResconstructed: Int = mergeCoresetQ(1, calculateSigma(1))
//      if (nextDataPoint.length() < coordinate) {
      mergeCoresetK(maxRankResconstructed)
      if (kmeansOrder == 0) {
//        clustering()
      }
      kmeansOrder = (kmeansOrder + 1) % kmeansCall
//      }
      remainingRankZeroSize = coordinate
      coresetQ(0) = new Dataset()
      // println("how are you")

      _parseDataStreaming(nextDataPoint)
    }
    // save image and data
//    cumulativeDataset.writeToFile(
//      "./result/streaming/data/" + fileCore + ".csv")
  }

  def mergeCoresetK(maxRank: Int): Unit = {
    // in case that maxRank is freshly new
    // we have to add it to coresetK
    // otherwise, just reconstruct that rank
//    println("max rank: ", coresetK.length, coresetQ.length, maxRank)
    if (maxRank >= 0) {
      if (maxRank + 1 == coresetQ.length) {
        if (maxRank == coresetK.length) {
          coresetK :+= coresetQ(maxRank).clone
        } else {
          coresetK(maxRank) = coresetQ(maxRank).clone
        }
      } else {
        coresetTemp = coresetK(maxRank + 1).clone
        coresetTemp ++= coresetQ(maxRank).clone
        if (maxRank == 0) {
          coresetK(maxRank) = coresetTemp
//          println("k0",coresetK(0).length())
        } else {
          val rho: Double = epsilon / (magicalConstant * maxRank * maxRank)
          coresetK(maxRank) = createCoreset(coresetTemp, rho)
        }
//        println(maxRank, coresetK(maxRank).length())
      }
      mergeCoresetK(maxRank - 1)
    }
  }

  def mergeCoresetQ(rank: Int, threshHold: Double): Int = {
    coresetTemp = createCoreset(coresetTemp, threshHold)
//    println("Q ", rank, coresetTemp.length(), coresetQ.length, threshHold)

    if (rank + 1 > coresetQ.length) {
      coresetQ :+= coresetTemp.clone
      return rank
    } else if (coresetQ(rank).length() == 0) {
      coresetQ(rank) = coresetTemp.clone
      return rank
    } else {
      coresetTemp ++= coresetQ(rank).clone
      val rho: Double = epsilon / (magicalConstant * (rank + 1) * (rank + 1))
      coresetQ(rank) = new Dataset()
      return mergeCoresetQ(rank + 1, rho)
    }
  }

  def calculateSigma(rank: Int): Double = {
    var sigma: Double = 1.0
    for (i <- 1 to rank) {
      sigma = sigma * (1 + epsilon / (magicalConstant * i * i))
    }
    return sigma - 1
  }

  def createCoreset(datasetStreaming: Dataset, threshold: Double): Dataset = {
    // assign new index for protras
    val dataPointProcess: Array[DataPoint] = datasetStreaming.getDataset()
    for (x <- 0 until datasetStreaming.length()) {
      originalIndex += (x -> dataPointProcess(x).index)
      dataPointProcess(x).index = x
    }
    val datasetStreamingNew: Dataset = new Dataset(dataPointProcess)

//    println("before protras", datasetStreamingNew.length())

    iprotras.setEpsilon(threshold)
    val pipeline: Pipeline = new Pipeline(Array(iprotras))
    pipeline.fit(datasetStreamingNew)
    pipeline.transform()

    // reset index for later process
    val coresetGenerated: Dataset = pipeline.getOutput()
    val coresetDataPoint: Array[DataPoint] = coresetGenerated.getDataset()
    for (x <- 0 until coresetGenerated.length()) {
      coresetDataPoint(x).index = originalIndex(coresetDataPoint(x).index)
    }
//    for (x <- 0 until datasetStreamingNew.length()) {
//      datasetStreamingNew(x).index = originalIndex(x)
//      datasetStreamingNew(x).rep = originalIndex(datasetStreamingNew(x).rep)
//    }
    val coresetNew: Dataset = new Dataset(coresetDataPoint)
    originalIndex = Map()
//    println("Coreset created")
    return coresetNew
  }

  def clustering(): Unit = {
    coresetK(0).setTargetFile(fileCore)
    println("coresetK0", coresetQ(0).length(), coresetK(0).length(), algorithm)
    if (algorithm.contains("corekm")) {
      val pipeline: Pipeline = new Pipeline(Array(vatclus))
      pipeline.fit(coresetK(0))
      pipeline.transform()

      val clusterList: Array[Array[Int]] = vatclus.getClusterList()
      println("Start KMeans..")
      val kmeans = new KMEANS()
        .setK(clusterList.length)
        .setCoreset(clusterList)
        .setCheatMode("cheat")
      clusteredSamplingSet = kmeans.fit(coresetK(0))
      estimatedCluster = kmeans.getClustes()
    }
//    else {
//      println("Coreset created")
//      val pipeline: Pipeline = new Pipeline(Array(corekm))
//      pipeline.fit(cumulativeDataset)
//      pipeline.transform()
//
//      samplingSet = pipeline.getOutput()
//    }
  }

  def getOutput(): Dataset = {
    if (clusteredSamplingSet.getDataset().isEmpty)
      return coresetK(0)
    else
      return clusteredSamplingSet
  }

  def saveScatterChart(saveDataset: Dataset,
                       saveType: String = "coreset"): Unit = {
    val saveData: Array[DataPoint] = saveDataset.getDataset()
    val dir =
      scala.reflect.io
        .File("./result/streaming/coreset/image/" + fileCore + "/")
    if (!dir.isDirectory) dir.createDirectory()

    if (saveType.equals("coreset")) {
      saveDataset.saveScatterChart(
        "./result/streaming/coreset/image/" + fileCore + "/" + fileCore + "_v1.png",
        fileCore + "_v1")
    }
  }

  def writeToFileStreaming(saveDataset: Dataset,
                           saveType: String = "coreset"): Unit = {
    val saveData: Array[DataPoint] = saveDataset.getDataset()
    if (saveData.length > 0) {
      println("ready")
      val dir =
        scala.reflect.io
          .File("./result/streaming/coreset/data/" + fileCore + "/")
      if (!dir.isDirectory) dir.createDirectory()
    }

    if (saveType.equals("coreset")) {
      val core = new PrintWriter(new File(
        "./result/streaming/coreset/data/" + fileCore + "/" + fileCore + "_v1.csv"))
      core.write(saveData.map(_.toString).mkString("\n"))
      core.close()
    }
  }

//  def regeneration(originalDataset: Dataset): Unit = {
//    val sample: Array[DataPoint] = this.getOutput().getDataset()
//    println("sample")
//    println(sample.mkString("\n"))
//    for (x <- 0 until originalDataset.length) {
//      println(x)
//      val trueIndex: Int = originalDataset(x).rep
//      originalDataset(x).cluster =
//        sample.filter(x => x.index == trueIndex)(0).cluster
//    }
//  }
}
