package clustering

import scala.io._
import sys.process._
import java.io.IOException

import scala.util.control.Breaks._
import math._
import utils.{DataPoint, Dataset, Measure, Pipeline, PipelineOperator}
import sampling.{IProTraS, ProTraS}
import estimation.VATClus
import estimation.VATClus2
import com.redis._
import configuration.YAMLConfig

class CVCModel extends ClusterAlgorithm[CVCModel] {
  /*
    A clustering algorithm designed by KK team
   */
  private var epsilon: Double = -1.0 //[0,1]
  private var percentage: Double = 0.5 //[0,1]
  private var ratio: Double = 0.00088 //[0,1]
  private var samplingMode: String = "improved" //default
  private var samplingPhase: String = "default" //default
  private var avatMode: String = "avatlite" //avat, avatlite, avatlite_idsa
  private var mode: String = "cluster" //estimate
  private var returnMode: String = "sample" //origin
  private var thresholdLevel: Int = 2 //for idsa
  private var numGen: Int = 200
  private var population: Int = 50
  private var cosineFunction = (b: DataPoint, a: DataPoint, c: DataPoint) => {
    Measure.cosineAngleFromPoint(b, a, c)
  }
  private var avatLiteThread: Option[Thread] = None
  private val redisConfig =
    YAMLConfig.get("redis").asInstanceOf[java.util.LinkedHashMap[String, Any]]
  private val redisAVATLite = redisConfig
    .get("redis_avatlite")
    .asInstanceOf[java.util.LinkedHashMap[String, Any]]
  private var applyKmeans: Boolean = false

  def setRatio(m: Double): CVCModel = {
    /*
        setting ratio for CVC that is used in removing too-small clusters
     */
    ratio = m
    return this
  }

  def setCosineFunction(
      cf: (DataPoint, DataPoint, DataPoint) => Double): CVCModel = {
    cosineFunction = cf
    return this
  }

  def setAVATMode(av: String): CVCModel = {
    /*
        Setting avat mode for CVC
     */
    if (av != "avat" && av != "avatlite" && av != "avatlite_idsa") {
      println("Unknown avat mode: Using default")
      avatMode = "avat"
    } else {
      avatMode = av
    }
    return this
  }

  def setSamplingMode(m: String): CVCModel = {
    /*
        Setting sampling mode for CVC
     */
    if (m != "default" && m != "improved") {
      println("Unknown avat mode: Using default, which is improved mode")
      samplingMode = "improved"
    } else {
      samplingMode = m
    }
    return this
  }

  def setReturnMode(m: String): CVCModel = {
    /*
        Setting type of dataset to return after finishing the algorithm
     */
    if (m != "sample" && m != "origin") {
      println("Unknown return mode: Using default")
      returnMode = "origin"
    } else {
      returnMode = m
    }

    if (returnMode == "sample") {
      println(
        "Warning: In class CVC, by returning the sampling set, system will try to create a new object. As a result, the pointer to the origin dataset will not point to this newly created dataset.")
    }

    return this
  }

  def setMode(mo: String): CVCModel = {
    /*
        Setting whether this CVCModel used for clustering or estimating
     */
    if (mo != "cluster" && mo != "estimate") {
      println("Unknown  mode: Using default")
      mode = "cluster"
    } else {
      mode = mo
    }

    return this
  }

  def setSamplingPhase(phase: String): CVCModel = {
    // 2 phases:
    // default: start from scratch
    // modification: start from Corset of ProTraS
    samplingPhase = phase

    return this
  }

  override def toString: String = {
    return "cvc"
  }

  def setPercentage(per: Double): CVCModel = {
    /*
        Determine the magnitude of "closeness" to shift samples points
     */
    if (per < 0.0 || per > 1.0) {
      println(
        "Error setting percentage for the modified ProTraS: Please choose percentage that is between 0 and 1!")
    }
    percentage = per
    //set percentage -> improved mode
    samplingMode = "improved"
    return this
  }

  def setEpsilon(eps: Double): CVCModel = {
    /*
        Determine the value of epsilong for ProTraS algorithm
     */
    if (eps <= 0.0) {
      println(
        "Error setting epsilon for the ProTraS: Please choose epsilon that is larger than 0!")
    }
    epsilon = eps
    return this
  }

  def setThresholdValueForIDSA(value: Int): CVCModel = {
    /*
        Set threshold value only for IDSA
     */
    if (value <= 0) {
      println("Invalid value for idsa")
    } else {
      thresholdLevel = value
    }
    return this
  }

  def setPopulationForIDSA(value: Int): CVCModel = {
    /*
        Set number of population for IDSA
     */
    if (value <= 0) {
      println("Invalid value for idsa")
    } else {
      population = value
    }
    return this
  }

  def setNumGenerationForIDSA(value: Int): CVCModel = {
    /*
        Set number of generation for IDSA
     */
    if (value <= 0) {
      println("Invalid value for idsa")
    } else {
      numGen = value
    }
    return this
  }

  def setApplyKmeans(isApplied: Boolean): CVCModel = {
    applyKmeans = isApplied
    return this
  }

  private def _generalization(clusterList: Array[Array[Int]],
                              originalDataset: Dataset,
                              samplingDataset: Dataset): (Dataset, Dataset) = {
    var i: Int = 0
    //for [data]
    for (cluster <- clusterList) {
      for (idx <- cluster) {
        originalDataset(idx).cluster = i
        for (c <- 0 until samplingDataset.length) {
          if (samplingDataset(c).index == idx) {
            samplingDataset(c).cluster = i
          }
        }
      }
      i = i + 1
    }
    println(samplingDataset.length())
    //for coreset point that have not been clustered
    for (t <- 0 until samplingDataset.length) {
      if (samplingDataset(t).cluster == -1) {
        var minDistance: Double = Double.MaxValue
        var cIdx: Int = -1
        for (target <- 0 until samplingDataset.length) {
          if (samplingDataset(target).index != samplingDataset(t).index && samplingDataset(
                target).cluster != -1) { //not the point itself and must be the clustered coreset point
            var dis: Double = getDistance(samplingDataset, target, t)
            if (dis < minDistance) {
              minDistance = dis
              cIdx = samplingDataset(target).index
            }
          }
        }
        originalDataset(samplingDataset(t).index).cluster = originalDataset(
          cIdx).cluster
        samplingDataset(t).cluster = originalDataset(cIdx).cluster
      }
    }

    //for other point
    for (x <- 0 until originalDataset.length) {
      originalDataset(x).cluster = originalDataset(originalDataset(x).rep).cluster
    }

    return Tuple2(originalDataset, samplingDataset)
  }

  /*
    Initialize python process for avatlite processing
   */

  private def initPythonClient(): Unit = {
    if (avatMode == "avatlite" || avatMode == "avatlite_idsa") {
      if (avatLiteThread.isEmpty) {
        val avatLiteHost = redisAVATLite.get("host").asInstanceOf[String]
        val avatLitePort = redisAVATLite.get("port").asInstanceOf[Int]
        val database = redisAVATLite.get("database").asInstanceOf[Int]
        val re = new RedisClient(avatLiteHost, avatLitePort, database)
        avatLiteThread = Some(new Thread {
          override def run {
            s"python3 -m common_python.estimation.aVATLite --host ${avatLiteHost} --port ${avatLitePort} --db ${database}" !
          }
        })
        //thread start
        avatLiteThread.get.start()
        //waiting for python to finish processing
        breakable {
          //maximum: 10s = 10000 milliseconds
          var start: Long = System.currentTimeMillis
          while (true) {
            val status = re.get("avatlite_status")
            if (status.isDefined) {
              if (status.get == "success") {
                println("Successfully connect to python through Redis.")
                break
              } else {
                //incorrect signal
                println(
                  "Failed connecting to Python through Redis. Exiting ...")
                avatLiteThread.get.stop()
                System.exit(1)
              }
            }
            if (System.currentTimeMillis - start > 10000) {
              println("Failed connecting to Python through Redis. Exiting ...")
              avatLiteThread.get.stop()
              System.exit(1)
            } else {
              Thread.sleep(1000)
              println("Connecting ..")
            }
          }
        }
      }
    }
  }

  def fit(dataset: Dataset): Dataset = {
    initPythonClient()

    //1. Creating an array of PipelineOperator
    var procedures: Array[PipelineOperator] = Array[PipelineOperator]()

    //a. sampling step
    if (samplingMode == "improved") {
      var sample: IProTraS = new IProTraS()
        .setEpsilon(epsilon)
        .setPercentage(percentage)
        .setDistanceFunction(distanceFunction)
        .enablePlot(plotEnable)
        .enableLog(logEnable)
        .setCosineFunction(cosineFunction)
        .setCalMode(calMode)
        .setSamplingMode(samplingMode)
        .setSamplingPhase(samplingPhase)
        .asInstanceOf[IProTraS]
      procedures :+= sample
    } else {
      var sample: ProTraS = new ProTraS()
        .setEpsilon(epsilon)
        .setDistanceFunction(distanceFunction)
        .enablePlot(plotEnable)
        .enableLog(logEnable)
        .setCalMode(calMode)
        .setSamplingMode(samplingMode)
      procedures :+= sample
    }

    //b. vat step
    var vatclus = new VATClus()
      .setVATMode("ivat") //avat requires that an ivat image be used
      .setEpsilon(epsilon)
      .setPercentage(percentage)
      .setRatio(ratio)
      .setAVATMode(avatMode)
      .setThresholdValueForIDSA(thresholdLevel)
      .setPopulationForIDSA(population)
      .setNumGenerationForIDSA(numGen)
      .setDistanceFunction(distanceFunction)
      .setAVATLiteThread(avatLiteThread)

    procedures :+= vatclus

    //declare pipeline
    var pipeline = new Pipeline(procedures)
    //fit dataset
    pipeline.fit(dataset)
    //begin transform
    pipeline.transform()
    //get output of dataset
    var samplingSet = pipeline.getOutput()

    if (applyKmeans)
      return samplingSet

    //For mode: cluster only
    var clusterDuration: Long = 0
    var returnDataset: Dataset = null

    if (mode == "cluster") {
      if (avatMode == "avatlite_idsa") {
        val idsaClusterList = vatclus.getIDSAClusterList()
        numClus = Array[Int]()
        println(
          "Beta mode: Sampling set or dataset will not change in avatlite_idsa mode")
        //cluster for multiple files
        var iter = 0
        for (image <- idsaClusterList) {
          val num: Int = image.length
          numClus :+= num
          val t0 = System.nanoTime() //begin algorithm
          val superDataset =
            _generalization(image, dataset.clone, samplingSet.clone)
          val t1 = System.nanoTime()
          clusterDuration += t1 - t0
          if (logEnable) {
            var originalDataset = superDataset._1
            var samplingDataset = superDataset._2
            var oldName = samplingDataset
              .getFileCore() + s"_${avatMode + "_" + iter.toString()}"
            samplingDataset.setTargetFile(oldName + "_coreset")
            writeToFile(samplingDataset)
            samplingDataset.setTargetFile(oldName)
            originalDataset.setTargetFile(oldName)
            writeToFile(originalDataset)
          }
          if (plotEnable) {
            var originalDataset = superDataset._1
            var samplingDataset = superDataset._2
            var oldName = samplingDataset
              .getFileCore() + s"_${avatMode + "_" + iter.toString()}"
            samplingDataset.setTargetFile(oldName + s"_coreset")
            saveScatterChart(samplingDataset)
            originalDataset.setTargetFile(oldName)
            saveScatterChart(originalDataset)
          }
          iter = iter + 1
        }
      } else {
        val t0 = System.nanoTime() //begin algorithm
        var clusterList = vatclus.getClusterList()
        val superDataset =
          _generalization(clusterList, dataset.clone, samplingSet.clone)
        val t1 = System.nanoTime()
        clusterDuration = t1 - t0
        if (logEnable) {
          var originalDataset = superDataset._1
          var samplingDataset = superDataset._2
          var oldName = samplingDataset.getFileCore() + s"_${avatMode}"
          samplingDataset.setTargetFile(oldName + "_coreset")
          writeToFile(samplingDataset)
          samplingDataset.setTargetFile(oldName)
          originalDataset.setTargetFile(oldName)
          writeToFile(originalDataset)
        }
        if (plotEnable) {
          var originalDataset = superDataset._1
          var samplingDataset = superDataset._2
          var oldName = samplingDataset.getFileCore() + s"_${avatMode}"
          samplingDataset.setTargetFile(oldName + s"_coreset")
          saveScatterChart(samplingDataset)
          originalDataset.setTargetFile(oldName)
          saveScatterChart(originalDataset)
        }
        numClus = vatclus.getEstimated()
        if (returnMode == "origin") {
          returnDataset = superDataset._1
        } else {
          returnDataset = superDataset._2
        }
      }
    }

    //calculate duration
    for (procedures <- procedures) {
      duration = duration + procedures.timeElapsed()
    }
    duration = duration + clusterDuration

    //depend on user: return
    return returnDataset
  }

  def closePythonConnection(): Unit = {
    //send message to close connection
    val avatLiteHost = redisAVATLite.get("host").asInstanceOf[String]
    val avatLitePort = redisAVATLite.get("port").asInstanceOf[Int]
    val database = redisAVATLite.get("database").asInstanceOf[Int]
    val re = new RedisClient(avatLiteHost, avatLitePort, database)
    re.set("is_join", "true")
    breakable {
      //maximum: 10s = 10000 milliseconds
      var start: Long = System.currentTimeMillis
      while (true) {
        val status = re.get("is_close")
        if (status.isDefined) {
          re.del("is_close")
          if (status.get == "true") {
            println("Successfully close Python process")
            break
          } else {
            Thread.sleep(1000)
            println("Closing ..")
          }
        }
      }
    }
  }
}
